<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attachment extends Model
{
    use SoftDeletes;


    protected $fillable =

        [
            'user_id',
//            'name',
            'employer_name',
            'employer_email',
            'address',
            'department',
            'start_date',
            'end_date',
            'job_title',
            'experience_area',
            'work_summary',
            'organisation_name',
            'date1',
            'date2',
            'position_held',
            'employee_prof_status',
            'date_endorsed',
            'comments',
            'status',
            'verifyToken',
        ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
