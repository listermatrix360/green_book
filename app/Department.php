<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{

    use SoftDeletes;

    protected $fillable = ['school_id','name'];



    public function school()
    {
        return $this->belongsTo(School::class);
    }


    public function programs()
    {
        return $this->hasMany(Program::class);
    }


    public function user()
    {
        return $this->hasMany(User::class);
    }
}
