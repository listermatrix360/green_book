<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Diary extends Model
{
    use SoftDeletes;

    protected  $fillable = ['user_id','title','body','date'];



    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
