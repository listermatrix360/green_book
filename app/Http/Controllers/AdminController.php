<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Notifications\PERNotify;
use Notification;

use Auth;
use App\staff;
use App\commservice;
use App\attachment;
use App\diary;
use App\mentor;
use App\personalDetail;
use App\profile_status;
use App\user;
use App\chapel;
use App\community_remark;
use App\physical_fit;
use App\physical_remark;
use DB;
use PDF;

class AdminController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
   */
    public function __construct()
    {
      $this->middleware('auth:admin');

    }

    public function community_final_remark(Request $request)
    {
      $change = community_remark::where('reg_num',$request->reg_num)
        ->update([
          'ess_remark'  => $request->remark
        ]);
      return 'updated';
    }

    public function physical_final_remark(Request $request)
    {
      $change = physical_remark::where('reg_num',$request->reg_num)
        ->update([
          'ess_remark'  => $request->remark
        ]);
      return 'updated';
    }


    public function home_tab()
    {
       $comm     = DB::table('commservices')->select('*')->where('status','pending')->get();
       $chapel   = DB::table('chapels')->select('*')->where('status','pending')->get();
       $physical = DB::table('physical_fits')->select('*')->where('status','pending')->get();
       $num = count($comm);
       $c_s = count($chapel);
       $c_p = count($physical);
       return view('admin.home-tab.first',compact('comm',
       'chapel','set_academic_year','physical','num','c_s','c_p'));
    }


    public function index()

    {
        $num = DB::table('profile_statuses')->join('users','profile_statuses.reg_num','=','users.reg_num')
        ->where([['prof_status','Approved'],['level','400']])->count();
        $rej = DB::table('profile_statuses')->join('users','profile_statuses.reg_num','=','users.reg_num')
        ->where([['prof_status','Rejected'],['level','400']])->count();

        $pending = DB::table('users')->join('personal_details','users.reg_num','=','personal_details.reg_num')
        ->select('users.*','personal_details.program')->where('level','400')->whereNotExists(function($status){
            $status->select(DB::raw('profile_statuses.reg_num'))->from('profile_statuses')
            ->whereRaw('profile_statuses.reg_num = users.reg_num');
        })->count();
        $count = DB::table('users')->where('level','400')->count('id');
          //JNR Queries
        $Jnr_num = DB::table('profile_statuses')->join('users','profile_statuses.reg_num','=','users.reg_num')
        ->where([['prof_status','Approved'],['level','!=','400'],['level','!=','completed']])->count();
        $Jnr_rej = DB::table('profile_statuses')->join('users','profile_statuses.reg_num','=','users.reg_num')
        ->where([['prof_status','Rejected'],['level','!=','400'],['level','!=','completed']])->count();
        $Jnr_pending = DB::table('users')->join('personal_details','users.reg_num','=','personal_details.reg_num')
        ->select('users.*','personal_details.program')->where([['level','!=','400'],['level','!=','completed']])->whereNotExists(function($status){
            $status->select(DB::raw('profile_statuses.reg_num'))->from('profile_statuses')
            ->whereRaw('profile_statuses.reg_num = users.reg_num');
        })->count();
        $Jnr_count = DB::table('users')->where([['level','!=','400'],['level','!=','completed']])->count('id');

        $fx_year = DB::table('current_year')->first();
        return view('admin.admin-dash', compact('count','num','rej','pending','Jnr_num','Jnr_rej',
        'Jnr_pending','Jnr_count','fx_year'));
    }

    public function change_year(Request $request)
    {
      $update = DB::table('current_year')->update(['year'=>$request->academic_year]);
      return 'Done';
    }

    public function com_post_update(Request $request,$state,$value)
    {
      $change = commservice::where('id',$request->link)
        ->update([
          'status'  => $state,
          'comment' => $value,
        ]);
      return 'updated';
    }

    public function cha_post_update(Request $request,$state,$value)
    {
      $change = chapel::where('id',$request->link)
        ->update([
          'status'  => $state,
          'comment' => $value,
        ]);
      return 'updated';
    }
    public function phy_post_update(Request $request,$state,$value)
    {
      $change = physical_fit::where('id',$request->link)
        ->update([
          'status'  => $state,
          'comment' => $value,
        ]);
      return 'updated';
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getStudentData($target)
    {
      $selUsers = DB::table('users')->select('*')
      ->where('reg_num',$target)->first();
      return response()->json($selUsers);
    }

    public function addStaff()
    {
      $dpt = DB::table('departments')->select('*')->get();
      return view('admin.add-staff',compact('dpt'));
    }

    public function createStaff(Request $request)
    {
      $staff = new staff;

      $this->validate($request,[
         'staff_id'=>'required|unique:staff',
         'email'=>'required:staff',
      ],[
          'reg_num.unique' => 'The ID number you are entering belongs to another user.',
          'reg_num.required'=> 'ID Number cannot be empty',
          'school.required'=> 'The School cannot be empty'
      ]);
      $staff->staff_id = $request->staff_id;
      $staff->title = $request->title;
      $staff->f_name = $request->f_name;
      $staff->las_name = $request->las_name;
      $staff->d_code = $request->department;
      $staff->email = $request->email;
      $staff->phone = $request->phone;
      $staff->save();
      return 'Data Saved';
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_level(Request $request)
    {
      $a = User::where('level','400')->update(['level' =>'completed']);
      $z = User::where('level','300')->update([ 'level' =>'400']);
      $y = User::where('level','200')->update([ 'level' =>'300']);
      $x = User::where('level','100')->update([ 'level' =>'200']);

     return 'Done';
    }

    public function action(Request $request)
    {
        $profile_status = new profile_status;
        $profile_status->reg_num = $request->reg_num;
        $profile_status->prof_status = $request->action;
        $profile_status->ess_comm = $request->comment;
        $profile_status->save();


          //will also try and include status whether is rejected or approved
        Notification::route('mail', $request->email)
            ->notify(new PERNotify());

        return 'done';
    }

    public function usershow()
    {
      $num = DB::table('profile_statuses')->where('prof_status','Approved')->count();
      $count = DB::table('users')->count('id');
      return view('admin.admin-show',compact('count','num'));
    }
    //for updating users details on the user table
    public function updateStudentData(Request $request)
    {
      $user = User::where('id',$request->id)
     ->update([
               'reg_num'         =>$request->reg_num,
               'level'           =>$request->level,
               'surname'         =>$request->name,
               'othernames'      =>$request->la_name,
               'email'           =>$request->email,
               'title'           =>$request->title,
     ]);
     return 'Done';

    }

    public function staff_edit_info($staff_id)
    {
      $user =  DB::table('staff')->join('departments','staff.d_code','=','departments.d_code')
      ->select('staff.*','departments.d_name')->where('staff.staff_id',$staff_id)->first();
      return response()->json($user);
    }

    public function staff_update_info(Request $request)
    {
      $staff = staff::where('id',$request->id)
      ->update([
                  'title'   =>$request->title,
                  'staff_id'=>$request->staff_id,
                  'f_name'=>$request->f_name,
                  'las_name'=>$request->las_name,
                  'd_code'=>$request->department,
                  'email'=>$request->email,
                  'phone'=>$request->phone,
      ]);
      return 'updated Successfully';
    }

    //to disable a staff
    public function disabled_update_info(Request $request)
    {
      $staff = staff::where('staff_id',$request->target)
      ->update([
                  'status'=>'0'
      ]);
      return 'Successfully Disabled';
    }

    //to enable a staff
    public function enabled_update_info(Request $request)
    {
      $staff = staff::where('staff_id',$request->target)
      ->update([
                  'status'=>'1'
      ]);
      return 'Successfully Enabled';
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($reg_num)
    {

        $users = DB::table('users')->join('personal_details','users.reg_num','=','personal_details.reg_num')
        ->select('users.*','personal_details.*')->where('users.reg_num',$reg_num)->first();

        $uzzers = DB::table('users')->select('*')->where('reg_num',$reg_num)->first();

        $mentor = DB::table('users')->join('mentors','users.reg_num','=','mentors.reg_num')
        ->join('staff','mentors.staff_id','=','staff.staff_id')
        ->select('mentors.*','staff.title','staff.f_name','staff.las_name')->where('mentors.reg_num',$reg_num)
        ->orderBy('level','asc')->get();

        $verify = DB::table('personal_details')->select('reg_num')->where('reg_num',$reg_num)->get();

        $action = DB::table('profile_statuses')->select('*')->where('reg_num',$reg_num)->first();

        $intern = DB::table('attachments')->where('reg_num',$reg_num)->get();
          //Query results for diaries
          $diary = DB::table('diaries')->select('*')->where('reg_num',$reg_num)->get();
          //Query for Professional Skills
          $prof = DB::table('prof_skills')->select('*')->where('reg_num',$reg_num)->get();
          //Query for educationa and examination
          $education = DB::table('educ_exams')->select('*')->where('reg_num',$reg_num)->get();
          //query for communiuty service
          $community = DB::table('commservices')->select('*')->where('reg_num',$reg_num)->get();
          $c_remark = DB::table('community_remarks')->select('*')->where('reg_num',$reg_num)->first();
          //Query for Other Courses attended
          $other = DB::table('othcourses')->select('*')->where('reg_num',$reg_num)->get();
          // query for chapel service
          $chapel = DB::table('chapels')->select('*')->where('reg_num',$reg_num)->get();
          $s_remark = DB::table('chapel_remarks')->select('*')->where('reg_num',$reg_num)->first();
          // query for physical fitness
          $physical = DB::table('physical_fits')->select('*')->where('reg_num',$reg_num)->get();
          $p_remark = DB::table('physical_remarks')->select('*')->where('reg_num',$reg_num)->first();
          $auto = DB::table('auto_bios')->select('*')->where('reg_num',$reg_num)->first();

        return view('admin.show-per',compact(
          'users','mentor','action','intern','uzzers','diary',
          'community','c_remark','physical','p_remark'
          ,'s_remark','chapel','prof','education','other','auto'));

    }

    public function change_status($reg_num){
        $update_status = profile_status::where('reg_num',$reg_num)->delete();
        return 'Done';
    }

    public function prof_status(Request $request)
    {

      $update = profile_status::where(['reg_num'=>$request->reg_num])->update(['prof_status'=>$request->action,'ess_comm'=>$request->comment]);
      return redirect('admin/show/'.$request->reg_num);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit(RegisDataTable $dataTable, $reg_num)
    // {   $count = DB::table('users')->count('id');
    //     $user = DB::table('users')->select('*')->where('reg_num',$reg_num)->first();
    //     return $dataTable->render('admin.edit',compact('user','count'));
    //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function report($reg_num)
    {
      $pd = DB::table('users')->join('personal_details','users.reg_num','=','personal_details.reg_num')
      ->where('users.reg_num',$reg_num)->first();

      $mentor = DB::table('mentors')->join('staff','mentors.staff_id','=','staff.staff_id')
      ->select('mentors.*','mentors.status as howfar','staff.*')
      ->where('mentors.reg_num',$reg_num)->get();

      $com    = DB::table('commservices')->where('reg_num',$reg_num)->get();
      $chap   = DB::table('chapels')->where('reg_num',$reg_num)->get();
      $phy    = DB::table('physical_fits')->where('reg_num',$reg_num)->get();
      $diary  = DB::table('diaries')->where('reg_num',$reg_num)->get();

      $intern = DB::table('attachments')->where('reg_num',$reg_num)->get();
      $educ   = DB::table('educ_exams')->where('reg_num',$reg_num)->get();
      $prof   = DB::table('prof_skills')->where('reg_num',$reg_num)->get();
      $other  = DB::table('othcourses')->where('reg_num',$reg_num)->get();
      $auto = DB::table('auto_bios')->select('*')->where('reg_num',$reg_num)->first();
      return view('admin.frame-report',compact('auto','pd','mentor','com','chap','phy','diary','intern','educ',
      'prof','other'));
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function defer_state(Request $request)
    {
      $change = user::where('reg_num',$request->id)
        ->update([
          'defer_status'  => $request->value
        ]);
      return 'updated';
    }
}
