<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Notifications\InternNotify;
use App\Notifications\PERNotify;
use App\attachment;
use App\prof_skill;
use Psy\Util\Str;

class AttachmentController extends Controller
{

    public function index()
    {
        return view('attach.attindex');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return view('attach.attachment');

        }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user  = Auth::user();

        $data = $request->validate([

//                'name' => 'required',
                'employer_name'=> 'required',
                'employer_email'=> 'required',
                'address'=> 'required',
                'department'=> 'required',
                'start_date'=> 'required',
                'end_date'=> 'required',
                'job_title'=> 'required',
                'experience_area'=> 'required',
                'work_summary'=> 'required',

        ]);

        $t = time();
        $data['verifyToken'] = Crypt::encryptString($t);


        DB::beginTransaction();

         $user->attachment()->create($data);

        DB::commit();

        return redirect()->back()->with('success',"Attachment successfully added");

    }


    public function show($id)
    {

        $user = Auth::user();
        $att_info = $user->attachment()->find($id);
        return view('attach.attach-show',compact('att_info'));
    }



    public function edit($id)
    {
        $user = Auth::user();
        $att_info = $user->attachment()->find($id);
        return view('attach.attach-edit',compact('att_info'));
    }


    public function update(Request $request, $id)
    {
        $user  = Auth::user();

        $attachment = $user->attachment()->find($id);

        $data = $request->validate([

            'employer_name'=> 'required',
            'employer_email'=> 'required',
            'address'=> 'required',
            'department'=> 'required',
            'start_date'=> 'required',
            'end_date'=> 'required',
            'job_title'=> 'required',
            'experience_area'=> 'required',
            'work_summary'=> 'required',

        ]);


        DB::beginTransaction();

        $attachment->update($data);

        DB::commit();

        return redirect()->route('attach.show',$id)->with('success',"Attachment successfully updated");
    }

    public function skill()
    {
      $count = DB::table('diaries')->where('reg_num',Auth::user()->reg_num)->count('reg_num');
      $num_attach = DB::table('attachments')->where('reg_num',Auth::user()->reg_num)->count('reg_num');
      $personal_details = DB::table('personal_details')->select('*')
      ->where('reg_num', Auth::user()->reg_num)->first();
      $mentor = DB::table('mentors')->select('*')->where('reg_num', Auth::user()->reg_num)->get();
      $diary = DB::table('diaries')->select('*')->where('reg_num', Auth::user()->reg_num)->get();
      $attachment = DB::table('attachments')->where('reg_num', Auth::user()->reg_num)->get();
      $img = DB::table('personal_details')->select('img')->where('reg_num',Auth::user()->reg_num)->get();
      $pskills = DB::table('prof_skills')->select('*')->where('reg_num',Auth::user()->reg_num)->get();
      $pskill_count = DB::table('prof_skills')->where('reg_num',Auth::user()->reg_num)->count('reg_num');

        return view('attach.pro-skills',compact('personal_details','mentor','count','diary','num_attach','attachment'
        ,'img','pskills','pskill_count'));
    }

    public function alpha_add(Request $request)
    {

      $prof_skill = new prof_skill;

      $prof_skill->reg_num         = $request->getRegNum;
      $prof_skill->title           = $request->getTitle;
      $prof_skill->prof_skill_body = $request->getBody;
      $prof_skill->save();
      return 'Done';
    }

    public function show_prof_skill($id)
    {
      $pskills = DB::table('prof_skills')->select('*')->where('reg_num',Auth::user()->reg_num)->get();
      $pskill_count = DB::table('prof_skills')->where('reg_num',Auth::user()->reg_num)->count('reg_num');
 // sidebar queries
      $pinfo = DB::table('prof_skills')->select('*')->where([['id',$id],['reg_num',Auth::user()->reg_num]])->first();
        return view('attach.show-pro-skills', compact('personal_details','mentor','count','diary','num_attach','attachment'
        ,'img','pskills','pskill_count','pinfo'));
    }

    public function edit_prof_skill($id)
    {

      $img = DB::table('personalDetail')->select('img')->where('reg_num',Auth::user()->reg_num)->get();
      $pskills = DB::table('prof_skills')->select('*')->where('reg_num',Auth::user()->reg_num)->get();
      $pskill_count = DB::table('prof_skills')->where('reg_num',Auth::user()->reg_num)->count('reg_num');
 // sidebar queries
      $pinfo = DB::table('prof_skills')->select('*')->where([['id',$id],['reg_num',Auth::user()->reg_num]])->first();
      return view('attach.edit-pro-skills',compact('personalDetail','mentor','count','diary','num_attach','attachment'
      ,'img','pskills','pskill_count','pinfo'));
    }

    public function update_prof_skill(Request $request,$id)
    {
      $prof_skill = prof_skill::find($id);
      $prof_skill->reg_num         = $request->getRegNum;
      $prof_skill->title           = $request->getTitle;
      $prof_skill->prof_skill_body = $request->getBody;
      $prof_skill->save();
        return 'Record SuccessFully Saved';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return bool
     */
    public function intern_notify(Request $request)
    {

      $data =  $request->all();

      $data['student'] =  Auth::user()->full_name();


      Notification::route('mail', $request->employer_email)
      ->notify(new InternNotify((object)$data));

      return  response()->json('mail sent!!');
    }
    public function endorse_intern($token)
    {
        $fetch = DB::table('attachments')->join('users','attachments.reg_num','=','users.reg_num')
        ->join('personalDetail','attachments.reg_num','=','personalDetail.reg_num')
        ->select('attachments.*','attachments.id as rec','users.*','personalDetail.img')->where('attachments.verifyToken',$token)->first();
        return view('attach.endorse',compact('fetch'));
    }
    public function update_endorse_intern(Request $request,$token,$rec)
    {
            $time = time();
            $attachment = attachment::find($rec);
            $attachment->org_name  = $request->org;
            $attachment->status  = $request->endorse;
            $attachment->emp_name   = $request->emp_name;
            $attachment->pos_held = $request->position;
            $attachment->emp_prof_status = $request->prof_s;
            $attachment->date_endorsed = $request->current_date;
            $attachment->comments = $request->comments;
            $attachment->verifyToken = Crypt::encryptString($time);
            $attachment->save();
            return 'Done';
    }
}
