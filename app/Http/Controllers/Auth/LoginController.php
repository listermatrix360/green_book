<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */


    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function postLogin(Request $request) {
        //validate request
        try {
            $this->validate($request, [
                'login' => 'required',
                'password' => 'required'
            ]);
        } catch (ValidationException $e) {
        }



        // get our login input
        $login = $request->input('login');

        // check login field
        $login_type = filter_var( $login, FILTER_VALIDATE_EMAIL ) ? 'email' : 'reg_num';

        // merge our login field into the request with either email or username as key
        $request->merge([ $login_type => $login ]);

        $credentials = $request->only($login_type, 'password');

        //check the user trying to login
        if (isset($credentials['reg_num']) && !empty($credentials['reg_num']))
            $user = User::query()->where([
                ['reg_num', '=', $credentials['reg_num']]
            ])->first();
        else

            $user = User::query()->where([
                ['email', '=', $credentials['email']]
            ])->first();

        if (!empty($user) && $user->is_locked) {
            //process locked user
            $request->session()->put('locked_user_id', $user->id);
            return view('locked');
        }

        if (Auth::attempt($credentials))
        {
            $user = Auth::user();
            $user->last_login_date = Carbon::now();
            if (
                empty($user->wrong_password_attempt_count) ||
                $user->wrong_password_attempt_count > 0
            ) {
                $user->wrong_password_attempt_count = 0;
            }
            $user->save();


            if ($user->must_change_password) {
                return 'must change password';
            } else {
//                return redirect()->route('dashboard.index');
                return redirect()->intended(route('book.index'));

            }
        } else {

            if (!empty($user)) {

                if (empty($user->wrong_password_attempt_count)) {
                    $user->wrong_password_attempt_count = 0;
                }
                $user->wrong_password_attempt_count++;
                //lock user password after 21 trial
                if ($user->wrong_password_attempt_count >= 30) {
                    $user->is_locked = true;
                }
                $user->save();
            }


            return redirect()->back()
                ->withInput($request->only('login', 'remember'))
                ->withErrors(['login' => Lang::get('auth.failed')]);
        }
    }


    public function logout(Request $request)
    {

        auth()->logout();
        session()->flush();
        session()->invalidate();
        return redirect()->route('login');
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout','userlogout');
    }

}
