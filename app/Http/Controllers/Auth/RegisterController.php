<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }






    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [
    //         'reg_num' => 'required|string|max:10',
    //         'title' => 'required|string|max:10',
    //         'surname' => 'required|string|max:50',
    //         'othernames' => 'required|string|max:50',
    //         'email' => 'required|string|email|max:255|unique:users',
    //     ]);
    // }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    // protected function create(array $data)
    // {
    //     return User::create([
    //       'reg_num'     => $data['reg_num'],
    //       'level'       => $data['level'],
    //       'title'       => $data['title'],
    //       'surname'     => $data['surname'],
    //       'othernames'  => $data['othernames'],
    //       'email'       => $data['email'],
    //       'password'    => bcrypt('regent123'),
    //     ]);
    // }

    public function AddUsers(Request $request)
    {

      $registration = new User;
      $registration->reg_num    = $request->reg_num;
      $registration->title      = $request->title;
      $registration->level      = $request->level;
      $registration->surname    = $request->surname;
      $registration->othernames = $request->othernames;
      $registration->email      = $request->email;
      $registration->password   = bcrypt('regent123');
      $registration->save();
       return 'Record Saved';

    }


}
