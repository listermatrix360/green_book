<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AutoBio;
use Auth;
use DB;
class AutoBioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get = DB::table('auto_bios')->select('*')->where('reg_num',Auth::user()->reg_num)->first();
        return view('biography.index',compact('get'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $biography = new AutoBio;
      $biography->reg_num = Auth::user()->reg_num;
      $biography->auto_body = $request->textbody;
      $biography->save();
      return 'Biography Saved Successfully !!';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $autobio = AutoBio::where('reg_num',Auth::user()->reg_num)
        ->update([
                  'auto_body' => $request->editbody,

        ]);
          return 'Edited Successfully !!';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
