<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\commservice;
use App\chapel;
use App\chaplain;
use App\community_remark;
use App\chapel_remark;
use App\physical_fit;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class CommunityServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function chaplain()
    {
          $fetch = DB::table('chaplains')->join('staff','chaplains.staff_id','=','staff.staff_id')
          ->select('staff.email')->first();
          return response()->json($fetch);
    }

    public function warden(Request $request)
    {
          $get = DB::table('societies')->join('staff','societies.staff_id','=','staff.staff_id')
          ->select('staff.email')->where('soc_name',$request->society)->first();

          return response()->json($get);
    }

    public function update_verify(Request $request)
    {
      $t=time();

      $update = commservice::where('verifyToken',$request->hashed_token)
      ->update(['status'=>$request->action,
                'comment'=>$request->comment,
                'verifyToken'=> Crypt::encryptString($t)
              ]);
              return redirect(route('welcome-home'));
    }

    public function warden_update_verify(Request $request)
    {
      $t=time();
      $update = community_remark::where('verifyToken',$request->hashed_token)
      ->update([

                'chaplain_remark'=>$request->comment,
                'verifyToken'=> Crypt::encryptString($t)
              ]);
              return redirect(route('welcome-home'));
    }

        // function for chaplain when sending his remark in the chapel_remark table
    public function remark_chaplain(Request $request)
    {
      $t=time();
      $update = chapel_remark::where('verifyToken',$request->hashed_token)
      ->update([

                'chaplain_remark'=>$request->comment,
                'verifyToken'=> Crypt::encryptString($t)
              ]);
              return redirect(route('welcome-home'));
    }


    public function chapel_update_verify(Request $request)
    {
      $t=time();

      $update = chapel::where('verifyToken',$request->hashed_token)
      ->update(['status'=>$request->action,
                'comment'=>$request->comment,
                'verifyToken'=> Crypt::encryptString($t)
              ]);
              return redirect(route('welcome-home'));
    }

      public function physical_update_verify(Request $request)
    {
      $t=time();

      $update = physical_fit::where('verifyToken',$request->hashed_token)
      ->update(['status'=>$request->action,
                'comment'=>$request->comment,
                'verifyToken'=> Crypt::encryptString($t)
              ]);
              return redirect(route('welcome-home'));
    }

    public function index()
    {

      $user = Auth::user();


      $community = DB::table('commservices')->select('*')->where('reg_num',Auth::user()->reg_num)->get();

      $remarks = DB::table('community_remarks')->select('*')->where('reg_num',Auth::user()->reg_num)->first();
      $details = $user->personal_detail;

      return view('health_fitness.community',compact('community','remarks','details'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function remark_store(Request $request)
    {   $t = time();
        $remarks = new community_remark;
        $remarks->reg_num = $request->getRegNum;
        $remarks->student_remark = $request->getRemark;
        $remarks->verifyToken = Crypt::encryptString($t);
        $remarks->save();
        return 'Done';
    }

    public function remark_update(Request $request)
    {
        $remarks = community_remark::where('reg_num',$request->getRegNum)
        ->update(['student_remark' => $request->getRemark]);
        return 'Done';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   $t = time();
        $commservices = new commservice;
        $commservices->reg_num = Auth::user()->reg_num;
        $commservices->date = $request->date;
        $commservices->activity = $request->activity;
        $commservices->verifyToken = Crypt::encryptString($t);
        $commservices->save();
        return 'Done';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($formID)
    {
        $getCommData = DB::table('commservices')->select('*')
        ->where([['reg_num',Auth::user()->reg_num],['frmID',$formID]])->get();


        return response()->json($getCommData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($rid)
    {
        $getEditRecord = DB::table('commservices')->select('*')
        ->where('id',$rid)->get();
        return response()->json($getEditRecord);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $rid)
    {
        $update = commservice::where('id',$rid)
        ->update(['date'=>$request->getDate,
                  'activity'=>$request->getActivity,
                  'comment'=>'null',
                  'status'=>'pending']);

            return 'Edited Successfully';
    }

    public function token($token)
    {
        $Token_GET = DB::table('commservices')->select('*')
        ->where([['verifyToken',$token],['reg_num',Auth::user()->reg_num]])->get();
        return response()->json($Token_GET);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function physical()
    {
        $user = Auth::user();

        $details = $user->personal_detail;
        $remark = DB::table('physical_remarks')->select('*')->where('reg_num',Auth::user()->reg_num)->first();
        $data   = DB::table('physical_fits')->select('*')->where('reg_num',Auth::user()->reg_num)->get();
        return view('health_fitness.physical',compact('data','remark','details'));
    }

    public function post_phyiscal_data(Request $request)
    {
      $t=time();
      $physical = new physical_fit;
      $physical->reg_num = Auth::user()->reg_num;
      $physical->date = $request->getDate;
      $physical->activity = $request->getActivity;
      $physical->verifyToken = Crypt::encryptString($t);
      $physical->save();
      return 'Done Recorded';
    }

    public function physical_edit($rid)
    {
      $getPhysicalRecord = DB::table('physical_fits')->select('*')
      ->where('id',$rid)->get();
      return response()->json($getPhysicalRecord);
    }

    public function physical_update(Request $request,$rid)
    {
      $update = physical_fit::where('id',$rid)
      ->update(['date'=>$request->getDate,
                'activity'=>$request->getActivity,
                'comment'=>'null',
                'status'=>'pending']);
          return 'Edited Successfully!!!';
    }

    public function spiritual()
    {
        $user = Auth::user();

        $details = $user->personal_detail;
        $remark  = DB::table('chapel_remarks')->select('*')->where('reg_num',Auth::user()->reg_num)->first();
        $spirit  = DB::table('chapels')->select('*')->where('reg_num',Auth::user()->reg_num)->get();

      return view('health_fitness.spirit',compact('remark','details','spirit'));
    }

    public function ChapelSave(Request $request)
    {
      $t=time();

      $chapel = new chapel;
      $chapel->reg_num = Auth::user()->reg_num;
      $chapel->date = $request->getDate;
      $chapel->activity = $request->getActivity;
      $chapel->verifyToken = Crypt::encryptString($t);
      $chapel->save();
      return 'Done';
    }

    public function chapel_edit($rid)
    {
      $getChapelRecord = DB::table('chapels')->select('*')
      ->where('id',$rid)->get();
      return response()->json($getChapelRecord);
    }

    public function spirit_update(Request $request,$rid)
    {
      $update = chapel::where('id',$rid)
      ->update(['date'=>$request->getDate,
                'activity'=>$request->getActivity,
                'comment'=>'null',
                'status'=>'pending']);
          return 'Edited Successfully';
    }

    public function spirit_token($token)
    {
      $return_token = DB::table('chapels')->select('*')
      ->where([['verifyToken',$token],['reg_num',Auth::user()->reg_num]])->get();
      return response()->json($return_token);
    }


       public function physical_token($token)
    {
      $return_token = DB::table('physical_fits')->select('*')
      ->where([['verifyToken',$token],['reg_num',Auth::user()->reg_num]])->get();
      return response()->json($return_token);
    }

    public function chapel_remark(Request $request)
    {   $t=time();
        $remarks = new chapel_remark;
        $remarks->reg_num = Auth::user()->reg_num;
        $remarks->student_remark = $request->getRemark;
        $remarks->verifyToken = Crypt::encryptString($t);
        $remarks->save();
        return 'Done';
    }

    public function chapel_update_remark(Request $request)
    {
        $remarks = chapel_remark::where('reg_num',Auth::user()->reg_num)
        ->update(['student_remark' => $request->getRemark]);
        return 'Done';
    }


}
