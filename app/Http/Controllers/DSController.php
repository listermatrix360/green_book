<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\society;
use App\User;
use App\Staff;
use Excel;
class DSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $ment_list = DB::table('staff')->select('*')->where('status','1')->get();
      $soc = DB::table('societies')->join('staff','societies.staff_id','=','staff.staff_id')
      ->select('societies.name','societies.id as sid','staff.*')->get();
      return view('admin.dept-soc.ds',compact('soc','ment_list','name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get($sid)
    {

      $soc = DB::table('societies')->join('staff','societies.staff_id','=','staff.staff_id')
      ->select('societies.soc_name','societies.id as sid','staff.*')
      ->where('societies.id',$sid)->first();
      return response()->json($soc);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $enter = new society;
      $enter->name = $request->input('name');
      $enter->staff_id = $request->input('staff_id');
      $enter->save();
        return 'Done';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $sid)
    {
      $enter = society::find($sid);
      $enter->soc_name = $request->soc_name;
      $enter->staff_id = $request->staff_id;
      $enter->save();
        return 'Done';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function import(Request $request)
    {
      if($request->hasFile('bulky-file')){
        $path = $request->file('bulky-file')->getRealPath();
        $data = \Excel::load($path)->get();
          if($data->count()){
            foreach ($data as $key =>$value){
              $user_list[ ] = [
                              'reg_num'    => $value->reg_num,
                              'level'      => $value->level,
                              'title'      => $value->title,
                              'surname'    => $value->surname,
                              'othernames' => $value->othernames,
                              'email'      => $value->email,
                              'password'   => bcrypt('regent123')
                            ];
            }
            if(!empty($user_list)){
              User::insert($user_list);
              return redirect('/admin');
            }
          }
          else{
            return 'There is an Error';
          }
      }
      return 'It Came here!!';

    }

    public function import_staff(Request $request)
    {
      if($request->hasFile('staff-file')){
        $path = $request->file('staff-file')->getRealPath();
        $data = \Excel::load($path)->get();
          if($data->count()){
            foreach ($data as $key =>$value){
              $user_list[ ] = [
                              'staff_id'     => $value->staff_id,
                              'title'        => $value->title,
                              'f_name'       => $value->f_name,
                              'las_name'     => $value->las_name,
                              'd_code'       => $value->d_code,
                              'email'        => $value->email,
                              'phone'        => $value->phone,
                            ];
            }
            if(!empty($user_list)){
              Staff::insert($user_list);
              return redirect('/admin');
            }
          }
          else{
            return 'There is an Error';
          }
      }
      return 'It Came here!!';

    }

}
