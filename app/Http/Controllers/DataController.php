<?php

namespace App\Http\Controllers;
use DataTables;
use Illuminate\Http\Request;
use App\DataTables\RegisDataTable;
use App\DataTables\ApprovedDataTable;
use App\DataTables\RejectDataTable;
use App\DataTables\PendingDataTable;
use App\DataTables\staffDataTable;
use App\DataTables\DisabledDataTable;
use App\DataTables\JnrApprovedDataTable;
use App\DataTables\JnrRejectedDataTable;
use App\DataTables\JnrPendingDataTable;
use App\DataTables\JnrDataTable;
use App\DataTables\GraduatedDataTable;
class DataController extends Controller
{
  public function graduated(GraduatedDataTable $dataTable)
  {
      return $dataTable->render('admin.tables.graduated');
  }

  public function reject(RejectDataTable $dataTable)
  {
      return $dataTable->render('admin.tables.bounce');
  }

  public function json(RegisDataTable $dataTable)
  {
      return $dataTable->render('admin.tables.registered');
  }

  public function approve(ApprovedDataTable $dataTable)
  {
      return $dataTable->render('admin.tables.approve');
  }
  public function pending(PendingDataTable $dataTable)
  {
      return $dataTable->render('admin.tables.pendin');
  }

  public function staff(staffDataTable $dataTable)
  {
      return $dataTable->render('admin.add-staff');
  }

  public function staff_disabled(DisabledDataTable $dataTable)
  {
      return $dataTable->render('admin.add-staff');
  }

  //dataTables for other levels
  public function Jnr_reject(JnrRejectedDataTable $dataTable)
  {
      return $dataTable->render('admin.tables.j-reject');
  }
  public function Jnr_approve(JnrApprovedDataTable $dataTable)
  {
      return $dataTable->render('admin.tables.j-approve');
  }
  public function Jnr_pending(JnrPendingDataTable $dataTable)
  {
      return $dataTable->render('admin.tables.Jnrpending');
  }
  public function Jnr_json(JnrDataTable $dataTable)
  {
      return $dataTable->render('admin.tables.lowlist');
  }
}
