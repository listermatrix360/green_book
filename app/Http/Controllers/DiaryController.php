<?php /** @noinspection PhpUndefinedMethodInspection */

namespace App\Http\Controllers;
use App\Diary;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class DiaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */


    public function index()
    {
        return view('diary.diaryindex');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {


        return view('diary.story');

    }


    public function store(Request $request)
    {
        $data = $request->validate([
            'date' =>'required',
            'title' =>'required',
            'body' =>'required',
        ]);



        $user = Auth::user();

        DB::beginTransaction();

        $user->diary()->create($data);

        DB::commit();


        return redirect()->back()->with('success',"Diary : {$request->input('title')} successfully created");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

        return view('diary.show', compact('personal_details','mentor','diary','dinfo','count','num_attach','attachment','img','pskills','pskill_count'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
          $user = Auth::user();
          $user_diary = $user->diary()->find($id);

      return view('diary.edit',compact('user_diary'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return void
     */
    public function update(Request $request, $id)
    {


       $diary = Diary::find($id);

        $data = $request->validate([
            'date' =>'required',
            'title' =>'required',
            'body' =>'required',
        ]);

        DB::beginTransaction();

        $diary->update($data);

        DB::commit();

        return redirect()->back()->with('success',"Diary : {$request->input('title')} successfully updated");


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
