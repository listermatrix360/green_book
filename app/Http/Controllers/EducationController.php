<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\educ_exam;
use App\othcourses;
use DB;
use Auth;
class EducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('education.edu');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $educ_exam = new educ_exam;

        $educ_exam->reg_num = $request->getRegNum;
        $educ_exam->st_dt = $request->getDate1;
        $educ_exam->en_dt = $request->getDate2;
        $educ_exam->sch_name = $request->getSchool;
        $educ_exam->study_mtd = $request->getStudy;
        $educ_exam->ex_taken = $request->getExam;
        $educ_exam->level = $request->getLevel;
        $educ_exam->paper = $request->getPaper;
        $educ_exam->results = $request->getResult;
        $educ_exam->remarks = $request->getRemark;
        $educ_exam->save();
        return 'Done';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $show = DB::table('educ_exams')->select('*')
      ->where([['reg_num',Auth::user()->reg_num],['id',$id]])->first();
      return view('education.show-edu',compact('show'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = DB::table('educ_exams')->select('*')
        ->where([['reg_num',Auth::user()->reg_num],['id',$id]])->first();
        return view('education.edit-edu',compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $educ_exam = educ_exam::where('id',$id)
      ->update([
        'st_dt'    => $request->getDate1,
        'en_dt'    => $request->getDate2,
        'sch_name' => $request->getSchool,
        'study_mtd'=> $request->getStudy,
        'ex_taken' => $request->getExam,
        'level'    => $request->getLevel,
        'paper'    => $request->getPaper,
        'results'  => $request->getResult,
        'remarks'  => $request->getRemark,
      ]);
      return 'Updated';
    }


    public function SeeOtherCourses()
    {
      return view('education.courses');
    }

    public function postOtherCourses(Request $request)
    {
      $othcourses = new othcourses;

      $othcourses->reg_num    = $request->getRegNum;
      $othcourses->date1      = $request->getDate1;
      $othcourses->date2      = $request->getDate2;
      $othcourses->cours_name = $request->getCourse;
      $othcourses->descrip    = $request->getDes;
      $othcourses->remarks    = $request->getRemarks;

      $othcourses->save();
      return 'Done';
    }

    public function showOtherCourses($id)
    {
      $item = DB::table('othcourses')->select('*')->where([['id',$id],['reg_num',Auth::user()->reg_num]])
      ->first();
      return view('education.show-course',compact('item'));
    }


    public function EditOtherCourses($id)
    {
      $item = DB::table('othcourses')->select('*')->where([['id',$id],['reg_num',Auth::user()->reg_num]])
      ->first();
      return view('education.edit-course',compact('item'));
    }

    public function updateOtherCourses(Request $request,$id)
    {
        return $id;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
