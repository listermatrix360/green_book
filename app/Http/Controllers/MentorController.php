<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\mentor;
use App\users;
class MentorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */


    public function index()
    {
        return Auth::user()->reg_num;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {



          $ment_list = DB::table('staff')->select('*')->where('status','1')->get();

          $l1 = DB::table('mentors')->join('staff','mentors.staff_id','=','staff.staff_id')
          ->select('mentors.*','staff.title','staff.f_name','staff.las_name','staff.email')
          ->where([['mentors.reg_num', '=', Auth::user()->reg_num],['mentors.level','=','100']])->first();

          $l2 = DB::table('mentors')->join('staff','mentors.staff_id','=','staff.staff_id')
          ->select('mentors.*','staff.title','staff.f_name','staff.las_name','staff.email')
          ->where([['mentors.reg_num', '=', Auth::user()->reg_num],['mentors.level','=','200']])->first();

         $l3 = DB::table('mentors')->join('staff','mentors.staff_id','=','staff.staff_id')
          ->select('mentors.*','staff.title','staff.f_name','staff.las_name','staff.email')
          ->where([['mentors.reg_num', '=', Auth::user()->reg_num],['mentors.level','=','300']])->first();

          $l4 = DB::table('mentors')->join('staff','mentors.staff_id','=','staff.staff_id')
          ->select('mentors.*','staff.title','staff.f_name','staff.las_name','staff.email')
          ->where([['mentors.reg_num', '=', Auth::user()->reg_num],['mentors.level','=','400']])->first();

          return view('mentor.mentmess',compact('l1','l2','l3','l4','ment_list'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
      $t = time();
      $mentor = new mentor;
      $mentor->verifyToken = Crypt::encryptString($t);
      $mentor->reg_num = Auth::user()->reg_num;
      $mentor->level = $request->level;
      $mentor->staff_id = $request->ment_name;
      $mentor->save();
      return 'Saved';
    }

    /**
     * Display the specified resource.
     *
     * @param $rid
     * @return Response
     */
    public function show($rid)
    {
        $mentor = DB::table('mentors')->select('*')
        ->where([['reg_num',Auth::user()->reg_num],['id',$rid]])->first();

        return response()->json($mentor);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return int
     */
    public function edit($id)
    {
        $mentor =  mentor::query()->find($id);
        return response()->json($mentor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$rid)
    {
        $t = time();
          $mentor_update = mentor::where([['level',$request->level],['reg_num',Auth::user()->reg_num],['id',$rid]])
          ->update(['staff_id'=>$request->ment_name,'verifyToken'=>Crypt::encryptString($t),'status'=>'pending'
          ,'staff_comm'=>'null']);

          return 'Updated Successfully';

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $token
     * @return Response
     */
    public function getemail($token)
    {
            $query =  DB::table('mentors')->join('staff','mentors.staff_id','staff.staff_id')
            ->where('verifyToken',$token)->select('staff.email','mentors.staff_id','mentors.verifyToken')->first();

            return response()->json($query);
    }
}
