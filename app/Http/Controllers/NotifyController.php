<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Notifications\QuickNotify;
use Notification;
class NotifyController extends Controller
{
    public function notify(Request $request)
    {
    	$users = User::all();
      // $users = User::find(1);
    	$message = $request->info;

    	// $users->notify(new QuickNotify($message));
      Notification::send($users, new QuickNotify($message));
      return 'Notification Sent!!';

    }


}
