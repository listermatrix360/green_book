<?php /** @noinspection PhpUndefinedMethodInspection */

namespace App\Http\Controllers;
use App\Program;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\personalDetail;
use App\User;
use App\mentor;
use App\aca_prog;
class PagesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Authenticatable
     */


    public function welcome()
    {
        return view('default');
    }


    public function index()
    {
        $user = Auth::user();

        $personal_details =  $user->personal_detail;

        $profile_status = DB::table('profile_statuses')->select('*')->where('reg_num',Auth::user()->reg_num)->first();
        $pass  = DB::table('users')->select('password')->where('reg_num',Auth::user()->reg_num)->first();
        $defer = DB::table('users')->select('defer_status')->where('reg_num',Auth::user()->reg_num)->first();
        $event = DB::table('events')->select('*')->get();

            return view('book.dashboard', compact('profile_status','defer','event','personal_details','pass'));

    }

    public function help()
    {
      return view('book.help');
    }



    public function create(Request $request)
    {

        $user = Auth::user();

        $prog = Program::query()->get();
        $school = DB::table('schools')->select('*')->get();
        $societies = DB::table('societies')->select('*')->get();
        $soc = DB::table('societies')->select('*')->get();




        return !empty($user->personal_detail) ? redirect()->route('personal.show'): ($request->isMethod('post') ? $this->store($request) : view('book.create',compact('societies','prog','school','soc')));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();
        $user = Auth::user();

        if (empty($user->personal_detail)) {
            $user->personal_detail()->create($data);
        }
        else{
            $user->personal_detail()->update($data);
        }

       return  redirect()->route('personal.show');
    }


    public function show()
    {

        $personal_details = PersonalDetail::query()->select('*')
        ->where('reg_num', Auth::user()->reg_num)->first();

        $item =  PersonalDetail::query()->where('reg_num',Auth::user()->reg_num)->first();
        $img =  PersonalDetail::query()->select('img')->where('reg_num',Auth::user()->reg_num)->get();

        return view('book.show',compact('item','personal_details','img'));



    }





    public function displayMentor($id)
    {
       $mentor = mentor::find($id);

      return view('book.mentshow',compact('mentor'));

    }



    public function edit(Request $request)
    {

          $user = Auth::user();

          $school = DB::table('schools')->select('*')->get();
          $item = PersonalDetail::query()->where('reg_num',$user->reg_num)->first();
          $soc = DB::table('societies')->select('*')->get();


          return $request->isMethod('post') ? $this->update($request) : view('book.edit',compact('soc','item','school'));
    }


     public function getFile(Request $request){

          $request->validate([
            'picture' => 'mimes:jpeg,bmp,png',
          ]);

         $filename = $request->picture->hashName();
         $filesize = $request->picture->getClientSize();
         $request->file('picture');
         $request->picture->storeAs('public/greenFolder',$filename);
         $update = PersonalDetail::query()->where('reg_num',Auth::user()->reg_num)
         ->update(['img'=>$filename]);

         return redirect()->back();


     }

     public function show_file()
     {
       return Storage::allFiles('public');
     }

    public function update(Request $request)
    {
        $data = $request->except('_token');
        $user = Auth::user();
        if (isset($user->personal_detail)) {
            $user->personal_detail->update($data);
        }

      return redirect()->route('personal.show');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = personalDetail::find($id);
        $item->delete();
        session()->flash('message','Deleted Successfully');
        return redirect('/book');
    }
    public function password(Request $request)
    {
      $personal_details = User::where('reg_num',Auth::user()->reg_num)
      ->update(['password'=>bcrypt($request->first)]);
      return 'Done';
    }
}
