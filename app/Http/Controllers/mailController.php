<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Mail;
use App\Mail\WardMail;
use App\Mail\ComMail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Crypt;
use App\Mail\SpiritMail;
use App\Mail\ChapReqMail;
use App\Mail\PhysicalMail;
use App\Mail\ment;
use App\users;
use App\mentor;
use Auth;
use DB;
class mailController extends Controller
{
    public function fetchUser(array $data){
    //
    }

    public function chap_request()
    {
      mail::send(new ChapReqMail());
      session()->flash('message','Mail Successfully Sent');

    }

    public function remark_chap_val_request($token)
    {
      $chapelActivity = DB::table('chapel_remarks')->join('chapels','chapel_remarks.reg_num','=','chapels.reg_num')
      ->join('users','chapel_remarks.reg_num','=','users.reg_num')
      ->select('chapels.*','users.name','users.la_name','chapel_remarks.verifyToken as token')->where('chapel_remarks.verifyToken',$token)->get();
      return view('health_fitness.ChaplainRemarkVerify',compact('chapelActivity'));
    }

    public function send()
    {
        mail::send(new ment());
        session()->flash('message','Mail Successfully Sent');

    }

    public function mentor_email(Request $request,$token)
    {
    $user = DB::table('mentors')
    ->join('users','mentors.reg_num','=','users.reg_num')
    ->join('personal_details','users.reg_num','=','personal_details.reg_num')
    ->select('mentors.*','mentors.level as mlevel','users.*','program','img')
    ->where('mentors.verifyToken',$token)->first();
    return view('mentor.staff-approval',compact('user'));
    }

    public function update(Request $request){
      $t = time();
       $update = mentor::where(['verifyToken'=>$request->hashed_token])
       ->update(['status'=>$request->action,'staff_comm'=>$request->comment,'verifyToken'=>Crypt::encryptString($t)]);
       return redirect('/');
    }

    public function sendComm()
    {
        mail::send(new ComMail());
        return 'Mail Successfully Sent';
    }


    public function wardComm()
    {
        mail::send(new WardMail());
        return 'Mail Successfully Sent';
    }
    public function spiritComm()
    {
        mail::send(new SpiritMail());
        return 'Mail Successfully Sent';
    }

    public function PhysicalComm()
    {
        mail::send(new PhysicalMail());
        return 'Mail Successfully Sent';
    }

    //view returned after staff clicks the button in the email for community service

    public function community_val_request($token)
    {
        $community = DB::table('commservices')
        ->join('users','commservices.reg_num','=','users.reg_num')
        ->join('personal_details','users.reg_num','personal_details.reg_num')
        ->select('users.title','users.surname','users.othernames','commservices.*','img','program')
        ->where('commservices.verifyToken',
        $token)->first();
        return view('health_fitness.ComServiceVerify',compact('community'));
    }

    //view returned after staff clicks the button in the email for Chaplain service

    public function chapel_val_request($token)
    {
        $chapel = DB::table('chapels')
        ->join('users','chapels.reg_num','=','users.reg_num')
        ->join('personal_details','users.reg_num','personal_details.reg_num')
        ->select('users.title','users.surname','users.othernames','program','img','chapels.*')
        ->where('chapels.verifyToken',$token)->first();
        return view('health_fitness.ChapelServiceVerify',compact('chapel'));
    }

    //view returned after staff clicks the button in the email for Physical Fitness
    public function physical_val_request($token)
    {
        $physical = DB::table('physical_fits')
        ->join('users','physical_fits.reg_num','=','users.reg_num')
        ->join('personal_details','users.reg_num','personal_details.reg_num')
        ->select('users.title','users.surname','users.othernames','program','img','physical_fits.*')
        ->where('physical_fits.verifyToken',$token)->first();
        return view('health_fitness.PhysicalFitsVerify',compact('physical'));
    }

    //view returned after staff clicks the button in the email for warden_of Society
    public function warden_val_request($token)
    {
        $data = DB::table('personal_details')->join('community_remarks','personal_details.reg_num','=','community_remarks.reg_num')
        ->select('img','program')->where('community_remarks.verifyToken','=',$token)->first();
        $warden = DB::table('community_remarks')->join('commservices','community_remarks.reg_num','=','commservices.reg_num')
        ->join('users','community_remarks.reg_num','=','users.reg_num')->join('personal_details','users.reg_num','=','personal_details.reg_num')
        ->select('commservices.*','users.surname','users.othernames','community_remarks.verifyToken as token','program','img')
        ->where('community_remarks.verifyToken',$token)->get();
        return view('health_fitness.WardenServiceVerify',compact('warden','data'));
    }

}
