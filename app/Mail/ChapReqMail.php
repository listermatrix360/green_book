<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\request;
use Auth;
class ChapReqMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        return $this->view('emails.ChaplainRemarkEmail',['reg_num'=>$request->id,
        'sender_name'=>Auth::user()->othernames,'sender_last_name'=>Auth::user()->surname,'sender_token'=>$request->code])
        ->subject('Community Chapel Service Endorse')->to($request->email);
    }
}
