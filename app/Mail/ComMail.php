<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\request;
use Auth;
class ComMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        return $this->view('emails.serviceEmail',['msg'=>$request->getActivity,'reg_num'=>$request->getRegNum,
        'sender_name'=>$request->name,'sender_token'=>$request->getToken,'date'=>$request->getDate])
        ->subject('Community Service Verification')->to('listermatrix360@gmail.com');
    }
}
