<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\request;
use Auth;
class PhysicalMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
     public function build(Request $request)
    {
        return $this->view('emails.PhysicalEmail',['msg'=>$request->getActivity,'reg_num'=>$request->getRegNum,
        'sender_name'=>$request->name,'sender_token'=>$request->getToken,'date'=>$request->getDate])
        ->subject('Physical Fitness Verification')->to('ice-t.boateng@regent.edu.gh');
    }
}
