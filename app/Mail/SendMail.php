<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\request;
use App\User;
use App\mentor;
use Auth;
class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(request $request)
    {
        return $this->view('mail',['msg'=>'The Following Student Is Requesting for endorsement','reg_num'=>Auth::user()->reg_num,'sender_name'=>$request->name, 'sender_token'=>$request->token])->subject('Mentor Endorsement Request')->to($request->staff_email);
    }
}
