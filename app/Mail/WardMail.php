<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\request;
use Auth;
class WardMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        $username = Auth::user()->surname .' '. Auth::user()->othernames;
        return $this->view('emails.WardenEmail',['msg'=>$request->id,'reg_num'=>$request->id,
        'sender_name'=>$username,'sender_token'=>$request->code,'date'=>$request->id])
        ->subject('Endorsement Request')->to($request->email);
    }
}
