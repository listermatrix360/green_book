<?php

namespace App\Mail;
use Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\request;
class ment extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(request $request)
    {

      $username = Auth::user()->surname .' '. Auth::user()->othernames;
        return $this->markdown('email',['url'=>route('mentor_email',["token"=>$request->token]),
        'std_name'=>$username,'id'=>Auth::user()->reg_num])
        ->subject('Mentor Endorsement Request')
        ->to($request->staff_email);
    }
}
