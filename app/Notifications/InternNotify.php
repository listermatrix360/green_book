<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class InternNotify extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @param array $data
     */
    // protected $date1,$date2;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
                    ->line('Hello '. $this->data->employer_name. ', our student, '. $this->data->student.'received training  
                        as an intern in your organization from '. $this->data->start_date. ' to ' .$this->data->end_date. '.')
                    ->line('As part of the evaluation assessment, we ask that you complete a form for us which will be used to assess the student.')
                    ->line('Use the link below to complete the form')
                    ->action('Click here', route('intern.clutter',$this->data->verify_token))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
