<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalDetail extends Model
{


    protected $fillable =
        [

            'user_id',
            'reg_num',
            'gender',
            'dob',
            'nationality_id',
            'session_id',
            'program_id',
            'society_id',
            'phone1',
            'phone2',
            'email1',
            'email2',
        ];





    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function program()
    {
        return $this->belongsTo(Program::class);
    }


    public function society()
    {
        return $this->belongsTo(Society::class);
    }

}
