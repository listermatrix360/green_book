<?php

namespace App\Providers;

use App\mentor;
use App\personalDetail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {





        view()->composer('*', function ($view) {

            if(Auth::user()):

                $user = Auth::user();

            $personal_details = personalDetail::query()->select('*')
                ->where('reg_num', Auth::user()->reg_num)->first();
            $mentor = mentor::query()->select('*')->where('reg_num', Auth::user()->reg_num)->get();
            $diary = $user->diary()->get();
            $attachment = $user->attachment()->get();
            $pskills = DB::table('prof_skills')->select('*')->where('reg_num',Auth::user()->reg_num)->get();
            $pskill_count = DB::table('prof_skills')->where('reg_num',Auth::user()->reg_num)->count('reg_num');
            $education = DB::table('educ_exams')->select('*')->where('reg_num',Auth::user()->reg_num)->get();
            $course = DB::table('othcourses')->select('*')->where('reg_num',Auth::user()->reg_num)->get();

            $profile_status = DB::table('profile_statuses')->select('*')->where('reg_num',Auth::user()->reg_num)->first();

            $ctm = DB::table('mentors')->where('reg_num',Auth::user()->reg_num)->count('reg_num');
            $times = count($education);
            $count_courses = count($course);
            $community_count = DB::table('commservices')->where('reg_num',Auth::user()->reg_num)->count();
            $chapel_count = DB::table('chapels')->where('reg_num',Auth::user()->reg_num)->count();
            $physical_count = DB::table('physical_fits')->where('reg_num',Auth::user()->reg_num)->count();
            $defer = DB::table('users')->select('defer_status')->where('reg_num',Auth::user()->reg_num)->first();

            endif;




            $view->with([
                'personalDetail'=> $personal_details ?? null,
                'mentor'=> $mentor ?? null,
                'diary'=> $diary ?? null,
                'attachment'=> $attachment ?? null,
                'pskills'=> $pskills ?? null,
                'pskill_count'=> $pskill_count ?? null,
                'education'=> $education ?? null,
                'course'=> $course ?? null,
                'profile_status'=> $profile_status ?? null,
                'count'=> $count ?? null,
                'ctm'=> $ctm ?? null,
                'times'=> $times ?? null,
                'count_courses'=> $count_courses ?? null,
                'community_count'=> $community_count ?? null,
                'chapel_count'=> $chapel_count ?? null,
                'physical_count'=> $physical_count ?? null,
                'defer'=> $defer  ?? null,
                'personal_details'=> $personal_details  ?? null
            ]);


        });

    }
}
