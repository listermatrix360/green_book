<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class School extends Model
{
    protected $fillable = ['name'];

    use SoftDeletes;


    public function departments()
    {
        return $this->hasMany(Department::class);
    }
}
