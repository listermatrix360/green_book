<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Society extends Model
{
    public function personal()
    {
        return $this->hasOne(PersonalDetail::class);
    }
}
