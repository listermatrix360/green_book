<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'reg_num','la_name','surname','title' ,'last_login_date','defer_status', 'email', 'password','title','level'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function full_name()
    {
        return trim("$this->surname $this->othernames");
    }



    public function department()
    {
        return $this->belongsTo(Department::class);
    }


    public function personal_detail()
    {
        return $this->hasOne(PersonalDetail::class);
    }


    public function diary()
    {
        return $this->hasMany(Diary::class);
    }

    public function attachment()
    {
        return $this->hasMany(Attachment::class);
    }



}
