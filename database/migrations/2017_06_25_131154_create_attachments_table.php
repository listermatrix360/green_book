<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
//            $table->string('name');
            $table->string('employer_name');
            $table->string('employer_email');
            $table->string('address');
            $table->string('department');
            $table->string('start_date');
            $table->string('end_date');
            $table->string('job_title');
            $table->string('experience_area');
            $table->string('work_summary');
            $table->string('organisation_name')->nullable();
            $table->string('date1')->nullable();
            $table->string('date2')->nullable();
            $table->string('position_held')->nullable();
            $table->string('employee_prof_status')->nullable();
            $table->string('date_endorsed')->nullable();
            $table->string('comments')->nullable();
            $table->string('status')->nullable()->default('pending');
            $table->string('verifyToken')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachments');
    }
}
