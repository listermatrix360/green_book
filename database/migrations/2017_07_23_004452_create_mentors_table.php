<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMentorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mentors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reg_num');
            $table->string('level');
            $table->string('staff_id');
            $table->string('status');
            $table->string('staff_comm');
            $table->string('verifyToken');
            $table->foreign('reg_num')->references('reg_num')->on('users')->onUpdate('cascade')->onDelete('no action');
            $table->foreign('staff_id')->references('staff_id')->on('staff')->onUpdate('cascade')->onDelete('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mentors');
    }
}
