<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reg_num')->unique();
            $table->string('name');
            $table->string('prof_status');
            $table->string('ess_comm');
            $table->foreign('reg_num')->references('reg_num')->on('users')->onUpdate('cascade')->onDelete('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_statuses');
    }
}
