<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('educ_exams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reg_num');
            $table->string('st_dt');
            $table->string('en_dt');
            $table->string('sch_name');
            $table->string('study_mtd');
            $table->string('ex_taken');
            $table->string('level');
            $table->string('paper');
            $table->string('results');
            $table->string('remarks');
            $table->timestamps();
            $table->foreign('reg_num')->references('reg_num')->on('users')->onUpdate('cascade')->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('educ_exams');
    }
}
