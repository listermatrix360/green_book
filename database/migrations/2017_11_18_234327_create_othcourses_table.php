<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOthcoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('othcourses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reg_num');
            $table->string('date1');
            $table->string('date2');
            $table->string('cours_name');
            $table->string('desc');
            $table->string('remarks');
            $table->foreign('reg_num')->references('reg_num')->on('users')->onUpdate('cascade')->onDelete('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('othcourses');
    }
}
