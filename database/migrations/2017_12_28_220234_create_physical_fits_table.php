<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhysicalFitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physical_fits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('frmID');
            $table->string('reg_num');
            $table->string('date');
            $table->string('activity');
            $table->string('status');
            $table->string('verifyToken');
            $table->foreign('reg_num')->references('reg_num')->on('users')->onUpdate('cascade')->onDelet('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('physical_fits');
    }
}
