<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChaplainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chaplains', function (Blueprint $table) {
          $table->increments('id');
          $table->string('staff_id')->unique();
          $table->string('chap_name');
          $table->foreign('staff_id')->references('staff_id')->on('staff')->onUpdate('cascade')->onDelete('no action');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chaplains');
    }
}
