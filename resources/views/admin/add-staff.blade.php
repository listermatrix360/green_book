<div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading ft-color">
        <h4 class="cinzel panel-title"><b>Staff Details</b></h4>
      </div>
      <div class="panel-body">
        <form method="post" id="staff_form" action="{{route('staffadd')}}" class="form-horizontal form text-font" role="form">
        {{csrf_field()}}
        <div class="form-group">
          <label for="last_name" class="col-md-4 control-label"><i class="fa fa-graduation-cap prefix" aria-hidden="true"></i> TITLE</label>
          <div class="col-md-6">
            <input name="title" type="text" class="form-control" required>
          </div>
        </div>
        <div class="form-group">
          <label for="last_name" class="col-md-4 control-label"><i class="fa fa-id-card prefix" aria-hidden="true"></i> STAFF ID</label>
          <div class="col-md-6">
            <input name="staff_id" type="number" class="form-control" required>
          </div>
        </div>
        <div class="form-group">
            <label for="first_name" class="col-md-4 control-label"><i class="fa fa-user-circle-o" aria-hidden="true"></i> FIRST NAME</label>
            <div class="col-md-6">
                <input type="text" name="f_name" class="form-control" required>
            </div>
        </div>
        <div class="form-group">
            <label for="first_name" class="col-md-4 control-label"><i class="fa fa-user-circle-o" aria-hidden="true"></i> LAST NAME</label>
            <div class="col-md-6">
                <input type="text" name="las_name" class="form-control" required>
            </div>
        </div>
        <div class="form-group">
            <label for="last_name" class="col-md-4 control-label"><i class="fa fa-book prefix" aria-hidden="true"></i> DEPARTMENT</label>
            <div class="col-md-6">
              <select class="selectpicker"  name="department" data-live-search="true" required>
                 <option value=""></option>
                    @foreach ($dpt as $dpts)
                      <option data-tokens="{{$dpts->d_name}}" value="{{$dpts->d_code}}" name="d_code">{{$dpts->d_name}}</option>
                    @endforeach
              </select>
        </div>
        </div>

        <div class="form-group">
            <label for="last_name" class="col-md-4 control-label"><i class="fa fa-phone prefix" aria-hidden="true"></i> MOBILE</label>
            <div class="col-md-6">
                <input name="phone" type="number" class="form-control" value="{{old('phone')}}" required>
            </div>
        </div>

        <div class="form-group">
            <label for="last_name" class="col-md-4 control-label"><i class="fa fa-envelope prefix" aria-hidden="true"></i> STAFF EMAIL</label>
            <div class="col-md-6">
                <input name="email" type="email" value="{{old('email')}}" class="form-control" required>
            </div>
        </div>

        <div class="text-center">
            <button type="submit" class="btn btn-primary">Save Staff Data</button>
        </div>
        </form>

      </div>
      <!-- <div class="panel-footer">
        Regent University College Of Science and Technology
      </div> -->
</div>
</div>
<div class="col-md-6 col-md-offset-">
  <div class="panel panel-default">
    <div class="panel-heading ft-color">
      <h5 class="cinzel panel-title">Staff Bulk Registeration</h5>
    </div>
    <div class="panel-body">
      <form class="form-inline" id="bulky-file" method="post" enctype="multipart/form-data" action="{{route('excel_staff')}}">
        {{csrf_field()}}
          <div class="form-group">
            <input type="file" class="form-control" name="staff-file" required>
          </div>

          <button type="submit" class="btn ft-color"><i class="fa fa-upload" aria-hidden="true"></i></button>
          </form>
    </div>
  </div>
</div>
<div class="col-md-6">
  <div class="panel panel-default">
    <div class="panel-heading ft-color">
      <h4 class="cinzel panel-title"> Disabled Staff</h4>
    </div>
    <div class="panel-body">
      <table class="table table-striped table-hover table-condensed text-font" id="disabled">
    <thead>
      <tr>
        <th>Staff ID</th>
        <th>Title</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Department</th>
        <th></th>
    </tr>
    </thead>


    <tfoot>
     <tr>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
     </tr>
    </tfoot>
  </table>
    </div>
  </div>
</div>

<div class="col-md-12">
  <div class="panel panel-default">
      <div class="panel-heading ft-color">
        <h4 class="cinzel panel-title">List of Available Staff</h4>
      </div>
      <div class="panel-body text-font">
        <table class="table table-striped table-hover table-condensed" id="dataTableBuilder">
          <thead>
            <tr>
              <th>Staff ID</th>
              <th>Title</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Department</th>
              <th>Email</th>
              <th>Phone</th>
              <th></th>
          </tr>
          </thead>
          <tfoot>
            <tr>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
          </tr>
          </tfoot>
        </table>
      </div>
      <div class="panel-footer">
        Regent University College of Science and Technology
      </div>
  </div>
</div>

<script>
  (function (window, $) {
   $('.modal').on('hide.bs.modal',function(){
     setTimeout(function(){
       $.ajax({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       },
       type:'get',
       url:'{{route('addStaff')}}',
       success:function(data){
         $("#pager").html(data);
         console.log('Page Loaded!!')
       },
       error:function(x,e){
         if(x.status==401)
         {
           window.location.replace('{{route('admin.login')}}');
         }
         console.log('Page Loading Failed!!')
       }
     });
   },300)
   });
   $('#dataTableBuilder').on('click','#getajax',function(e){
     e.preventDefault();
     var $this = $(this);
     target = $this.data('target');
     $.ajax({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       },
       method: 'GET',
       url: target,
       success:function(data){
         $('#id').html('<input name="id" type="hidden" class="form-control" value="'+data.id+'"    readonly>');
         $('#title').html('<input name="title"            type="text" class="form-control" value="'+data.title+'" required>')
         $('#staff_id').html('<input name="staff_id"      type="number" class="form-control" value="'+data.staff_id+'" required>');
         $('#f_name').html('<input name="f_name"          type="text" class="form-control" value="'+data.f_name+'" required>');
         $('#las_name').html('<input name="las_name"      type="text" class="form-control" value="'+data.las_name+'" required>');
         $('#email').html('<input name="email"            type="email" class="form-control" value="'+data.email+'"    required>');
         $('#phone').html('<input name="phone"            type="number" class="form-control" value="'+data.phone+'"    required>');
         // $('#dpt').html('<option value="'+data.d_code+'">'+data.d_name+'</option>');
         // $('#dpt').html('<option value="'+data.d_code+'">uj</option>');


         $('.staff-modal').modal('show');
       },
       error:function(x,e){
         if(x.status==401)
         {
           window.location.replace('{{route('admin.login')}}');
         }
         console.log('error fecting data from the database');
       }
     });

   });
   $('#dataTableBuilder').on('click','#disable',function(e){
     e.preventDefault();
     var $this = $(this);
     target = $this.data('target');
      //fecthing info to display staff name on modal before submitting the disable thing
     $.ajax({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       },
       method: 'GET',
       url: 'admin/staff/'+target,
       success:function(data){
         $('#warning').html('Are You Sure You Want to Disable <b>'+data.title+' '+data.f_name+' '+data.las_name+'</b>');
         $('#dec-target').html('<input type="hidden" name="target" value="'+target+'" />');
         $('.disable').modal('show');
       },
       error:function(x,e){
         if(x.status==401)
         {
           window.location.replace('{{route('admin.login')}}');
         }
         console.log('error fecting data from the database');
       }
     });

         $('.deactivate').submit(function(e){
           e.preventDefault();
             var myform = $(this).closest('form');
           $.ajax({
             headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
             method: 'get',
             url: '{{route('staff-disable')}}',

             data:myform.serialize(),
             success:function(data){
              $('.disable').modal('hide');
               console.log(data);
             },
             error:function(x,e){
               if(x.status==401)
               {
                 window.location.replace('{{route('admin.login')}}');
               }
               console.log('error posting');
             }
           });
         })

   });
   $('#disabled').on('click','#enable',function(e){
     e.preventDefault();
     var $this = $(this);
     target = $this.data('target');
      //fecthing staff name into modal before actually enabling the staff
     $.ajax({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       },
       method: 'GET',
       url: 'admin/staff/'+target,
       success:function(data){
         $('#en-warning').html('Are You Sure You Want to Enable <b>'+data.title+' '+data.f_name+' '+data.las_name+'</b>');
          $('#en-target').html('<input type="hidden" name="target" value="'+target+'" />');
         $('.enable').modal('show');
       },
       error:function(x,e){
         if(x.status==401)
         {
           window.location.replace('{{route('admin.login')}}');
         }
         console.log('error fecting data from the database');
       }
     });
         $('.activate').submit(function(e){
           e.preventDefault();
            var myform = $(this).closest('form');
           $.ajax({
             headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
             method: 'get',
             url: '{{route('staff-enable')}}',
             data:myform.serialize(),
             success:function(data){
              $('.enable').modal('hide');
               console.log(data);
             },
             error:function(x,e){
               if(x.status==401)
               {
                 window.location.replace('{{route('admin.login')}}');
               }
               console.log('error posting');
             }
           });
         })
   });

   $('.form').submit(function(e){
     e.preventDefault();
     var myform = $(this).closest('form');
     $.ajax({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       },
       type:'post',
       url:'{{route('staffadd')}}',
       data:myform.serialize(),
       success:function(data){
          $.ajax({
            type:'get',
            url:'{{route('addStaff')}}',
            success:function(data){
                $('#pager').html(data);
            }
          });
         alertify.set('notifier','position', 'top-center');
         alertify.set('notifier','delay',2);
         alertify.success('Saved');
       },
       error:function(x,e){
         if(x.status==401)
         {
           window.location.replace('{{route('admin.login')}}');
         }
         alertify.set('notifier','position', 'top-center');
         alertify.set('notifier','delay',5);
         alertify.error('staff already exist');
       }
     })
   });

   $('.edit').submit(function(e){
     e.preventDefault();
     var myform = $(this).closest('form');
     $.ajax({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       },
       type:'post',
       url:'{{route('staffupdate')}}',
       data:myform.serialize(),
       success:function(data){
         $('.staff-modal').modal('hide');
         alertify.set('notifier','position', 'top-center');
         alertify.set('notifier','delay',2);
         alertify.success(data);
       },
       error:function(x,e){
         if(x.status==401)
         {
           window.location.replace('{{route('admin.login')}}');
         }
         alertify.set('notifier','position', 'top-center');
         alertify.set('notifier','delay',5);
         alertify.error('update failed');
       }
     })
   });

   window.LaravelDataTables = window.LaravelDataTables || {};
   window.LaravelDataTables["dataTableBuilder"] = $("#dataTableBuilder").DataTable({
    "serverSide": true,
    "processing": true,
    "columnDefs":[

      {"width":"10%", "targets":0},
      {"width":"10%", "targets":1},
      {"width":"15%", "targets":2},
      {"width":"15%", "targets":3},
      {"width":"15%", "targets":4},
      {"width":"10%", "targets":5},
      {"width":"10%", "targets":6},
      {"width":"15%", "targets":7},
    ],
    "ajax": {
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      "url": "{{route('staff')}}",
      "type": "GET",
      "data": function (data) {
        for (var i = 0, len = data.columns.length; i < len; i++) {
          if (!data.columns[i].search.value) delete data.columns[i].search;
          if (data.columns[i].searchable === true) delete data.columns[i].searchable;
          if (data.columns[i].orderable === true) delete data.columns[i].orderable;
          if (data.columns[i].data === data.columns[i].name) delete data.columns[i].name;
        }
        delete data.search.regex;
        $('.selectpicker').selectpicker('refresh');
      }
    },
    "columns": [{
      data: 'staff_id',
      name:'staff_id',
      orderable: true,
      searchable: true,

    },
    {
      data: 'title',
      name:'title',
      orderable: true,
      searchable: true
    },
    {
      data: 'f_name',
      name:'f_name',
      orderable: true,
      searchable: true
    },

    {
      data: 'las_name',
      name:'las_name',
      orderable: true,
      searchable: true
    },
    {
      data: 'd_name',
      name:'departments.d_name',
      orderable: true,
      searchable: true
    },
    {
      data: 'email',
      name:'email',
      orderable: true,
      searchable: true
    },
    {
      data: 'phone',
      name:'phone',
      orderable: true,
      searchable: true
    },
    {
      "defaultContent": "",
      "name": "Edit / Disable",
      "data": "action",
      "title": "Edit || Disable",
      "orderable": false,
      "searchable": false
    }
    ],
    "dom": "Bfrtip",
    "order": [[0, "desc"]],
    "buttons": ["export", "pdf", "print", "reset", "reload"],
    "initComplete": function () {

      this.api().columns().every(function () {
        var column = this;
        var input = document.createElement("input");
        $(input).css({
          'width':'100%','display':'inline-block'
        }); 
        $(input).appendTo($(column.footer()).empty())
        .on('change', function () {
          column.search($(this).val(), false, false, true).draw();
        });
      });
    }
  });  //default staff
   window.LaravelDataTables["disabled"] = $("#disabled").DataTable({
       "serverSide": true,
       "processing": true,
       "columnDefs":[

         {"width":"10%", "targets":0},
         {"width":"10%", "targets":1},
         {"width":"15%", "targets":2},
         {"width":"15%", "targets":3},
         {"width":"15%", "targets":4},
     ],
       "ajax": {
         headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
           "url": "{{route('disabled_staff')}}",
           "type": "GET",
           "data": function (data) {
               for (var i = 0, len = data.columns.length; i < len; i++) {
                   if (!data.columns[i].search.value) delete data.columns[i].search;
                   if (data.columns[i].searchable === true) delete data.columns[i].searchable;
                   if (data.columns[i].orderable === true) delete data.columns[i].orderable;
                   if (data.columns[i].data === data.columns[i].name) delete data.columns[i].name;
               }
               delete data.search.regex;
           }
       },
       "columns": [{
           data: 'staff_id',
           name:'staff_id',
           orderable: true,
           searchable: true,

       },
       {
         data: 'title',
         name:'title',
         orderable: true,
         searchable: true
       },
       {
         data: 'f_name',
         name:'f_name',
         orderable: true,
         searchable: true
       },

       {
         data: 'las_name',
         name:'las_name',
         orderable: true,
         searchable: true
       },
       {
         data: 'd_name',
         name:'departments.d_name',
         orderable: true,
         searchable: true
       },

       {
         "defaultContent": "",
         "name": "Edit",
         "data": "action",
         "title": "Edit",
         "orderable": false,
         "searchable": false
       }
       ],
       "dom": "Bfrtip",
       "order": [[0, "desc"]],
       "buttons": ["export"],
       "initComplete": function () {

           this.api().columns().every(function () {
               var column = this;
               var input = document.createElement("input");
               $(input).css({
                 'width':'100%','display':'inline-block'
               });
               $(input).appendTo($(column.footer()).empty())
                   .on('change', function () {
                       column.search($(this).val(), false, false, true).draw();
                   });
           });
       }
   });    //disabled staff

})(window, jQuery);
  </script>

  <!-- modal for disabling staff -->
          <div class="modal fade disable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
             <div class="modal-dialog" role="document">
               <div class="modal-content">
                 <div class="modal-header">
                   <h4 class="modal-title text-font" id="myModalLabel">Confirmation Message<small class="pull-right">
                     <i class="fa fa-plus"></i></small></h4>
                 </div>
                 <div class="modal-body">
                   <form class="deactivate" role="form">
                     {{csrf_field()}}
                     <div id="dec-target">

                     </div>
                     <h4 class="text-font" id="warning">

                     </h4>
                     <div class="modal-footer">
                       <a class="btn btn-primary" data-dismiss="modal">No</a>
                       <button type="submit" class="btn btn-danger">Yes</button>
                     </div>
                   </form>
                 </div>
               </div>
             </div>
          </div>


          <!-- modal for enabling staff -->
          <div class="modal fade enable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
             <div class="modal-dialog" role="document">
               <div class="modal-content">
                 <div class="modal-header">
                   <h4 class="modal-title text-font" id="myModalLabel">Confirmation Message<small class="pull-right">
                     <i class="fa fa-plus"></i></small></h4>
                 </div>
                 <div class="modal-body">
                   <form class="activate" role="form">
                     {{csrf_field()}}
                     <div id="en-target">

                     </div>
                     <h4 class="text-font" id="en-warning">

                     </h4>
                     <div class="modal-footer">
                       <a class="btn btn-primary" data-dismiss="modal">No</a>
                       <button type="submit" class="btn btn-danger modal-update">Yes</button>
                     </div>
                   </form>
                 </div>
               </div>
             </div>
          </div>


            <!-- modal for staff edititng -->
           <div class="modal fade staff-modal" id="staff-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                 <div class="modal-dialog" role="document">
                               <div class="modal-content">
                                 <div class="modal-header">
                                   <h4 class="modal-title text-font" id="myModalLabel">Change Staff Details <small class="pull-right">
                                     <i class="fa fa-plus"></i></small></h4>
                                   <hr />
                                 </div>
                                 <div class="modal-body">
                                   <form class="form-horizontal text-font edit" role="form">
                                     {{csrf_field()}}

                                     <div class="form-group">
                                       <label for="last_name" class="col-md-3 control-label"><i class="fa fa-graduation-cap prefix" aria-hidden="true"></i> TITLE</label>
                                       <div class="col-md-6" id="title">

                                       </div>

                                       <div class="col-md-6 hidden" id="id">

                                       </div>
                                     </div>
                                     <div class="form-group">
                                       <label for="last_name" class="col-md-3 control-label"><i class="fa fa-id-card prefix" aria-hidden="true"></i> ID NUMBER</label>
                                       <div class="col-md-6" id="staff_id">

                                       </div>
                                     </div>
                                     <div class="form-group">
                                         <label for="first_name" class="col-md-3 control-label"><i class="fa fa-user-circle-o" aria-hidden="true"></i> FIRST NAME</label>
                                         <div class="col-md-6" id="f_name">

                                         </div>
                                     </div>
                                     <div class="form-group">
                                         <label for="first_name" class="col-md-3 control-label"><i class="fa fa-user-circle-o" aria-hidden="true"></i> LAST NAME</label>
                                         <div class="col-md-6" id="las_name">

                                         </div>
                                     </div>
                                     <div class="form-group">
                                         <label for="last_name" class="col-md-3 control-label"><i class="fa fa-book prefix" aria-hidden="true"></i> DEPARTMENT</label>
                                         <div class="col-md-6">
                                           <select class="form-control" id="" name="department" data-live-search="true" required>

                                             <option></option>
                                             @foreach ($dpt as $dpts)
                                                   <option data-tokens="{{$dpts->d_name}}" value="{{$dpts->d_code}}">{{$dpts->d_name}}</option>
                                                 @endforeach
                                           </select>
                                         </div>
                                     </div>

                                     <div class="form-group">
                                         <label for="last_name" class="col-md-3 control-label"><i class="fa fa-phone prefix" aria-hidden="true"></i> MOBILE</label>
                                         <div class="col-md-6" id="phone">

                                         </div>
                                     </div>

                                     <div class="form-group">
                                         <label for="last_name" class="col-md-3 control-label"><i class="fa fa-envelope prefix" aria-hidden="true"></i> STAFF EMAIL</label>
                                         <div class="col-md-6" id="email">

                                         </div>
                                     </div>
                                     <div class="modal-footer">
                                       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                       <button type="submit" class="btn btn-primary modal-update">Save changes</button>
                                     </div>
                                   </form>
                                 </div>

                               </div>
                             </div>
             </div>
