<!Doctype html>
<html lang="{{ config('app.locale') }}">
@include('admin.header') @yield('header')
<body class="cinzel">
    @yield('admin-nav')
    <div class="container-fluid">
      @yield('sidebar')
            <input type="hidden" value="{{$fx_year->year}}" id="fx_year" />
            <div class="col-md-10" id="pager">

            </div>
    </div>
@yield('footer')



<script type="text/javascript">

$(document).ready(function(){

  $('#single a').on('click',function(event){
      event.preventDefault();
      var $this = $(this);
      target = $this.data('target');
      $.ajax({
        method: 'GET',
        url: target,
        beforeSend: function(){
          $('#pager').html('Please Wait')
        },
        success:function(data){
          $('select').selectpicker('refresh');
          $('#pager').html(data);
        },
        error:function(x,e)
        {
          if(x.status==401)
          {
            window.location.replace('{{route('admin.login')}}');
          }
        }
      });
  });

  $.ajax({
    type:'get',
    url:'{{route('home-tab')}}',
    beforeSend:function(){
        $('#pager').html('Please Wait');
    },
    success:function(data){
      $('#pager').html(data);
    },
    error:function(x,e)
    {
      if(x.status==401)
      {
        window.location.replace('{{route('admin.login')}}');
      }
    }
  })

  var fixed_year = $('#fx_year').val()
  var today = new Date();
  var this_year = today.getFullYear();
  var mysplit = fixed_year.split("/");
  var month = today.getMonth() + 1;
  var day = today.getDate();
  console.log(month);


  if(mysplit[1] <= this_year)
  {
    //here
    if( month >= '8'){

     var new_year_1 = parseInt(mysplit[0]) + 1;
     var new_year_2 = parseInt(mysplit[1]) + 1;
     var academic_year = new_year_1+'/'+new_year_2;
     // console.log(new_year_1,new_year_2,academic_year);

     alertify.confirm('Update Confirmation Message',
     '<p class="text-justify">Hello <b>Director</b>, the school has entred a new academic year which is '
      +'<b>'+academic_year+'</b>. I need your permission to update the records. '
      +'After updating the academic year, all students will be moved to another level '
      +'e.g those in level <b>100</b> will be moved to level <b>200</b> and it '
      +'applies to other levels. <br /> For those in level <b>400</b> their '
      +'levels will be set to <b>graduated</b> or <b>completed</b>.'
      +'SHOULD I PROCEED?? </p>',function(){
       $.ajax({
         headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         type:'post',
         url:'{{route('set_academic_year')}}',
         data:{academic_year:academic_year},
         success:function(data)
         {
           // update script for levels of student
             $.ajax({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               type:'post',
               url:'{{route('level-change')}}',
               success:function(data){
                 console.log(data)
               },error:function(){
                 alert('There Was An Error levels couldnt be updated');
               }
             })
         },error:function(x,e){
           if(x.status==401){
             window.location.replace('/admin')
           }
           console.log('There is an error couldnt post');
         }
       })
     },function(){
       alertify.error('Cancel');
     });

   }
     //here
  }
});

</script>
</body>
</html>
