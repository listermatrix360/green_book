<html>
@include('admin.header')
@yield('header')

<body>
    <div class="col-md-9">
        <a class="btn btn-primary" id="dview">Dynamic View</a>
        <table class="table" id="user-list">
            <thead>
                <tr>
                    <th>Registration Number</th>
                    <th>Name Of Student</th>
                    <th>Email Of Student</th>
                    <th>Program</th>
                    <th>Date Created</th>
                </tr>
            </thead>

            <tfoot>
                <tr>
                    <th>Registration Number</th>
                    <th>Name Of Student</th>
                    <th>Email Of Student</th>
                    <th>Program</th>
                    <th>Date Created</th>
                </tr>
            </tfoot>
            <tbody class="">


            </tbody>
        </table>
    </div>


    <script type="text/javascript">
        $(function() {
            $('#user-list').DataTable({
                processing: true,
                serverSide: true,
              "ajax":{
                   "url": "{{route('ajax_users')}}",
                   "type": "GET",
                   "datatype":"json",
                   "contentType": "application/json;charset=UTF-8",
                },
                columns: [
                          {data: 'reg_num', name:'users.reg_num'},
                          {data: 'name', name:'users.name'},
                          {data: 'email', name:'users.email'},
                          {data: 'program', name:'personal_details.program'},
                          {data: 'created_at', name:'users.created_at' },
                ],
                "dom": "Bfrtip",
                "order": [[0, "desc"]],
                "buttons": ["export", "pdf", "print", "reset", "reload"],
                 initComplete: function () {
                      this.api().columns().every(function () {
                          var column = this;
                          var input = document.createElement("input");
                          $(input).appendTo($(column.footer()).empty())
                          .on('change', function () {
                              column.search($(this).val(), false, false, true).draw();
                          });
                      });
                  }
            });
        });
    </script>
    <!-- displaying Edit page -->
    <script>
        $(document).ready(function() {
            $("#dview").on('click', function() {
                $.ajax({
                    url: '{{route('d_list')}}',
                    type: 'GET',
                    beforeSend: function() {
                        $("#body1").html('Working On...');
                    },
                    success: function(data) {
                        $("#body1").html(data);
                    },
                });
            });
        });
    </script>
</body>



</html>
