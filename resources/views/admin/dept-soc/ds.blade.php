
        <div class="col-md-9">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="cinzel">Socieities</h4>
            </div>
            <div class="panel-body">
              @if(count($soc) < 4)
                <form class="text-font society">
                  {{ csrf_field() }}
                  <div class="row">
                  <div class="">
                    <label class="col-sm-1">SOCIETY</label>
                    <div class="col-md-4">
                       <select class="selectpicke"  name="name" data-live-search="true" required>
                         <option value=""></option>
                            <option data-tokens="Anim" name="">Anim</option>
                            <option data-tokens="Stott" name="">Stott</option>
                            <option data-tokens="Nkrumah" name="">Nkrumah</option>
                            <option data-tokens="Ubuntu" name="">Ubuntu</option>
                        </select>
                    </div>
                  </div>

                  <div class="">
                    <label class="col-sm-1">WARDEN</label>
                    <div class="col-sm-4">
                      <select class="selectpicker"   name="staff_id" data-live-search="true" required>
                         <option value=""></option>
                            @foreach ($ment_list as $staff)
                         <option data-tokens="{{$staff->title}} {{$staff->f_name}} {{$staff->las_name}}" value="{{$staff->staff_id}}" name="ment_name">{{$staff->title}} {{$staff->f_name}} {{$staff->las_name}}</option>
                            @endforeach
                        </select>

                    </div>
                  </div>
                  <div class="email" id="editBtn">
                    <button class="btn ft-color" type="submit" id="btn-save">Save</button>
                  </div>
                </div>
                </form>
              @endif

          <br />
          @foreach($soc as $socs)
          <form class="text-font society">
            {{ csrf_field() }}
            <div class="row">
              <div class="">
                <label class="col-sm-1">SOCIETY</label>
                <div class="col-md-4 text-edit">
                  <input type="text" value="{{$socs->name}}" readonly>
                </div>
              </div>

              <div class="">
                <label class="col-sm-1">WARDEN</label>
                <div class="col-sm-4 text-edit">
                  <input type="text" value="{{$socs->title}} {{$socs->f_name}} {{$socs->las_name}}" readonly/>
                </div>
              </div>
              <div class="edit">
                <a class="btn sub-color-bg" href="{{$socs->sid}}" id="btn-save"><i class="fa fa-pencil"></i></a>
              </div>
            </div>
          </form>
          <br />
          @endforeach
            </div>
            <div class="panel-footer">
                 Regent University College Of Science and Technology
            </div>
          </div>

        </div>
<script>
  $('document').ready(function(){
    $('select').selectpicker('refresh');

    function reload(){
     $.ajax({
       method: 'GET',
       url: "{{route('misc.index')}}",
       success:function(data){
         $('#pager').html(data);
       }

     });
      }

    $('.society').on('submit',function(e){
          e.preventDefault();
          var myform = $(this).closest('form');
          $.ajax({
            type:'post',
            url:'{{route('misc.store')}}',
            data:myform.serialize(),
            success:function(data){
              reload();
            },
            error:function(){
              alertify.set('notifier','position', 'top-center');
              alertify.set('notifier','delay',10);
              alertify.error('SOCIETY NAME ALREADY EXIST CHOOSE ANOTHER ONE !!!!');
            }
          })
    })

    $('.edit a').on('click',function(e){
      e.preventDefault();
       var sid = $(this).attr('href');
       $.ajax({
         method: 'GET',
         url: 'misc-fetch/' + sid,
         success:function(data){
           var soc_name = data.soc_name;
           $('#name').html('<input type="text" name="name" class="form-control" value="'+soc_name+'" readonly> <input type="hidden" id="key" class="form-control" value="'+data.sid+'" readonly>');
           $('#edit-mod').modal();
         },
         error:function()
         {
           alert('There is an error')
         }

       });
    })
        $('#edit-mod').on('hide.bs.modal',function(){
          setTimeout(function(){
            reload();
          },300)
        });
    $('.editForm').on('submit',function(e){
      e.preventDefault();
      var myform = $(this).closest('form');
      var sid = $('#key').val();
        $.ajax({
          type:'POST',
          url:'misc/' + sid,
          data:myform.serialize(),
          success:function(data)
          {
            alertify.set('notifier','position', 'top-center');
            alertify.set('notifier','delay',10);
            alertify.success('Edit Successfull');
            $('#edit-mod').modal('hide');
          },
          error:function()
          {
            alertify.set('notifier','position', 'top-center');
            alertify.set('notifier','delay',10);
            alertify.error('There is an error !!');
          }
        })

    })
  })
</script>

<div class="modal fade" id="edit-mod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-font" id="myModalLabel">EDIT SOCIETY DETAILS <small class="pull-right">
          <i class="fa fa-plus"></i></small></h4>
        <hr />
      </div>
      <div class="modal-body">
        <form class="text-font form-horizontal editForm">
          {{ csrf_field() }}
          {{method_field('PUT')}}

          <div class="form-group">
            <div class="col-sm-3">
            <label class="control-label">SOCIETY</label>
          </div>
            <div class="col-md-5" id="name">

            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-3">
            <label class="control-label">WARDEN</label>
          </div>
            <div class="col-sm-5">
              <select class="selectpicker"   name="staff_id" data-live-search="true" required>
                 <option value=""></option>
                    @foreach ($ment_list as $staff)
                 <option data-tokens="{{$staff->title}} {{$staff->f_name}} {{$staff->las_name}}" value="{{$staff->staff_id}}" name="ment_name">{{$staff->title}} {{$staff->f_name}} {{$staff->las_name}}</option>
                    @endforeach
                </select>

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary modal-update">Save changes</button>
          </div>
        </div>
    </form>
      </div>

    </div>
  </div>
</div>
