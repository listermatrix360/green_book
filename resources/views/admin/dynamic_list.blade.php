<!DOCTYPE html>
<html>
@include('admin.header') @yield('header')

<body>
    @yield('admin-na')
    <div class="container-fluid">
        <div class="row">
            @yield('sideba')
            <div class="col-md-8" id="body1">
                <table class="table ">
                    <thead>
                        <tr>
                            <th>Registration Number</th>
                            <th>Name Of Student</th>
                            <th>Email Of Student</th>
                        </tr>
                    </thead>

                    <tfoot>
                        <tr>
                            <th>Registration Number</th>
                            <th>Name Of Student</th>
                            <th>Email Of Student</th>
                        </tr>
                    </tfoot>
                    <tbody class="">
                        @foreach ($user as $users)
                        <tr>
                            <td><a class="list-group-item" data-toggle="tooltip" data-placement="bottom" title="Click me to view more info" href="{{'/admin/show/'.$users->reg_num}}" id="userlink">{{$users->reg_num}}</a></td>
                            <td><a class="list-group-item" href="{{'/admin/show/'.$users->reg_num}}" id="userlink">{{$users->name}}</a></td>
                            <td><a class="list-group-item" href="{{'/admin/show/'.$users->reg_num}}" id="userlink">{{$users->email}}</a></td>
                            <td><a class="btn btn-danger" href="{{'/admin/'.$users->reg_num. '/edit'}}">Edit</a></td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            <div class="col-md-3">
                <button class="btn btn-danger" data-toggle="popover" title="Help!!" data-content="You can click on the data displayed to view a detailed information
          about each student">CLICK ME!!!</button>
            </div>
        </div>
    </div>

    @yield('admin-scrip')
    <script>
        $(function() {
            $('[data-toggle="popover"]').popover()
        })
    </script>
</body>

</html>
