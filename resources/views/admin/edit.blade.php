<!DOCTYPE html>
<html>
@include('admin.header') @yield('header')

<body>
    @yield('admin-nav')

    <div class="container-fluid">
        <div class="row">
            @yield('sidebar')
            <div class="col-md-10" id="body1">
                {{$user->name}}
            </div>
        </div>
    </div>




    @yield('admin-script')
</body>

</html>
