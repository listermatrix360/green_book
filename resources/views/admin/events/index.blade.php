<div class="col-md-7">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title text-edit">Event Form</h4>
    </div>
    <div class="panel-body">
      <form class="form-horizontal text-font event col-md-offset-2" role="form">
        {{csrf_field()}}
        <div class="form-group">
          <label class="col-md-3 control-label">Event Date</label>
          <div class="col-md-5">
            <input type="date" name="date" class="form-control" required/>
          </div>
        </div>

        <div class="form-group">
          <label class="col-md-3 control-label">Event Title</label>
          <div class="col-md-5">
            <input type="text" name="title" class="form-control" placeholder="events" required/>
          </div>
        </div>

        <div class="form-group">
          <label class="col-md-3 control-label">Description</label>
          <div class="col-md-5">
            <textarea placeholder="" name="description"class="form-control" rows="3" cols="40" required></textarea>
          </div>
        </div>

          <div class="col-md-offset-4">
          <button class="btn ft-color">Add Event</button>
        </div>
  </form>
    </div>
  </div>
</div>
<div class="col-md-5">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title text-edit">Broadcast Notification</h4>
    </div>
    <div class="panel-body text-font">
      <!-- <i class="fa fa-flask fa-5x" aria-hidden="true"></i>
      <i class="fa fa-sign-language fa-5x" aria-hidden="true"></i> -->
      <form class="form-horizontal notification">
        {{csrf_field()}}
        <div class="form-group">
          <label class="col-md-2 control-label">Message</label>
          <div class="col-md-8">
            <input type="text" placeholder="type broadcast message here" name="info" class="form-control" required/>
          </div>
        </div>
        <div class="text-center">
          <button class="btn ft-color"><i class="fa fa-send"></i> Send Message</button>
        </div>
      </form>
    </div>
  </div>
</div>



  @if($event->isEmpty())
    <h4 class="text-font">No Event Set Yet</h4>
    @else
    <table class="table text-font table-responsive table-striped">
      <thead>
        <tr>
          <th>Date</th>
          <th>Title</th>
          <th>Description</th>
          <th>Edit</th>
        </tr>
      </thead>
      <tbody>
        @foreach($event as $events)
        <tr>
            <td>{{$events->date}}</td>
            <td>{{$events->title}}</td>
            <td>{{$events->description}}</td>
            <td>
              <a class="btn btn-xs ft-color" id="edit"    href="{{$events->id}}"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a>
              <a class="btn btn-xs btn-danger" id="purge" href="{{$events->id}}"><i class="fa fa-trash" aria-hidden="true"></i></a>
            </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  @endif


<script>
$(document).ready(function(){
$('.modal').on('hide.bs.modal',function(){
    setTimeout(function(){
      $.ajax({ headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },type:'get',url:'{{route('swift.index')}}',success:function(data){
        $("#pager").html(data);$('select').selectpicker('refresh');},
        error:function(){console.log('Page Loading Failed!!')}});

    },300)
    });
$('.event').on('submit',function(e){
  e.preventDefault();var myform = $(this).closest('form');$.ajax({ headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }, type:'post',url:'{{route('swift.store')}}',
  data:myform.serialize(),success:function(){$.ajax({type:'get',url:'{{route('swift.index')}}',success:function(data){
  $('#pager').html(data)},error:function(){alert('failed');}})},error:function(){alert('failed');}})})
$('.notification').on('submit',function(e){
  e.preventDefault(); var myform = $(this).closest('form');$.ajax({ headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },type:'get',url:'{{route('flash-notify')}}',data:myform.serialize(),
    success:function(data){console.log(data); $.ajax({type:'get',url:'{{route('swift.index')}}',success:function(data){$('#pager').html(data)},
        error:function(){alert('failed');}})},error:function(){alert('failed');}})})
$('.edit-event').on('submit',function(e){
  e.preventDefault(); var myform = $(this).closest('form');   var id = $('#index').val();
  $.ajax({headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }, type:'PATCH',url:'swift/'+id,data:myform.serialize(),success:function(data){console.log(data);$('#event_modal').modal('hide');}});})
$('tbody #edit').on('click',function(e){
  e.preventDefault();  var token = $(this).attr('href')
  $.ajax({ headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }, method:'get',url:'/swift/'+token+'/edit',success:function(data){
      $('#event_modal').modal('toggle');
      $('#date').html('<input type="date" name="date" class="form-control" value="'+data.date+'" required/> <input type="hidden" name="id" id="index" class="form-control" value="'+data.id+'" required/>');
      $('#title').html('<input type="text" name="title" class="form-control" value="'+data.title+'" required/>');
      $('#description').html('<textarea placeholder="" name="description"class="form-control" rows="3" cols="40" required>'+data.description+'</textarea>');
    },error:function(){console.log('error');}})})
$('tbody #purge').on('click',function(e){
  e.preventDefault(); var token = $(this).attr('href');
  $.ajax({ headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}, method:'delete',url:'swift/delete/'+token, data:{'_token':$('input[name=_token]').val()},success:function(data){
  $.ajax({type:'get',url:'{{route('swift.index')}}',success:function(data){$('#pager').html(data)},error:function(){
    alert('failed');}})},error:function(){console.log('deletion unsuccessful!!')}})
})

});
</script>
<!-- modal for enabling staff -->
<div class="modal fade" id="event_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
     <div class="modal-content">
       <div class="modal-header">
         <h4 class="modal-title text-font" id="myModalLabel">Confirmation Message<small class="pull-right">
           <i class="fa fa-plus"></i></small></h4>
       </div>
       <div class="modal-body">
         <form class="form-horizontal text-font edit-event" role="form">
           {{csrf_field()}}
           {{method_field('PATCH')}}
           <div class="form-group">
              <label class="control-label col-md-3">DATE</label>
              <div class="col-md-6" id="date">
                  <input type="date" class="form-control" required/>
              </div>
           </div>
           <div class="form-group">
             <label class="control-label col-md-3">TITLE</label>
                <div class="col-md-6" id="title">
                    <input type="text" class="form-control" value="" required/>
                </div>
           </div>

           <div class="form-group">
             <label class="control-label col-md-3">DESCRIPTION</label>
            <div class="col-md-6" id="description">
              <textarea cols="40" rows="5" class="form-control"></textarea>
            </div>
           </div>
           <div class="modal-footer">
             <a class="btn btn-primary" data-dismiss="modal">No</a>
             <button type="submit" class="btn btn-danger modal-update">Yes</button>
           </div>
         </form>
       </div>
     </div>
   </div>
</div>
