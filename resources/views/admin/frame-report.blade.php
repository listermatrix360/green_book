<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if(count($pd)!=0)
    <title>P.E.R {{$pd->reg_num}}</title>
    @endif
    <link href="https://fonts.googleapis.com/css?family=Alegreya|Cinzel+Decorative|Cormorant|Courgette|Fontdiner+Swanky|Great+Vibes|Lobster|Lora|Mogra|Mr+Dafoe|Mr+De+Haviland|Noto+Serif|Oregano|Poiret+One|Pompiere|Raleway|Slabo+27px|Spectral|Vibur|Yellowtail" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link rel="shortcut icon" href="{{{ asset('logoo.png') }}}">
    <link rel="stylesheet" href="{{asset('css/frame.css')}}" />
    <link rel="stylesheet" href="{{asset('css/style.css')}}" />
    <!-- <link rel="stylesheet" href="{{asset('css/paper.min.css')}}" /> -->
    <link rel="stylesheet" href="{{asset('materialize/css/materialize.min.css')}}" />
    <script src="{{ asset('js/jquery-3.2.1.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{asset('materialize/js/materialize.min.js')}}"></script>
    <script src="{{ asset('pdf/printThis.js') }}"></script>
    <script type="text/javascript">
      $(document).ready(function(){

        setTimeout(function(){
              $('body').printThis({
              debug: false,           // show the iframe for debugging
              importCSS: true,        // import parent page css
              importStyle: false,     // import style tags
              printContainer: false,   // print outer container/$.selector
              loadCSS: "{{asset('materialize/css/materialize.min.css')}}",  // load an additional css file - load multiple stylesheets with an array []
              pageTitle: "P.E.R @if(count($pd)!=0){{$pd->reg_num}} @endif",          // add title to print page
              removeInline: false,    // remove all inline styles
              printDelay: 555,        // variable print delay
              header: null,           // prefix to html
              footer: null,           // postfix to html
              formValues: true,       // preserve input/form values
              canvas: true,          // copy canvas content (experimental)
              base: false,            // preserve the BASE tag, or accept a string for the URL
              doctypeString: '<!DOCTYPE html>', // html doctype
              removeScripts: false,   // remove script tags before appending
              copyTagClasses: false   // copy classes from the html & body tag
          });
           },500)
        })

    </script>
</head>

<body class="cinzel">
  <nav class="green darken-4">
     <div class="nav-wrapper">
       <a href="#" class="brand-logo right"><img src="{{asset('logoo.png')}}" style="height:58px; width:65px;"/></a>
       <ul id="nav-mobile" class="left hide-on-med-and-down">
         <li><a href="sass.html">REGENT UNIVERSITY COLLEGE OF SCIENCE AND TECHNOLOGY</a></li>
         <li><a href="badges.html">STUDENT PRACTICAL EXPERIENCE RECORD </a></li>
       </ul>
     </div>
  </nav>
    <div class="container" id="printable">
      @if(count($pd)!=0)
      <div class="row">
        <br />
            <div class="col s4">
              <div class="card-image">
                @if($pd->img =='null')
                  <img src="{{asset('css/img/kristy.png')}}" class="responsive-img" height="200px">
                  @else
                  <img src="{{asset('storage/greenFolder/'.$pd->img)}}" class="responsive-img materialboxed" width="300" height="400">
                  @endif
                </div>
            </div>


        <div class="col s8">
          <h4 class="text-font">{{$pd->title}} {{$pd->othernames}} {{$pd->surname}} </h3>
          <h4 class="text-font">{{$pd->reg_num}}, Level: {{$pd->level}}</h3>
          <h4 class="text-font">{{$pd->program}}</h4>
        </div>


      <div class="col s12">
        <br /><br />
        <div class="row">
          <form class="col s12">
            <div class="row">
              <div class="input-field col s3">
                <input placeholder="Placeholder" value="{{$pd->session}}" type="text" class="validate" readonly>
                <label for="first_name">SESSION</label>
              </div>

              <div class="input-field col s2">
                <input id="last_name" type="text" value="{{$pd->gender}}" class="validate" readonly>
                <label for="last_name">GENDER</label>
              </div>
              <div class="input-field col s3">
                <input id="last_name" type="text" value="{{$pd->dob}}" class="validate" readonly>
                <label for="last_name">DATE OF BIRTH</label>
              </div>
              <div class="input-field col s4">
                <input id="last_name" type="text" value="{{$pd->society}}" class="validate" readonly>
                <label for="last_name">SOCIETY</label>
              </div>
            </div>
            <div class="row">

              <div class="input-field col s6">
                <input id="last_name" type="text" value="{{$pd->email}}" class="validate" readonly>
                <label for="last_name">EMAIL 1</label>
              </div>

              <div class="input-field col s6">
                <input id="last_name" type="text" value="{{$pd->email2}}" class="validate" readonly>
                <label for="last_name">EMAIL 2</label>
              </div>
            </div>

            <div class="row">
              <div class="input-field col s4">
                <input placeholder="Placeholder" value="{{$pd->nationality}}" type="text" class="validate" readonly>
                <label for="first_name">NATIONALITY</label>
              </div>

              <div class="input-field col s4">
                <input id="last_name" type="text" value="{{$pd->phone1}}" class="validate" readonly>
                <label for="last_name">PHONE 1</label>
              </div>

              <div class="input-field col s4">
                <input id="last_name" type="text" value="{{$pd->phone2}}" class="validate" readonly>
                <label for="last_name">PHONE 2</label>
              </div>
            </div>

          </form>
        </div>
      </div>

      <div class="col s12">
        <br />
            <h4 class="cinzel center-align">MENTOR</h4>
            <table class="bordered striped centered">
              <thead>
                <tr>
                  <th>Level</th>
                  <th>Mentor</th>
                  <th>status</th>
                </tr>
              </thead>
              <tbody>
                @foreach($mentor as $ment)
                <tr>
                  <td>{{$ment->level}}</td>
                  <td>{{$ment->title}} {{$ment->f_name}} {{$ment->las_name}}</td>
                  <td>
                    {{$ment->howfar}}
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
      </div>
      <div class="col s12">
        <br />
        <h4 class="text-font center-align">DIARY</h4>
        <hr />
        @if($diary->isEmpty())
        <blockquote style="text-align:justify">
          <p>
            No data available !!!
          </p>
        </blockquote>
          @else
        @foreach($diary as $dd)
          <div class="col s12">
            <blockquote  style="text-align:justify">
              <p>
                {{$dd->title}}    {{$dd->date}}
              </p>
              {!!html_entity_decode($dd->body)!!}
            </blockquote>
          </div>
        @endforeach
        @endif
      </div>
      <div class="col s12 text-font">
        <br />
            @if($intern->isEmpty())
            <h4 class="center-align">INDUSTRIAL ATTACHMENT
            </h4>
            <hr />
                No Information Available
              @else
                <h4 class="center-align">INDUSTRIAL ATTACHMENT </h4>
                <hr />
                @foreach($intern as $attach)
                <h5 class="center-align">From <b>{{$attach->st_date}}</b> to <b>{{$attach->en_date}}</b></h5>
                <h5 class="center-align">Employer Details</h5>

                  <div  class="row">
                    <form class="col s12">
                      <div class="row">
                        <div class="input-field col s6">
                          <input placeholder="Placeholder" id="first_name" value="{{$attach->emp_name}}" type="text" readonly>
                          <label for="first_name">EMPLOYER</label>
                        </div>
                        <div class="input-field col s6">
                          <input id="last_name" type="text" class="validate" value="{{$attach->emp_email}}" readonly>
                          <label for="last_name">EMPLOYEE EMAIL</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s6">
                        <input placeholder="Placeholder" id="first_name" value="{{$attach->address}}" type="text" readonly>
                        <label for="first_name">ADDRESS</label>
                      </div>
                      <div class="input-field col s6">
                        <input id="last_name" type="text" class="validate" value="{{$attach->department}}" readonly>
                        <label for="last_name">DEPARTMENT</label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="input-field col s6">
                      <input placeholder="Placeholder" id="first_name" value="{{$attach->job_title}}" type="text" readonly>
                      <label for="first_name">JOB TITLE</label>
                    </div>
                    <div class="input-field col s6">
                      <input id="last_name" type="text" class="validate" value="{{$attach->exp_area}}" readonly>
                      <label for="last_name">EXPERIENCE AREA</label>
                  </div>
                </div>
                  </form>
                </div>




                <h5 class="center-align">
                  <em>Work as summarised by student</em>
                </h5>
                    <blockquote style="text-align:justify">
                      {!!html_entity_decode($attach->work_sum)!!}
                    </blockquote>

                  <h5 class="center-align"><em>
                    Employer / Supervisor's Endorsement
                    </em>
                  </h5>
                  <blockquote style="text-align:justify">
                    I certify that <b>{{$pd->title}} {{$pd->surname}} {{$pd->othernames}}</b> was
                    employed by <b>{{$attach->org_name}}</b> between <b>{{$attach->st_date}}</b> and <b>{{$attach->en_date}}</b>
                    and has satisfactorily completed the activities summarised above.
                    <br />
                    <label><b>Postion held</b></label>: {{$attach->pos_held}},
                    <br />
                     {{$attach->emp_name}}
                    <br />
                    <label><b>Professional Status</b></label>: {{$attach->emp_prof_status}}
                    <label><b>Organisation</b></label>: {{$attach->org_name}}
                    <!-- <blockquote> -->
                      <h5><em>
                        Comments on attitude to work, ability to communicate, willingness to act on own initiative
                        and to excercise judgement.
                        </em>
                      </h5>
                      <hr />

                    {!!html_entity_decode($attach->comments)!!}

                    <!-- </blockquote> -->
                  </blockquote>
                </p>
                @endforeach
            @endif
          </div>
      <div class="col s12 text-font">
      <br />
        <h4 class="center-align">PROFESSIONAL SKILLS</h4>
        <hr />
        @if($prof->isEmpty())
            No details submitted
          @else

        @foreach($prof as $skills)
        <blockquote style="text-align:justify">

          <h5 class="">{{$skills->title}}</h5>
          {!!html_entity_decode($skills->prof_skill_body)!!}

        </blockquote>
        @endforeach
        @endif
    </div>
      <div class="col s12 text-font">
        <br />
        <h4 class="cinzel center-align">EDUCATION AND EXAMINATION<small>(Outside the University)</small></h4>
        <hr />
        @if($educ->isEmpty())
            No details submitted
          @else
            <table class="bordered stripped centered">
              <thead>
                <tr>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>School</th>
                  <th>Method</th>
                  <th>Exams</th>
                  <th>Level</th>
                  <th>Paper</th>
                  <th>Results</th>
                  <th>Remarks</th>
                </tr>
              </thead>
              <tbody>
                @foreach($educ as $learn)
                  <tr>
                    <td>{{$learn->st_dt}}</td>
                    <td>{{$learn->en_dt}}</td>
                    <td>{{$learn->sch_name}}</td>
                    <td>{{$learn->study_mtd}}</td>
                    <td>{{$learn->ex_taken}}</td>
                    <td>{{$learn->level}}</td>
                    <td>{{$learn->paper}}</td>
                    <td>{{$learn->results}}</td>
                    <td>{{$learn->remarks}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
        @endif
    </div>
      <div class="col s12 text-font">
        <br />
        <h4 class="center-align">OTHERS COURSES ATTENDED <small>(Organized Within the University)</small></h4>
        <hr />
        @if($other->isEmpty())
        No details submitted
        @else
        <table class="centered bordered striped">
          <thead>
            <tr>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Course</th>
              <th>Description</th>
              <th>Remarks</th>
            </tr>
          </thead>
          <tbody>
            @foreach($other as $course)
            <tr>
              <td>{{$course->date1}}</td>
              <td>{{$course->date2}}</td>
              <td>{{$course->cours_name}}</td>
              <td>{{$course->descrip}}</td>
              <td>{{$course->remarks}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
        @endif
      </div>
      <div class="col s12 text-font">
      <br />
      <h5 class="center-align">CHAPEL SERVICES ATTENDED</h5>
      <hr />
      @if($chap->isEmpty())
      No details submitted
      @else
      <table class="bordered striped centered">
        <thead>
          <tr><th>Date</th>
            <th>Activity</th>
            <th>Status</th>
          </tr></thead>
          <tbody>
            @foreach($chap as $ch)
            <tr>
              <td>{{$ch->date}}</td>
              <td>{{$ch->activity}}</td>
              <td>{{$ch->status}}</td>
            </tr>
            @endforeach

          </tbody>
        </table>
        @endif
      </div>

      <div class="col s12 text-font">
        <h5 class="center-align">COMMUNITY SERVICES UNDERTAKEN</h5>
        <hr />
        @if($com->isEmpty())
        No Details Submitted!!
        @else
        <table class="bordered striped centered">
          <thead>
            <tr><th>Date</th>
              <th>Activity</th>
              <th>Status</th>
            </tr></thead>
            <tbody>
              @foreach($com as $comm)
              <tr>
                <td>{{$comm->date}}</td>
                <td>{{$comm->activity}}</td>
                <td>{{$comm->status}}</td>
              </tr>
              @endforeach

            </tbody>
          </table>
          @endif
        </div>

        <div class="col s12 text-font">
          <h5 class="center-align">PHYSICAL EXERCISES UNDERTAKEN</h5>
          <hr />

          @if($phy->isEmpty())
          No Data Yet !!
          @else

          <table class="bordered striped centered">
            <thead>
              <tr>
                <th>Date</th>
                <th>Activity</th>
                <th>Status</th>
              </tr>

            </thead>
            <tbody>
              @foreach($phy as $ph)
              <tr>
                <td>{{$ph->date}}</td>
                <td>{{$ph->activity}}</td>
                <td>{{$ph->status}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
          @endif
        </div>
      <div class="col s12 text-font">
        <h5 class="center-align">AUTOBIOGRAPHY</h5>
        <hr />
        @if(count($auto)==0)
        <h6>There is nothing here !!</h6>
        @else
        <blockquote class="t2 text-font" style="text-align:justify">
          {!!html_entity_decode($auto->auto_body)!!}
        </blockquote>
        @endif
      </div>
    </div>
    @else
    <div>
      <h4 class="text-font center-align">NOTHING TO BE PRINTED</h4>
    </div>
    @endif
    </div>


</body>
</html>
