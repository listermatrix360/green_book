@section('header')
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>GB ESS | Dashboard</title>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/paper/bootstrap.min.css" /> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Alegreya|Cinzel+Decorative|Cormorant|Courgette|Fontdiner+Swanky|Great+Vibes|Lobster|Lora|Mogra|Mr+Dafoe|Mr+De+Haviland|Noto+Serif|Oregano|Poiret+One|Pompiere|Raleway|Slabo+27px|Spectral|Vibur|Yellowtail" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Alegreya|Allura|Calligraffitti|Cinzel|Cinzel+Decorative|Cormorant+Garamond|Cormorant+Upright|Courgette|Fontdiner+Swanky|Great+Vibes|Italianno|Josefin+Slab|Kurale|Lora|Merienda|Mogra|Mr+De+Haviland|Noto+Serif|Playball|Raleway|Sacramento|Slabo+13px|Slabo+27px|Spectral|Tangerine|Yellowtail" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link rel="shortcut icon" href="{{{ asset('logoo.png') }}}">
    <link rel="stylesheet" href="{{asset('css/paper.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/alertify.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/themes/bootstrap.min.css')}}" />
    <link href="{{asset('css/datatable/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/datatable/jquery.dataTables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/datatable/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/datatable/buttons.dataTables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('modal/iziModal.min.css')}}" rel="stylesheet">
    <link rel="shortcut icon" href="{{{ asset('admin.png') }}}">
    <!-- <script src="{{ asset('js/jquery-3.2.1.js') }}"></script> -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('js/alertify.min.js')}}"></script>
    <script src="{{asset('js/datatable/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/datatable/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('js/datatable/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('js/datatable/buttons.print.min.js')}}"></script>
    <script src="{{asset('vendor/datatables/buttons.server-side.js')}}"></script>
</head>
@endsection
@section('sidebar')
<div class="col-md-2 list-group text-font">
  <a class="list-group-item sub-color-bg"  href="#"><i class="fa fa-graduation-cap fa-2x text-success"></i> <b>L. 400 STUDENTS</b></a>
  <ul class="list-group" id="single">
    <a class="list-group-item" data-target="{{route('json')}}" href="#"><i class="fa fa-user"></i> &nbsp; REGIST. USERS <span class="badge">{{$count}}</span></a>
    <a class="list-group-item" href="#" data-target="{{route('approve')}}"><i class="fa fa-check" style="color:green;"></i> &nbsp; APPROVED<span class="badge" style="background-color:green;">{{$num}}</span></a>
    <a class="list-group-item" href="#"  data-target="{{route('reject')}}"><i class="fa fa-times" style="color:red"></i> &nbsp; REJECTED<span class="badge" style="background-color:red;">{{$rej}}</span></a>
    <a class="list-group-item" href="#" data-target="{{route('pending')}}"><i class="fa fa-spinner fa-fw fa-pulse text-warning"></i>&nbsp; PENDING <span class="badge" style="background-color:#F5AB35;">{{$pending}}</span></a>
  </ul>
  <a class="list-group-item ft-color-bg"  href="#"><i class="fa fa-book fa-2x text-success"></i> <b>L.100-300 STUDENTS</b></a>
  <ul class="list-group" id="single">
    <a class="list-group-item" href="#" data-target="{{route('Jnrjson')}}"><i class="fa fa-user"></i> &nbsp; REGIST. USERS <span class="badge">{{$Jnr_count}}</span></a>
    <a class="list-group-item" href="#" data-target="{{route('Jnrapprove')}}"><i class="fa fa-check" style="color:green;"></i> &nbsp; APPROVED<span class="badge" style="background-color:green;">{{$Jnr_num}}</span></a>
    <a class="list-group-item" href="#" data-target="{{route('Jnrreject')}}"><i class="fa fa-times" style="color:red"></i> &nbsp; REJECTED<span class="badge" style="background-color:red;">{{$Jnr_rej}}</span></a>
    <a class="list-group-item" href="#" data-target="{{route('Jnrpending')}}"><i class="fa fa-spinner fa-fw fa-pulse text-warning"></i>&nbsp; PENDING <span class="badge" style="background-color:#F5AB35;">{{$Jnr_pending}}</span></a>
  </ul>
</div>

@endsection

@section('admin-nav')
<nav class="navbar navbar-default sub-color-bg text-font">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
            <a class="navbar-brand" href="{{route('admin.dashboard')}}">The Educational Support Services</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav" id="single">
                <li><a href="#" data-target="{{route('home-tab')}}">H & F Rquests <span class="sr-only">(current)</span></a></li>
{{--                <li><a href="#" data-target="{{ route('register') }}"> <i class="fa fa-user-plus" aria-hidden="true"></i> Add Student</a></li>--}}
                <li><a href="#" data-target="{{route('addStaff')}}"><i class="fa fa-user-plus" aria-hidden="true"></i> Manage Staff</a></li>
            </ul>
            <ul class="nav navbar-nav">
                  <li id="single"><a href="#" data-target="{{route('swift.index')}}">Events</a></li>
                  <li id="single"><a href="#" data-target="{{route('misc.index')}}">Societies</a></li>
              <li id="single"><a href="#" data-target="{{route('graduated')}}">Graduated students</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a>Welcome, {{Auth::user()->jobtitle}}</a></li>
                <li><a href="" onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
          Logout
    </a>
                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </div>
    </div>
</nav>
<main id="admin-main" class="ft-color">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <h2 style="font-family: 'Cormorant', serif; color:white;"><span class="fa fa-gavel" aria-hidden="true"></span> THE P.E.R <small class="white">(Green Book Management)</small></h2>
            </div>



            <div class="pull-right" style="margin-right:10px !important;">
                <a href=""><img class="img-circle avatar" src="{{asset('logoo.png')}}" alt="admin Image" /></a>
            </div>
        </div>
    </div>
</main>
<div class="admin-bread">
    <section id="breadcrumb">
        <ol class="breadcrumb">
            <li class="active">Admin Panel</li>
            <li class=""><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
        </ol>
    </section>
</div>
@endsection

@section('footer')
<footer class="admin-footer sub-color-bg cinzel">
  &copy; RUCST, 2018
</footer>
@endsection
