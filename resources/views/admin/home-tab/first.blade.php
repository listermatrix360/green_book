<div class="panel panel-default">
  <div class="panel-heading">
     <h4 class="text-font panel-title">Pending Health & Fitness Requests</h4>
  </div>
  <div class="panel-body">
    <div class="col-md-9">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs text-font" role="tablist">
        <li role="presentation" class="active"><a href="#Community" aria-controls="Community" role="tab" data-toggle="tab">Community Service <span class="badge" style="background-color:#F5AB35;">{{$num}}</span> </a></li>
        <li role="presentation"><a href="#Chapel" aria-controls="Chapel" role="tab" data-toggle="tab">Spiritual Fitness <span class="badge" style="background-color:#F5AB35;">{{$c_s}}</span> </a></li>
        <li role="presentation"><a href="#Physical" aria-controls="Physical" role="tab" data-toggle="tab">Physical Fitness <span class="badge" style="background-color:#F5AB35;">{{$c_p}}</span></i></a></li>
        <!-- <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings <i class="fa fa-gavel" aria-hidden="true"></i></a></li> -->
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="Community">
          <br />

          @if($comm->isEmpty())
          No request have been made recently
          @else
          @foreach($comm as $labor)
          <form class="text-font labour-form">
            {{csrf_field()}}
            <div class="row">
              <div>
                <label class=" col-sm-1">ID</label>
                <div class="col-sm-2">
                  <input class="form-control" name="reg_num" value="{{$labor->reg_num}}" readonly/>
                  <input class="form-control" type="hidden" name="link" value="{{$labor->id}}" readonly/>
                </div>
              </div>

              <div>
                <label class=" col-sm-1">Date</label>
                <div class="col-sm-2">
                  <input class="form-control" name="date" value="{{$labor->date}}" readonly/>
                </div>
              </div>


              <div>
                <label class=" col-sm-1">Activity</label>
                <div class="col-sm-3">
                  <textarea class="form-control" cols="40" rows="2" readonly>{{$labor->activity}}</textarea>
                </div>
              </div>

              <div class="">
                <button class="btn btn-success" id="approve" value="Approved"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></button>
                <a class="btn btn-danger" id="reject" href="Rejected"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></a>
              </div>
            </div>
          </form>
          <br />
          @endforeach
          @endif
        </div>

        <div role="tabpanel" class="tab-pane" id="Chapel">
          <br />

          @if($chapel->isEmpty())
          No request have been made recently
          @else
          @foreach($chapel as $service)
          <form class="text-font chapel-form">
            {{csrf_field()}}
            <div class="row">
              <div>
                <label class=" col-sm-1">ID</label>
                <div class="col-sm-2">
                  <input class="form-control" name="reg_num" value="{{$service->reg_num}}" readonly/>
                  <input class="form-control" type="hidden" name="link" value="{{$service->id}}" readonly/>
                </div>
              </div>

              <div>
                <label class=" col-sm-1">Date</label>
                <div class="col-sm-2">
                  <input class="form-control" name="date" value="{{$service->date}}" readonly/>
                </div>
              </div>


              <div>
                <label class=" col-sm-1">Activity</label>
                <div class="col-sm-3">
                  <textarea class="form-control" cols="40" rows="2" readonly>{{$service->activity}}</textarea>
                </div>
              </div>

              <div class="">
                <button class="btn btn-success" id="approve" value="Approved"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></button>
                <a class="btn btn-danger" id="reject" href="Rejected"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></a>
              </div>
            </div>
          </form>
          <br />
          @endforeach
          @endif
      </div>
        <div role="tabpanel" class="tab-pane" id="Physical">
          <br />
          @if($physical->isEmpty())
          No request have been made recently
          @else
          @foreach($physical as $fitness)
          <form class="text-font physical-form">
            {{csrf_field()}}
            <div class="row">
              <div>
                <label class=" col-sm-1">ID</label>
                <div class="col-sm-2">
                  <input class="form-control" name="reg_num" value="{{$fitness->reg_num}}" readonly/>
                  <input class="form-control" type="hidden" name="link" value="{{$fitness->id}}" readonly/>
                </div>
              </div>

              <div>
                <label class=" col-sm-1">Date</label>
                <div class="col-sm-2">
                  <input class="form-control" name="date" value="{{$fitness->date}}" readonly/>
                </div>
              </div>


              <div>
                <label class=" col-sm-1">Activity</label>
                <div class="col-sm-3">
                  <textarea class="form-control" cols="40" rows="2" readonly>{{$fitness->activity}}</textarea>
                </div>
              </div>

              <div class="">
                <button class="btn btn-success" id="approve" value="Approved"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></button>
                <a class="btn btn-danger" id="reject" href="Rejected"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></a>
              </div>
            </div>
          </form>
          <br />
          @endforeach
          @endif
        </div>
      </div>
    </div>

      </div>

</div>



<script type="text/javascript">
  $(document).ready(function(){

    alertify.defaults.transition = "slide";
    alertify.defaults.theme.ok = "btn btn-primary";
    alertify.defaults.theme.cancel = "btn btn-danger";
    alertify.defaults.theme.input = "form-control";

    $('.labour-form').on('submit',function(e){
          e.preventDefault();
          var myform = $(this).closest("form");
          var state = $('#approve').val();
          var records = myform.serialize();

          alertify.prompt('Community Service', 'Add Your Comment', 'null',function(evt, value) {
            console.log(value,state);
            $.ajax({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              method:'post',
              url:'admin/com/post/'+state+'/'+value,
              data:records,
              success:function(data){
                console.log(data);
                $.ajax({
                  type:'get',
                  url:'{{route('home-tab')}}',
                  success:function(data){
                    $('#pager').html(data);
                  }
                })
              },
              error:function()
              {

                if(x.status==401)
                {
                  window.location.replace('{{route('admin.login')}}');
                }

                alert('posting error')
              }
            })
          }
          , function() {
            // alertify.error('Cancel')
          }
        ).set({transition:'fade'}).show();
    });
    $('.labour-form #reject').on('click',function(e){
      e.preventDefault();

          var myform = $(this).closest("form");
          var state = $(this).attr('href');
          var records = myform.serialize();

          alertify.prompt('Community Service', 'Reason for the rejection', 'null',function(evt, value) {
            console.log(value,state);
            $.ajax({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              method:'post',
              url:'admin/com/post/'+state+'/'+value,
              data:records,
              success:function(data){
                console.log(data);
                $.ajax({
                  type:'get',
                  url:'{{route('home-tab')}}',
                  success:function(data){
                    $('#pager').html(data);
                  }
                })
              },
              error:function(x,e){
                if(x.status==401)
                {
                  window.location.replace('{{route('admin.login')}}');
                }
                alert('posting error')
              }
            })
          }
          , function() {
            // alertify.error('Cancel')
          }
        );
    });

//
$('.chapel-form').on('submit',function(e){
      e.preventDefault();
      var myform = $(this).closest("form");
      var state = $('#approve').val();
      var records = myform.serialize();

      alertify.prompt('Community Service', 'Add Your Comment', 'null',function(evt, value) {
        console.log(value,state);
        $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          method:'post',
          url:'admin/cha/post/'+state+'/'+value,
          data:records,
          success:function(data){
            console.log(data);
            $.ajax({
              type:'get',
              url:'{{route('home-tab')}}',
              success:function(data){
                $('#pager').html(data);
              }
            })
          },
          error:function(x,e){
            if(x.status==401)
            {
              window.location.replace('{{route('admin.login')}}');
            }
            alert('posting error')
          }
        })
      }
      , function() {
        // alertify.error('Cancel')
      }
    ).set({transition:'fade'}).show();
});
$('.chapel-form #reject').on('click',function(e){
  e.preventDefault();
      var myform = $(this).closest("form");
      var state = $(this).attr('href');
      var records = myform.serialize();

      alertify.prompt('Community Service', 'Reason for the rejection', 'null',function(evt, value) {
        console.log(value,state);
        $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          method:'post',
          url:'admin/cha/post/'+state+'/'+value,
          data:records,
          success:function(data){
            console.log(data);
            $.ajax({
              type:'get',
              url:'{{route('home-tab')}}',
              success:function(data){
                $('#pager').html(data);
              }
            })
          },
          error:function(x,e){
            if(x.status==401)
            {
              window.location.replace('{{route('admin.login')}}');
            }
            alert('posting error')
          }
        })
      }
      , function() {
        // alertify.error('Cancel')
      }
    );
});

//physical Fitness
$('.physical-form').on('submit',function(e){
      e.preventDefault();
      var myform = $(this).closest("form");
      var state = $('#approve').val();
      var records = myform.serialize();

      alertify.prompt('Community Service', 'Add Your Comment', 'null',function(evt, value) {
        console.log(value,state);
        $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          method:'post',
          url:'admin/phy/post/'+state+'/'+value,
          data:records,
          success:function(data){
            console.log(data);
            $.ajax({
              type:'get',
              url:'{{route('home-tab')}}',
              success:function(data){
                $('#pager').html(data);
              }
            })
          },
          error:function(x,e){
            if(x.status==401)
            {
              window.location.replace('{{route('admin.login')}}');
            }
            alert('posting error')
          }
        })
      }
      , function() {
        // alertify.error('Cancel')
      }
    ).set({transition:'fade'}).show();
});
$('.physical-form #reject').on('click',function(e){
  e.preventDefault();
      var myform = $(this).closest("form");
      var state = $(this).attr('href');
      var records = myform.serialize();

      alertify.prompt('Community Service', 'Reason for the rejection', 'null',function(evt, value) {
        console.log(value,state);
        $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          method:'post',
          url:'admin/phy/post/'+state+'/'+value,
          data:records,
          success:function(data){
            console.log(data);
            $.ajax({
              type:'get',
              url:'{{route('home-tab')}}',
              success:function(data){
                $('#pager').html(data);
              }
            })
          },
          error:function(x,e){
            if(x.status==401)
            {
              window.location.replace('{{route('admin.login')}}');
            }
            alert('posting error')
          }
        })
      }
      , function() {
        // alertify.error('Cancel')
      }
    );
});

  })
</script>
