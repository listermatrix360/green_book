sdsddsss
<div class="col-md-12">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs text-font" role="tablist">
        <li role="presentation"><a href="#basic_info" class="active" aria-controls="basic_info" role="tab" data-toggle="tab"><i class="fa fa-user-o" aria-hidden="true"></i> Basic Info</a></li>
        <li role="presentation"><a href="#diary_content" aria-controls="diary_content" role="tab" data-toggle="tab"> <i class="fa fa-book" aria-hidden="true"></i> Diary</a></li>
        <li role="presentation"><a href="#internship" aria-controls="internship" role="tab" data-toggle="tab"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Internship</a></li>
        <li role="presentation"><a href="#prof_skills" aria-controls="prof_skills" role="tab" data-toggle="tab"><i class="fa fa-bolt" aria-hidden="true"></i> Professional Skills</a></li>
        <li role="presentation"><a href="#education" aria-controls="education" role="tab" data-toggle="tab"><i class="fa fa-university" aria-hidden="true"></i> Education & Examination.</a></li>
        <li role="presentation"><a href="#other" aria-controls="other" role="tab" data-toggle="tab"><i class="fa fa-university" aria-hidden="true"></i> Other Courses Attended</a></li>
        <li role="presentation"><a href="#community" aria-controls="community" role="tab" data-toggle="tab"><i class="fa fa-h-square" aria-hidden="true"></i> Communty Services</a></li>
        <li role="presentation"><a href="#chapel" aria-controls="chapel" role="tab" data-toggle="tab"><i class="fa fa-plus-square" aria-hidden="true"></i> Chapel Attendance</a></li>
        <li role="presentation"><a href="#physical" aria-controls="physical" role="tab" data-toggle="tab"><i class="fa fa-blind" aria-hidden="true"></i> Physical Fitness</a></li>
        <li role="presentation"><a href="#autobio" aria-controls="autobio" role="tab" data-toggle="tab">Autobiography</a></li>
        <li role="presentation"><a href="#ESS" aria-controls="ESS" role="tab" data-toggle="tab"><i class="fa fa-cogs fa-spin" aria-hidden="true"></i> Educational Support Services</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="basic_info">
            <br />
            <!-- Personal Details -->
            <div class="col-md-7">
                <div class="panel panel-default">
                    <div class="panel-heading photobox">
                        <div class="row">
                            <div class="col-md-5">
                                <h4 class="text-font">Personal Details</h4>
                            </div>
                            @if(count($users)==1 && $users->img !='null')
                            <div class="photo-single pull-right">
                                <img class="img-rounded" id="avatar" src="{{asset('storage/greenFolder/'.$users->img)}}" />
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="panel-body">
                        @if(count($users)===0)
                        <h4>This Student has not provided any relevant information for this section</h4>
                        @else
                        <div class="col-md-12">
                            <form class="form-horizontal" role="form">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="first_name" class="col-md-3 control-label"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Full name</label>
                                    <div class="col-md-5">
                                        <input type="text" name="name" class="form-control" value="{{$users->title}} {{$users->surname}} {{$users->othernames}}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="last_name" class="col-md-3 control-label"><i class="fa fa-id-card prefix" aria-hidden="true"></i> ID Number</label>
                                    <div class="col-md-4">
                                        <input id="last_name" name="reg_num" type="number" class="form-control" value="{{ $users->reg_num }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="last_name" class="col-md-3 control-label"><i class="fa fa-graduation-cap prefix" aria-hidden="true"></i> School</label>
                                    <div class="col-md-8">
                                        <input id="school" name="school" type="text" value="{{$users->school}}" class="form-control" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="last_name" class="col-md-3 control-label"><i class="fa fa-calendar" aria-hidden="true"></i> Date of Birth  </label>
                                    <div class="col-md-8">
                                        <input id="school" name="school" type="text" value="{{$users->dob}}" class="form-control" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="last_name" class="col-md-3 control-label"><i class="fa fa-mercury" aria-hidden="true"></i> Gender</label>
                                    <div class="col-md-8">
                                        <input id="school" name="school" type="text" value="{{$users->gender}}" class="form-control" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="last_name" class="col-md-3 control-label"><i class="fa fa-flag-checkered" aria-hidden="true"></i> Nationality</label>
                                    <div class="col-md-8">
                                        <input id="school" name="school" type="text" value="{{$users->nationality}}" class="form-control" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="last_name" class="col-md-3 control-label"><i class="fa fa-clock-o" aria-hidden="true"></i> Session</label>
                                    <div class="col-md-8">
                                        <input id="school" name="school" type="text" value="{{$users->session}}" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="last_name" class="col-md-3 control-label"><i class="fa fa-book prefix" aria-hidden="true"></i> Program</label>
                                    <div class="col-md-7">
                                        <input id="program" name="program" type="text" class="form-control" value="{{$users->program}}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="last_name" class="col-md-3 control-label"><i class="fa fa-users prefix" aria-hidden="true"></i> Society</label>
                                    <div class="col-md-4">
                                        <input id="society" name="society" type="text" class="form-control" value="{{$users->society}}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="last_name" class="col-md-3 control-label"><i class="fa fa-phone prefix" aria-hidden="true"></i> Mobile No. 1</label>
                                    <div class="col-md-4">
                                        <input name="phone1" type="number" class="form-control" value="{{$users->phone1}}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="last_name" class="col-md-3 control-label"><i class="fa fa-phone prefix" aria-hidden="true"></i> Mobile No. 2</label>
                                    <div class="col-md-4">
                                        <input name="phone2" type="number" value="{{$users->phone2}}" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="last_name" class="col-md-3 control-label"><i class="fa fa-envelope prefix" aria-hidden="true"></i> Regent E-mail</label>
                                    <div class="col-md-7">
                                        <input name="email1" type="email" value="{{$users->email}}" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="last_name" class="col-md-3 control-label"><i class="fa fa-envelope prefix" aria-hidden="true"></i> Personal E-mail</label>
                                    <div class="col-md-7">
                                        <input name="email2" type="email" value="{{$users->email2}}" class="form-control" readonly>
                                    </div>
                                </div>
                            </form>
                        </div>

                        @endif
                    </div>
                    <div class="panel-footer text-font sub-color-bg">
                        Regent University College Of Science and Technology
                    </div>
                </div>
            </div>
            <!-- Mentor Details -->
            <div class="col-md-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="text-font">Mentor Details</h4>
                    </div>
                    <div class="panel-body">
                        @if(count($mentor)===0)
                        <h4 class=" text-font">This Student has not supplied any information for this section It is recommended you alert the student on this matter</h4>
                        @else
                        <table class="table text-font table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>LEVEL</th>
                                    <th>NAME OF MENTOR</th>
                                    <th>STATUS</th>
                                </tr>
                            </thead>
                            <tbody class="">
                                @foreach ($mentor as $mentors)
                                <tr>
                                    <td><b>{{$mentors->level}}</b></td>
                                    <td><b>{{$mentors->title}} {{$mentors->f_name}} {{$mentors->las_name}}</b></td>
                                    <td><b>{{$mentors->status}}</b></td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                        @endif
                    </div>
                    <div class="panel-footer text-font sub-color-bg">
                        Regent University College Of Science and Technology
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="diary_content">
            <br /> @if($diary->isEmpty()) Nothing Here!! @else @foreach($diary as $story)
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="text-font panel-title">{{$story->title}} <small>({{$story->date}})</small></h4>
                    </div>
                    <div class="panel-body" id="body">
                        {!!html_entity_decode($story->body)!!}
                    </div>

                </div>
            </div>
            @endforeach @endif
        </div>
        <div role="tabpanel" class="tab-pane fade" id="internship">Intersnhip</div>
        <div role="tabpanel" class="tab-pane fade" id="community">
          <br />

          <div class="col-md-12">
              <div class="panel panel-default">
                  <div class="panel-heading ft-color">
                      <h4 class="text-font panel-title">Community Services attended
                      </h4>

                  </div>
                  <div class="panel-body">
                      <div class="col-md-7">
                          @if($community->isEmpty())
                          <h4 class=" text-font">This Student has not supplied any information for this section It is recommended you alert the student on this matter</h4>
                          @else
                          <table class="table text-font table-condensed table-striped table-responsive">
                              <thead>
                                  <tr>
                                      <th>Date</th>
                                      <th>Activity Undertaken</th>
                                      <th>Status</th>
                                      <th>E.S.S Comment</th>
                                  </tr>
                              </thead>
                              <tbody class="">
                                  @foreach ($community as $labour)
                                  <tr>
                                      <td>{{$labour->date}}</td>
                                      <td>{{$labour->activity}}</td>
                                      <td>{{$labour->status}}</td>
                                      <td>{{$labour->comment}}</td>
                                  </tr>
                                  @endforeach
                              </tbody>
                          </table>
                          @endif
                      </div>
                      <div class="col-md-5">
                          <h4 class="text-font">Remarks</h4>
                          @if(count($c_remark) !=0)
                          <form class="form-horizontal text-font" role="form">
                              <div class="form-group">
                                  <label class="control-label col-md-2">Student</label>
                                  <div class="col-md-10">
                                      <input type="text" class="form-control" value="{{$c_remark->student_remark}}" readonly/>
                                  </div>
                              </div>

                              <div class="form-group">
                                  <label class="control-label col-md-2">Warden</label>
                                  <div class="col-md-10">
                                      <input type="text" class="form-control" value="{{$c_remark->warden_remark}}" readonly/>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="control-label col-md-2">The E.S.S</label>
                                  <div class="col-md-10">
                                      <textarea placeholder="{{$c_remark->ess_remark}}" rows="4" cols="40" readonly></textarea>
                                  </div>
                              </div>
                          </form>
                          @endif
                      </div>
                  </div>
              </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="chapel">
          <br />
          <div class="col-md-12">
              <div class="panel panel-default">
                  <div class="panel-heading ft-color">
                      <h4 class="text-font panel-title">Chapel Services Attended</h4>
                  </div>
                  <div class="panel-body">
                      <div class="col-md-7">
                          @if($community->isEmpty())
                          <h4 class=" text-font">This Student has not supplied any information for this section It is recommended you alert the student on this matter</h4>
                          @else
                          <table class="table text-font table-condensed table-striped table-responsive">
                              <thead>
                                  <tr>
                                      <th>Date</th>
                                      <th>Activity Undertaken</th>
                                      <th>Status</th>
                                      <th>E.S.S Comment</th>
                                  </tr>
                              </thead>


                              <tbody class="">
                                  @foreach ($chapel as $service)
                                  <tr>
                                      <td>{{$service->date}}</td>
                                      <td>{{$service->activity}}</td>
                                      <td>{{$service->status}}</td>
                                      <td>{{$service->comment}}</td>
                                  </tr>
                                  @endforeach

                              </tbody>
                          </table>
                          @endif
                      </div>
                      <div class="col-md-5">
                          <h4 class="text-font">Remarks</h4>
                          @if(count($s_remark) !=0)
                          <form class="form-horizontal text-font" role="form">
                              <div class="form-group">
                                  <label class="control-label col-md-2"><b>Student</b></label>
                                  <div class="col-md-10">
                                      <input type="text" class="form-control" value="{{$s_remark->student_remark}}" readonly/>
                                  </div>
                              </div>

                              <div class="form-group">
                                  <label class="control-label col-md-2"><b>Chaplain</b></label>
                                  <div class="col-md-10">
                                      <input type="text" class="form-control" value="{{$s_remark->chaplain_remark}}" readonly/>
                                  </div>
                              </div>

                          </form>
                          @endif
                      </div>
                  </div>
              </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="physical">
          <br />
          <div class="col-md-12">
              <div class="panel panel-default">
                  <div class="panel-heading ft-color">
                      <h4 class="text-font panel-title">Physical Fitness Attended</h4>
                  </div>
                  <div class="panel-body">
                    <div class="col-md-7">
                      @if($physical->isEmpty())
                      <h4 class=" text-font">This Student has not supplied any information for this section It is recommended you alert the student on this matter</h4>
                      @else
                      <table class="table text-font table-condensed table-striped table-responsive">
                        <thead>
                          <tr>
                            <th>Date</th>
                            <th>Activity Undertaken</th>
                            <th>Status</th>
                            <th>E.S.S Comment</th>
                          </tr>
                        </thead>
                        <tbody class="">
                          @foreach ($physical as $physicals)
                          <tr>
                            <td>{{$physicals->date}}</td>
                            <td>{{$physicals->activity}}</td>
                            <td>{{$physicals->status}}</td>
                            <td>{{$physicals->comment}}</td>
                          </tr>
                          @endforeach

                        </tbody>
                      </table>
                      @endif

                    </div>
                    <div class="col-md-5">
                        <h4 class="text-font">Remarks</h4>
                        @if(count($p_remark) !=0)
                        <form class="form-horizontal text-font" role="form">
                            <div class="form-group">
                                <label class="control-label col-md-2"><b>Student</b></label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="{{$p_remark->student_remark}}" readonly/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2"><b>Chaplain</b></label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="{{$p_remark->chaplain_remark}}" readonly/>
                                </div>
                            </div>
                        </form>
                        @else
                        No Remark Has been Provided Yet!!
                        @endif
                    </div>
                  </div>
              </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="prof_skills">
          <br />
          @if($prof->isEmpty())
              No information Has Been Provided
              @else
                @foreach($prof as $skills)
                <div class="col-md-6">
                  <div class="panel panel-default">
              <div class="panel-heading">
                 <h4 class="text-font panel-title">Tasks Which Demonstrates Professional
                 <small class="pull-right"> </small></h4>
              </div>
              <div class="panel-body text-font">
                {{$skills->title}}
                {!!html_entity_decode($skills->prof_skill_body)!!}
              </div>
           </div>
                </div>
          @endforeach
          @endif
          </div>
        <div role="tabpanel" class="tab-pane fade" id="education">
          <br />
          @if($education->isEmpty())
            No data available
            @else
            <div class="col-md-12">
              <table class="table table-responsive table-striped">
              <thead>
                <tr>
                  <th>From</th>
                  <th>To</th>
                  <th>School</th>
                  <th>Study Method</th>
                  <th>No of Exams taken</th>
                  <th>Level</th>
                  <th>Paper</th>
                  <th>Results</th>
                  <th>Remarks</th>
                </tr>
              </thead>
              <tbody>
                @foreach($education as $show)
                  <tr>
                    <td class="active">{{$show->st_dt}}</td>
                    <td>{{$show->en_dt}}</td>
                    <td class="success">{{$show->sch_name}}</td>
                    <td>{{$show->study_mtd}}</td>
                    <td>{{$show->ex_taken}}</td>
                    <td>{{$show->level}}</td>
                    <td>{{$show->paper}}</td>
                    <td>{{$show->results}}</td>
                    <td>{{$show->remarks}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            </div>

          @endif
        </div>
        <div role="tabpanel" class="tab-pane fade" id="other">
          <br />
          @if($other->isEmpty())
          Not Detail Has Been Provided
            @else
            <div class="col-md-11">
              <table class="table table-striped text-font table-responsive">
              <thead>
                <tr>
                  <th>From</th>
                  <th>To</th>
                  <th>Course Name </th>
                  <th>Description</th>
                  <th>Remarks</th>
                </tr>
              </thead>
              <tbody>
                @foreach($other as $cos)
                <tr>
                  <td>{{$cos->date1}}</td>
                  <td>{{$cos->date2}}</td>
                  <td>{{$cos->cours_name}}</td>
                  <td>{{$cos->descrip}}</td>
                  <td>{{$cos->remarks}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
            </div>
          @endif
        </div>
        <div role="tabpanel" class="tab-pane fade" id="ESS">
          <br />
          @if(count($action)==0)
            <div class="col-md-7">
              <div class="panel panel-default">
                <div class="panel-heading ft-color">
                   <h4 class="text-font white panel-title">Approve || Reject PER</h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal text-font" id="PER-FORM" role="form" action="" method="post">
                          {{csrf_field()}}
                          <div class="form-group">
                            <label class="col-md-2">Id Num.</label>
                            <div class="col-md-6">
                              <input type="number" class="form-control" name="reg_num" value="{{$uzzers->reg_num}}" readonly>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="col-md-2">Id Num.</label>
                            <div class="col-md-6">
                              <input type="email" class="form-control" name="email" value="{{$uzzers->email}}" readonly/>
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="col-md-2">Action</label>
                            <div class="col-md-6">
                              <select class="form-control selectpicker" name="action">
                                <option></option>
                                <option name="action">Approved</option>
                                <option name="action">Rejected</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-2">Comments</label>
                            <div class="col-md-6">
                              <textarea class="form-control" row="10" name="comment" required></textarea>
                            </div>
                          </div>

                          <div class="col-md-offset-4">
                            <button class="btn btn-primary" id="per-button">Submit <i class="fa fa-send" aria-hidden="true"></i></button>
                          </div>
              </form>
                  </div>
            </div>
            </div>
            @elseif($action->prof_status=='Approved')
            <br />
            <div class="col-md-5">
                  <div class="alert alert-success alert-dismissible tex-font" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong><i class="fa fa-thumbs-o-up fa-5x" aria-hidden="true"></i></strong> PER of this student has been approved.
                    </div>
                <div id="revert" class="text-font">
                  <a class="btn btn-success" href="#" data-target="{{'admin/show/status/'.$action->reg_num}}">Revert Changes</a>
                </div>
              </div>
            @elseif($action->prof_status=='Rejected')
            <br />
            <div class="col-md-5">
              <div class="alert alert-danger alert-dismissible tex-font" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><i class="fa fa-thumbs-o-down fa-5x" aria-hidden="true"></i></strong>  PER of this student has been rejected.
                </div>
              <div id="revert" class="text-font">
                <a class="btn btn-danger" href="#" data-target="{{'admin/show/status/'.$action->reg_num}}">Undo Changes</a>
              </div>
            </div>
            @endif

            <div class="col-md-5 text-font">
                <button class="btn ft-color btn-report"><i class="fa fa-file-text-o" aria-hidden="true"></i> Genereate Report</button>
            </div>
        </div>
        <div class="col-md-6 tab-pane fade" role="tabpanel" id="autobio">
            Something Else Here
        </div>
    </div>
</div>
<!-- <div class=""> -->
  <iframe src="" id="content">

  </iframe>
<!-- </div> -->
<script>
  $(document).ready(function(){
    $('#revert a').on('click', function(e){
        e.preventDefault();
        var target = $(this).data('target');
      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method:'get',
        url:target,
        data:{
          '_token':$('input[name=_token]').val()
        },
        success:function(data){
              $.ajax({
                  type:'get',
                  url:'{{'admin/show/'.$uzzers->reg_num}}',
                  success:function(data){
                  $('#pager').html(data);
                  $('select').selectpicker('refresh');
                  },
                  error:function(x,e){
                    if(x.status==401){
                      window.location.replace('/admin');
                    }
                  }
                })
        },
        error:function(x,e){
          if(x.status ==401){
              window.location.replace('/admin');
          }
          alert('There is an error');
        }
      })
       })

    $('#PER-FORM').on('submit',function(e){
        var btn = $('#per-button');
        e.preventDefault();
        var myform = $(this).closest('form');
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type:'POST',
          url:'{{route('profile_action')}}',
          data:myform.serialize(),
          beforeSend:function()
          {
            $(btn).html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i>')
          },
          success:function(data)
          {
            alertify.set('notifier','position', 'top-center');
            alertify.set('notifier','delay',10);
            alertify.success('Notification Sent !!!');
            $.ajax({
                type:'get',
                url:'{{'admin/show/'.$uzzers->reg_num}}',
                success:function(data){
                $('#pager').html(data);
                $('select').selectpicker('refresh');
                },
                error:function(x,e){
                  if(x.status==401){
                    window.location.replace('/admin');
                  }
                }
              })
          },
          error:function(x,e)
          {
            if(x.status==401)
            {
              window.location.replace('/admin');
            }
            alertify.set('notifier','position', 'top-center');
            alertify.set('notifier','delay',10);
            alertify.error('Status Saved But Notification Could Not Be Sent, Check your internet conncection');

            $.ajax({
                type:'get',
                url:'{{'admin/show/'.$uzzers->reg_num}}',
                success:function(data){
                $('#pager').html(data);
                $('select').selectpicker('refresh');
                },
                error:function(x,e){
                  if(x.status==401){
                    window.location.replace('/admin');
                  }
                }
              })
          }

        });
    });

    $('.btn-report').on('click',function(e){
      e.preventDefault();
        $('#printa').get(0).contentWindow.print();
    });
          // $.ajax({
          //   type:'get',
          //   url:'{{route('frame',["reg_num"=>$users->reg_num])}}',
          //   success:function(data){
          //       $('iframe').html(data);
          //   }
          // })
  });
</script>
