<div id="dashboard">

<!DOCTYPE html>
<html>
<title></title>
<head>
  <link rel="stylesheet" href="{{asset('css/paper.min.css')}}" />
  <script src="{{asset('js/jquery-3.2.1.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#SingleForm').on('submit',function(e){
        e.preventDefault();
        var email = $('#email').val();
        var password = $('#password').val();
        var radio = $('#radio').val();
        console.log(radio);
        if($('#radio').is(':checked'))
          
        {
          $.ajax({
            type:'POST',
            url:'{{route('admin.login.submit')}}',
            datatype:'json',
            data:{
              email:email,
              password:password,
              '_token':$('input[name=_token]').val()
            },
            success:function(data){
              window.location.replace('{{route('admin.dashboard')}}')
            },
            error:function(){
              console.log('post failed');
            }
          })
        }
        else{
          $.ajax({
            type:'POST',
            url:'{{route('login')}}',
            datatype:'json',
            data:{
              email:email,
              password:password,
              '_token':$('input[name=_token]').val()
            },
            success:function(){
              console.log('done');
              window.location = '/book'
            },
            error:function(){
              console.log('post failed');
            },
          });
        }

      });
    });
  </script>
</head>
<body>
  <div class="container">
    <br />
    <div class="col-md-7 col-md-offset-2">
        <br />
          <br />
            <br />
      <div class="col-md-offset-2">
      <form class="form-horizontal" id="SingleForm">
        {{csrf_field()}}
        <input type="checkbox" value="admin" id="radio" />
        <div class="form-group">
          <label class="control-label col-md-4">Email Addreess</label>
          <div class="col-md-6">
            <input type="text" class="form-control" id="email" placeholder="email"/>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4">Passeword</label>
          <div class="col-md-6">
            <input type="password" id="password" class="form-control" placeholder="password"/>
          </div>
        </div>
        <div class="text-center">
          <button class="btn btn-success">Login</button>
        </div>
      </form>
    </div>
    </div>
  </div>
</body>
</html>
</div>
