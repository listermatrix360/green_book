
<div class="col-md-11">
  <div class="panel panel-default">
    <div class="panel-heading header-color-bg">
      <h4 class="text-font panel-title" style="color:white">Rejected L100-L300 Profiles</h4>
    </div>
    <div class="panel-body">
      <table class="table table-striped table-hover text-font" id="dataTableBuilder">
    <thead>
        <tr>
            <th>Id</th>
            <th>Surname</th>
            <th>Other names</th>
            <th>Program</th>
            <th>Status</th>
            <th>E.S.S Comment</th>
            <th>Profile</th>
        </tr>
    </thead>

    <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>

        </tr>
    </tfoot>

</table>
    </div>
    <div class="panel-footer sub-color-bg">
      Regent University College Of Science and Technology
    </div>
  </div>
</div>
  <script type = "text/javascript">
   (function (window, $) {
     $('#dataTableBuilder').on('click','#getajax',function(event){
       event.preventDefault();
       var $this = $(this);
       target = $this.data('target');
       console.log(target);
       $.ajax({
         headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         method: 'GET',
         url: target,
         beforeSend: function(){
           $('#pager').html('Please Wait')
         },
         success:function(data){
           $('#pager').html(data)
             $('select').selectpicker('refresh');
         },
         error:function(x,e){
           if(x.status==401)
           {
             window.location.replace('{{route('admin.login')}}');
           }
         }
       });
     });

      window.LaravelDataTables = window.LaravelDataTables || {};
      window.LaravelDataTables["dataTableBuilder"] = $("#dataTableBuilder").DataTable({
          "serverSide": true,
          "processing": true,
          "columnDefs":[

            {"width":"10%", "targets":0},
            {"width":"15%", "targets":1},
            {"width":"20%", "targets":2},
            {"width":"10%", "targets":3},
            {"width":"20%", "targets":4},
            {"width":"10%", "targets":5},
        ],
          "ajax": {
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
              "url": "{{route('Jnrreject')}}",
              "type": "GET",
              "data": function (data) {
                  for (var i = 0, len = data.columns.length; i < len; i++) {
                      if (!data.columns[i].search.value) delete data.columns[i].search;
                      if (data.columns[i].searchable === true) delete data.columns[i].searchable;
                      if (data.columns[i].orderable === true) delete data.columns[i].orderable;
                      if (data.columns[i].data === data.columns[i].name) delete data.columns[i].name;
                  }
                  delete data.search.regex;
              }
          },
          "columns": [{
              data: 'reg_num',
              name:'profile_statuses.reg_num',
              orderable: true,
              searchable: true,

          },
          {
            data: 'surname',
            name:'users.surname',
            orderable: true,
            searchable: true
          },
          {
            data: 'othernames',
            name:'users.othernames',
            orderable: true,
            searchable: true
          },

          {
            data:'program',
            name:'personalDetail.program',
            orderable:true,
            searchable:true,
          },

          {
            data:'prof_status',
            name:'profile_statuses.prof_status',
            orderable:true,
            searchable:true,
          },

          {
            data: 'ess_comm',
            name:'profile_statuses.ess_comm',
            orderable: true,
            searchable: true
          },
          {
            "defaultContent": "",
            "name": "Profile",
            "data": "action",
            "title": "P.E.R | | EDIT | | STATUS",
            "orderable": false,
            "searchable": false
          }
          ],
          "dom": "Bfrtip",
          "order": [[0, "desc"]],
          "buttons": ["export", "pdf", "print", "reset", "reload"],
          "initComplete": function () {

              this.api().columns().every(function () {
                  var column = this;
                  var input = document.createElement("input");
                  $(input).css({
                    'width':'100%','display':'inline-block'
                  });
                  $(input).appendTo($(column.footer()).empty())
                      .on('change', function () {
                          column.search($(this).val(), false, false, true).draw();
                      });
              });
          }
      });
  })(window, jQuery);
  </script>
</body>
