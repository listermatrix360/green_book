<div class="col-md-12">
  <div class="panel panel-default">
    <div class="panel-heading ft-color">
      <h4 class="text-font panel-title" style="color:white;">Pending PER</h4>
    </div>
    <div class="panel-body">
      <table class="table table-striped table-hover text-font" id="dataTableBuilder">
        <thead class="">
          <tr>
            <th>Id</th>
            <th>Surname</th>
            <th>Other names</th>
            <th>Program</th>
            <th>Profile</th>
          </tr>
        </thead>

        <tfoot>
          <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
        </tfoot>

      </table>
    </div>
    <div class="panel-footer sub-color-bg">
      Regent University College Of Science and Technology
    </div>
  </div>
</div>
  <script type = "text/javascript">
   (function (window, $) {

     $('#dataTableBuilder').on('click','#getajax',function(event){
       event.preventDefault();
       var $this = $(this);
       target = $this.data('target');
       console.log(target);
       $.ajax({
         headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         method: 'GET',
         url: target,
         beforeSend: function(){
           $('#pager').html('Please Wait')
         },
         success:function(data){
           $('#pager').html(data)
             $('select').selectpicker('refresh');
         },
         error:function(x,e){
           if(x.status==401)
           {
             window.location.replace('{{route('admin.login')}}');
           }
         }
       });
     });

     function reload(){
       $.ajax({
         headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         type:'get',
         url:'{{route('pending')}}',
         success:function(data){
           $("#pager").html(data);
           $('select').selectpicker('refresh');
         },
         error:function(){
           console.log('Page Loading Failed!!')
         }
       });
     }
     $('.modal').on('hide.bs.modal',function(){
       setTimeout(function(){
         reload();
       },300)
     })
     $('#dataTableBuilder').on('click','#editmodal',function(event){
       event.preventDefault();
       var $this = $(this);
       target = $this.data('target');
       $.ajax({
         headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         method: 'GET',
         url: 'admin/get/user/'+target,
         success:function(data){
           $('#EditModal').modal('toggle');
           $.each(data,function(i,item){
             $('#col').html('<input type="hidden" name="id" value="'+data.id+'" class="form-control"/>');
             $('#id').html('<input type="number" name="reg_num" value="'+data.reg_num+'" class="form-control"/>');
             $('#level').html('<input type="text" name="level" value="'+data.level+'" class="form-control"/>');
             $('#title').html('<input type="text" name="title" value="'+data.title+'" class="form-control"/>');
             $('#fname').html('<input type="text" name="name" value="'+data.surname+'" class="form-control"/>');
             $('#lname').html('<input type="text" name="la_name" value="'+data.othernames+'" class="form-control"/>');
             $('#mail').html('<input type="text" name="email" value="'+data.email+'" class="form-control"/>');
           });
         },
         error:function(x,e){
           if(x.status==401)
           {
             window.location.replace('{{route('admin.login')}}');
           }
         }
       });
     });
     $('#dataTableBuilder').on('click','.state #active',function(e){
         e.preventDefault();
           $('#defer').modal();
           var data = $(this).data('toggle')
           var info = $(this).data('dismiss');
           var id   = $(this).data('target');
           var value = $(this).attr('href');
           $('#message').html('Are You Sure You Want To '+ info +' '+ data);

           $('#send').on('click',function(e){
             $.ajax({
               headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               type:'post',
               url:'{{route('defer-status')}}',
               data:{
                 id:id,
                 value:value,
               },
               success:function(data){
                 $('#defer').modal('toggle');
               },
               error:function(){
                 alert('There is an error')
               }

             })
           })
       });
      window.LaravelDataTables = window.LaravelDataTables || {};
      window.LaravelDataTables["dataTableBuilder"] = $("#dataTableBuilder").DataTable({
          "serverSide": true,
          "processing": true,
          "columnDefs":[

            {"width":"10%", "targets":0},
            {"width":"10%", "targets":1},
            {"width":"10%", "targets":2},
            {"width":"15%", "targets":3},
            {"width":"15%", "targets":4},
        ],

          "ajax": {
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
              "url": "{{route('pending')}}",
              "type": "GET",
              "data": function (data) {
                  for (var i = 0, len = data.columns.length; i < len; i++) {
                      if (!data.columns[i].search.value) delete data.columns[i].search;
                      if (data.columns[i].searchable === true) delete data.columns[i].searchable;
                      if (data.columns[i].orderable === true) delete data.columns[i].orderable;
                      if (data.columns[i].data === data.columns[i].name) delete data.columns[i].name;
                  }
                  delete data.search.regex;
              }
          },
          "columns": [{
              data: 'reg_num',
              name:'users.reg_num',
              orderable: true,
              searchable: true,

          },
          {
            data: 'surname',
            name:'surname',
            orderable: true,
            searchable: true
          },

          {
            data: 'othernames',
            name:'othernames',
            orderable: true,
            searchable: true
          },


          {
            data:'program',
            name:'personalDetail.program',
            orderable:true,
            searchable:true,
          },
          {
            "defaultContent": "",
            "name": "Profile",
            "data": "action",
            "title": "P.E.R | | EDIT | | STATUS",
            "orderable": false,
            "searchable": false
          }
          ],
          "dom": "Bfrtip",
          "order": [[0, "desc"]],
          "buttons": ["export", "pdf", "print", "reset", "reload"],
          "initComplete": function () {

              this.api().columns().every(function () {
                  var column = this;
                  var input = document.createElement("input");
                  $(input).css({
                    'width':'100%','display':'inline-block'
                  });
                  $(input).appendTo($(column.footer()).empty())
                      .on('change', function () {
                          column.search($(this).val(), false, false, true).draw();
                      });
              });
          }
      });
     $('.form').on('submit',function(event){
        event.preventDefault();
          var myform = $(this).closest("form");
            $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              type:'post',
              url:'{{route('update_user')}}',
              data:myform.serialize(),
              datatype:'json',
              success:function(data){
                $('#EditModal').modal('hide');
              },
              error:function(x,e){
                  if(x.status==401)
                  {
                    window.location.replace('{{route('admin.login')}}');
                  }

                console.log('posting failed');
              }
            })
     });
  })(window, jQuery);
  </script>
  <div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title text-font" id="myModalLabel">Edit Student Detail</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal text-font form" id="UserEditForm" role="form">
              {{ csrf_field() }}

              <div class="form-group">
                  <label for="name" class="col-md-4 control-label">REGISTRATION NO.</label>
                  <div class="col-md-6" id="id">
                      <input type="number" class="form-control" name="reg_num" value="{{ old('reg_num') }}" required autofocus>
                  </div>
              </div>

              <div class="col-md-6 hidden" id="col">

              </div>

              <div class="form-group">
                  <label for="title" class="col-md-4 control-label">LEVEL</label>

                  <div class="col-md-6" id="level">
                      <input type="text" class="form-control" name="level" value="{{ old('level') }}" required autofocus>
                  </div>
              </div>

              <div class="form-group">
                  <label for="title" class="col-md-4 control-label">TITLE</label>

                  <div class="col-md-6" id="title">
                      <input type="text" class="form-control" name="title" value="{{ old('title') }}" required autofocus>
                  </div>
              </div>
              <div class="form-group">
                  <label for="name" class="col-md-4 control-label">FIRST NAME</label>

                  <div class="col-md-6" id="fname">
                      <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                  </div>
              </div>

              <div class="form-group">
                  <label for="name" class="col-md-4 control-label">LAST NAME</label>
                  <div class="col-md-6" id="lname">
                      <input id="la_name" type="text" class="form-control" name="la_name" value="{{ old('la_name') }}" required autofocus>
                  </div>
              </div>

              <div class="form-group">
                  <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                  <div class="col-md-6" id="mail">
                      <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                  </div>
              </div>


              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="submit">Save changes</button>
              </div>
          </form>

        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="defer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title text-font" id="myModalLabel">Edit Student Detail</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal text-font form" id="instate-form" role="form">
              {{ csrf_field() }}
                <h4 class="text-font" id="message"></h4>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a class="btn btn-primary" id="send">Save changes</a>
              </div>
          </form>
        </div>
      </div>
    </div>
  </div>
