@push('scripts')
    <script>
        $(document).ready(function(){

            $('#exp_area').picker({search : true});

            CKEDITOR.replace( 'textbody').required;
        });
    </script>
@endpush
@extends('blueprint')
@section('content')
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="text-font">Industrial Attachment</h3>
            </div>
            <div class="panel-body">
                <form class="text-font" method="post" action="{{route('attach.update',$att_info->id)}}">
                        @csrf
                    <div class="row">

                        <div class="form-group col-md-6">
                            <label class="col-md-4 text-uppercase">Employer</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="employer_name" placeholder="Employer's Name" value="{{old('employer_name',$att_info->employer_name)}}" >
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="col-md-4 text-uppercase">Employer's Email</label>
                            <div class="col-md-8">
                                <input type="email" class="form-control" name="employer_email" placeholder="Employer's Email Address" value="{{old('employer_email',$att_info->employer_email)}}">
                            </div>
                        </div>


                        <div class="form-group col-md-6">
                            <label class="col-md-4 text-uppercase">Address</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="address" value="{{old('address',$att_info->address)}}" id="address" placeholder="Address" >
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="col-md-4 text-uppercase">Department</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="department" value="{{old('department',$att_info->department)}}" placeholder="Department" >
                            </div>
                        </div>

                    </div>

                    <hr>

                    <div class="row">

                        <div class="form-group col-md-6">
                            <label class="col-md-4 text-uppercase">Start Date</label>
                            <div class="col-sm-8">
                                <input type="date" class="form-control" value="{{old('start_date',$att_info->start_date)}}" name="start_date" >
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="col-md-4 text-uppercase">End Date</label>
                            <div class="col-sm-8">
                                <input type="date" class="form-control" value="{{old('end_date',$att_info->end_date)}}" name="end_date" placeholder="Jane Doe" >
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="col-md-4 text-uppercase">Job Title</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="job_title" value="{{old('job_title',$att_info->job_title)}}" placeholder="E.g IT Techician" >
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="col-md-4 text-uppercase">Experience Area</label>
                            <div class="col-sm-8">
                                @php $area = $att_info->experience_area ?? null @endphp
                                <select name="experience_area" id="experience_area">
                                    <option value="General Administrative function" {{preg_match('/General/',$area) ? 'selected' :''}}>General Administrative function</option>
                                    <option value="I.T Function" {{preg_match('/I.T Function/',$area) ? 'selected' :''}}>I.T Function</option>
                                    <option value="Management of staff / Work" {{preg_match('/Management/',$area) ? 'selected' :''}}>Management of staff / Work</option>
                                    <option value="Other" {{preg_match('/Other/',$area) ? 'selected' :''}}>Other</option>
                                </select>
                            </div>
                        </div>

                    </div>


                    <div class="">
                        <label>Summary Of Work Experience</label>
                        <textarea class="form-control" name="work_summary" id="textbody" rows="10">{{old('work_summary',$att_info->work_summary)}}</textarea>
                    </div>
                    <br />
                    <div class="text-center text-font">
                        <button class="btn new-btn-green" id="btn-save">SUBMIT CHANGES</button>
                    </div>
                </form>
            </div>
            <div class="panel-footer text-font">
                Regent University College of Science and Technology
            </div>
        </div>
    </div>
@endsection

