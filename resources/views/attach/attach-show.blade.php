@extends('blueprint')
@section('content')
<div class="col-md-9">

  <div class="panel panel-default">
      <div class="panel-heading text-font">
        <h3 class="cinzel">Viewing Attachment Deatils</h3>
      </div>
      <div class="panel-body">
{{--          <form class="text-font attach-form" method="post" action="{{route('intern-notification')}}">--}}
          <form class="text-font attach-form" method="post">
              @csrf
              <div class="row">

                  <div class="form-group col-md-6">
                      <label class="col-md-4 text-uppercase">Employer</label>
                      <div class="col-md-8">
                          <input type="text" class="form-control" name="employer_name" placeholder="Employer's Name" value="{{old('employer_name',$att_info->employer_name)}}" >
                          <input type="hidden" class="form-control" name="verify_token" value="{{$att_info->verifyToken}}" >
                      </div>
                  </div>

                  <div class="form-group col-md-6">
                      <label class="col-md-4 text-uppercase">Employer's Email</label>
                      <div class="col-md-8">
                          <input type="email" class="form-control" name="employer_email" placeholder="Employer's Email Address" value="{{old('employer_email',$att_info->employer_email)}}">
                      </div>
                  </div>


                  <div class="form-group col-md-6">
                      <label class="col-md-4 text-uppercase">Address</label>
                      <div class="col-md-8">
                          <input type="text" class="form-control" name="address" value="{{old('address',$att_info->address)}}" id="address" placeholder="Address" >
                      </div>
                  </div>

                  <div class="form-group col-md-6">
                      <label class="col-md-4 text-uppercase">Department</label>
                      <div class="col-md-8">
                          <input type="text" class="form-control" name="department" value="{{old('department',$att_info->department)}}" placeholder="Department" >
                      </div>
                  </div>

              </div>

              <hr>

              <div class="row">

                  <div class="form-group col-md-6">
                      <label class="col-md-4 text-uppercase">Start Date</label>
                      <div class="col-sm-8">
                          <input type="date" class="form-control" value="{{old('start_date',$att_info->start_date)}}" name="start_date" >
                      </div>
                  </div>

                  <div class="form-group col-md-6">
                      <label class="col-md-4 text-uppercase">End Date</label>
                      <div class="col-sm-8">
                          <input type="date" class="form-control" value="{{old('end_date',$att_info->end_date)}}" name="end_date" placeholder="Jane Doe" >
                      </div>
                  </div>

                  <div class="form-group col-md-6">
                      <label class="col-md-4 text-uppercase">Job Title</label>
                      <div class="col-sm-8">
                          <input type="text" class="form-control" name="job_title" value="{{old('job_title',$att_info->job_title)}}" placeholder="E.g IT Techician" >
                      </div>
                  </div>

                  <div class="form-group col-md-6">
                      <label class="col-md-4 text-uppercase">Experience Area</label>
                      <div class="col-sm-8">
                          @php $area = $att_info->experience_area @endphp
                          <select name="experience_area" id="experience_area">
                              <option value="General Administrative function" {{preg_match('/General/',$area) ? 'selected' :''}}>General Administrative function</option>
                              <option value="I.T Function" {{preg_match('/I.T Function/',$area) ? 'selected' :''}}>I.T Function</option>
                              <option value="Management of staff / Work" {{preg_match('/Management/',$area) ? 'selected' :''}}>Management of staff / Work</option>
                              <option value="Other" {{preg_match('/Other/',$area) ? 'selected' :''}}>Other</option>
                          </select>
                      </div>
                  </div>

              </div>


              <div class="">
                  <label>Summary Of Work Experience</label>
                  <textarea class="form-control" name="work_summary" id="textbody" rows="10">{{old('work_summary',$att_info->work_summary)}}</textarea>
              </div>
              <br />
              @if($att_info->status=='pending')
                  <div class="text-center text-font" id="ajax2">
                      <a class="btn btn-primary" title="Edit Information" href="{{route('attach.edit',$att_info->id)}}">Edit Info  <i class="fa fa-pencil"></i></a>
                      <button  title="Sends Endorsement to supervisor during internship" class="btn ft-color">Request Endorsement <i class="fa fa-telegram"></i></button>
                  </div>
              @endif
          </form>

                @if($att_info->status == 'pending')

                  <blockquote class="text-font">
                    <strong>ENDORSEMENT PENDING</strong>
                  </blockquote>

                  @else

                    <blockquote class="text-font text-justify">
                          <h5>EMLOYER'S ENDORSEMENT</h5>
                          <p class="text-justify">

                          I certify that <strong><em>{{Auth::user()->surname}} {{Auth::user()->othernames}}</em></strong> was employed by
                          <strong><em>{{$att_info->org_name}}</em></strong> between <strong><em>{{$att_info->st_date}}</em></strong> and
                          <strong><em>{{$att_info->en_date}}</em></strong>
                          and has satisfactorily completed the activities summarised above.
                          </p>
                          <h5>COMMENTS ON ATTITUDE TO WORK, ABILITY TO COMMUNICATE, WILLINGNESS TO ACT OWN
                            INITIATIVE AND TO EXERCISE JUDGEMENT</h5>
                            <hr />
                          <p>
                            {{$att_info->comments}}
                          </p>
                    </blockquote>

              <div class="col-md-6">
                          <p class="text-font">
                            Status: {{$att_info->status}},<br />
                            <strong>{{$att_info->emp_name}}.  </strong><br />
                            Date: {{$att_info->date_endorsed}}

                          </p>
                        </div>
              <div class="col-md-6">
                          <p class="text-font pull-right">
                            <strong>Position held: {{$att_info->pos_held}}</strong><br />
                            <strong>Professional status: {{$att_info->emp_prof_status}}</strong><br />
                            <strong>Organization: {{$att_info->org_name}}</strong>

                          </p>
                        </div>

              @endif
          </div>
          <div class="panel-footer text-font">
            Regent University College Of Science and Technology
          </div>
  </div>
</div>
@endsection

@push('scripts')
<script>
$(document).ready(function(){
  CKEDITOR.replace('textbody');
  $('.attach-form button').on('click',function(e){
    e.preventDefault();
    let myform = $(this).closest('form'),
       btn = $(this),
        route = '{{route('intern-notification')}}';
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type:'post',
        url:route,
        data:myform.serialize(),
        beforeSend:function()
        {
          $(btn).html('<i class="fa fa-spinner fa-fw fa-pulse" aria-hidden="true"></i>..Sending')
        },
        success:function(data)
        {
          $(btn).html('<i class="fa fa-thumbs-up" aria-hidden="true"></i>..SENT').addClass('new-btn-green');
        },
        error:function(error) {
            alert(error.responseJSON.message);
            $(btn).html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>..RETRY').removeClass('ft-color').addClass('btn-danger');
        }

      });

  })
});
</script>
@endpush
