<script>
$('#graph').hide();
  $(document).ready(function(){
    CKEDITOR.replace( 'textbody').required;
    document.getElementById('textbody').innerHTML = "{{$pinfo->prof_skill_body}}";
      $('#profSkillsForm').submit(function(event){
          event.preventDefault();
          for (instance in CKEDITOR.instances)
          {
            CKEDITOR.instances[instance].updateElement();
            CKEDITOR.instances[instance].setData('');
          }
          var getRegNum = $('#reg_num').val();
          var getTitle  = $('#title').val();
          var getBody  = $('#textbody').val();

          console.log(getRegNum,getTitle,getBody);
          $.ajax({

            type:'post',
            url:'/prof-skills/{{$pinfo->id}}',
            datatype:'json',
            data:{
                getRegNum: getRegNum,
                getTitle : getTitle,
                getBody  : getBody,
                '_token':$('input[name=_token]').val()
        },
          success:function(data){
                console.log(data);
                $.ajax({
                  type:'get',
                  url:'prof-skills/{{$pinfo->id}}',
                  success:function(data){
                    $("#replace").html(data);
                    console.log('Page Loading worked');
                  },
                  error:function(){
                    console.log('Page Loading Failed!!')
                  }
                });
          },
          error:function(){
            console.log('post error');
          }
          });


      });
  });
</script>
<div class="col-md-9">

      <div class="panel panel-default">
          <div class="panel-heading">
              <h3 class="text-font">Professional Skills</h3>
          </div>
        <div class="panel-body">

    @if(count($pinfo)==1)
           <form  class=" text-font" id="profSkillsForm">
              {{csrf_field()}}
            <div class="row">
              <div class="col-md-6">
                  <label class="col-sm-4">ID NUMBER</label>
                  <div class="col-sm-5">
                    <input type="number" class="form-control" value="{{Auth::user()->reg_num}}" id="reg_num" readonly/>
                  </div>
              </div>

              <div class="col-md-6  ">
                <label class="col-sm-2">TITLE</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" value="{{$pinfo->title}}" id="title" required/>
                    </div>
              </div>
          </div>
              <br />

              <div style="margin-left:2% !important;">
                <label style="mar"><h5>Details of task and Skill Demonstrated</h5></label>
                <textarea rows="10" name="prof_skill_body" id="textbody" class="form-control"></textarea>
              </div>
              <br />
              <div class="pull-right">
                  <button class="btn btn-primary text-edit" id="btn-save" type="submit">Save Changes <i class="fa fa-pencil" aria-hidden="true"></i></button>
              </div>
              </form>
            @else
              <h4 class="text-font">There Is No Record For You</h4>
            @endif
    </div>
    <div class="panel-footer">
      Regent University College Of Science and Technology
    </div>
  </div>
</div>
