<!Doctype>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>STUDENT | INTERN END. </title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Alegreya|Cinzel+Decorative|Cormorant|Courgette|Fontdiner+Swanky|Great+Vibes|Lobster|Lora|Mogra|Mr+Dafoe|Mr+De+Haviland|Noto+Serif|Oregano|Poiret+One|Pompiere|Raleway|Slabo+27px|Spectral|Vibur|Yellowtail" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link rel="shortcut icon" href="{{{ asset('logoo.png') }}}">
    <link rel="stylesheet" href="{{asset('materialize/css/materialize.min.css')}}" />
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link rel="shortcut icon" href="{{{ asset('admin.png') }}}">
    <script src="{{ asset('js/jquery-3.2.1.js') }}"></script>
    <script src="{{asset('materialize/js/materialize.min.js')}}"></script>

  </head>
    <body class="">
      <nav class="ft-color cinzel">
         <div class="nav-wrapper">
           <a href="#" class="brand-logo right"><img src="{{asset('logoo.png')}}" style="height:65px"/></a>
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
           <ul id="nav-mobile" class="left hide-on-med-and-down">
             <li><a href="#">REGENT UNIVERSITY COLLEGE OF SCIENCE AND TECHNOLOGY</a></li>
             <li><a href="#">STUDENT PRACTICAL EXPERIENCE RECORD <i class="fa fa-chevron-right" aria-hidden="true"></i> INDUSTRIAL ATTACHMENT</a></li>
             <li><a href="#"></a></li>
           </ul>

        
      <ul class="side-nav" id="mobile-demo">
        <!-- <li><a href="#">REGENT UNIVERSITY COLLEGE OF SCIENCE AND TECHNOLOGY</a></li> -->
        <li><a href="#">STUDENT PRACTICAL EXPERIENCE RECORD <i class="fa fa-chevron-right" aria-hidden="true"></i> INDUSTRIAL ATTACHMENT</a></li>
        <li><a href="#"></a></li>
      </ul>
         </div>
      </nav>

        @if(!empty($fetch))
        <div class="container">
          <div class="row">
            <div class="col s12 center-align" style="margin-top:5%">
                <img src="{{asset('404.jpg')}}" class="col-md-offset-2" style="height:300px; width:450px"/>
              <div class="">
                <h3 class="text-style text-center text-danger">THIS URL HAS EXPIRED!!</h3>
              </div>
          </div>
        </div>
      </div>
        @else
          <div class="container text-font">
          <div class="row">
            <div class="col s4 offset-s4">
              <div class="card">
                 <div class="card-image">
                    @if($fetch->img == 'null')
                    <img src="{{asset('css/img/Kristy.png')}}" alt="nothing here" class="materialboxed" width="650"/>
                      @else
                        <img src="{{asset('storage/greenFolder/'.$fetch->img)}}" alt="nothing here" class="materialboxed" width="650"/>
                    @endif
                   <span class="card-title">{{$fetch->surname}} {{$fetch->othernames}}</span>
                 </div>
           </div>
            </div>
            <div class="col s12">
              <ul class="collapsible" data-collapsible="accordion">
             <li>
               <div class="collapsible-header active">INDUSTRIAL ATTACHMENT SUMMARY</div>
               <div class="collapsible-body"><span>
                 <blockquote style="text-align:justify">
                   {!!html_entity_decode($fetch->work_sum)!!}
                 </blockquote>
               </span></div>
             </li>
           </ul>

              <form class="endorsement">
                {{csrf_field()}}
                <input type="hidden" value="{{$fetch->verifyToken}}" id="token"/>
                <input type="hidden" value="{{$fetch->rec}}" id="rec"/>
                <div class="card-panel">
                  <div class="row">
                    <div class="input-field col s6">
                      <input placeholder="Full name of student" id="first_name" type="text" value="{{$fetch->surname}} {{$fetch->othernames}}" class="validate" readonly>
                      <label for="first_name">Name Of Student</label>
                    </div>
                    <div class="input-field col s6">
                      <input id="org" type="text" class="validate" placeholder="Mame of Organization" required>
                      <label for="org">Your Organization</label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="input-field col s6">
                      <input id="date1" type="text" class="validate" value="{{$fetch->st_date}}">
                      <label for="date1">Start date</label>
                    </div>

                    <div class="input-field col s6">
                      <input placeholder="Placeholder" id="date2" type="text" class="validate" value="{{$fetch->en_date}}">
                      <label for="date2">End date</label>
                    </div>
                  </div>

              <div class="row">
                <div class="input-field col s6">
                      <select id="endorse" required>
                         <option value="" disabled selected>Choose your option</option>
                        <option value="Endorsed">Yes</option>
                      </select>
                      <label>Endorse</label>
                    </div>
                <div class="input-field col s6">
                  <input id="position" type="text" class="validate" required>
                  <label for="position">Position Held</label>
                </div>
              </div>

              <div class="row">
                <div class="input-field col s6">
                  <input placeholder="Placeholder" id="emp_name" type="text" class="validate" value="{{$fetch->emp_name}}">
                  <label for="emp_name">Your Name</label>
                </div>
                <div class="input-field col s6">
                  <input id="prof_s" type="text" class="validate" required>
                  <label for="prof_s">Professional Status</label>
                </div>
              </div>

              <div class="row">
                <div class="input-field col s6">
                  <input type="text" class="datepicker" id="current_date" required>
                  <label for="current_date">Date</label>
                </div>
                <div class="input-field col s12">
                  <textarea id="comments" class="materialize-textarea" required></textarea>
                  <label for="comments">Please add comments on attitude to work, ability, to communicate, willingness, to act own
                  initiative and to exercise judgement</label>
                </div>
              </div>

              <div class="row">
                <div class="col s3 offset-s5">
                  <button class="btn waves-effect waves-light offset-s4" type="submit" name="action">Proceed
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
                </div>
              </form>

            </div>
          </div>
      </div>
        @endif

              <div id="modal1" class="modal modal-fixed-footer">
                <div class="modal-content">
                  <h4 class="text-font">Are you sure of this ???</h4>
                  <h5 id="summary" class="text-font"></h5>

                  <div class="row">
                    <div class="col s6 text-font" id="left">
                      sssdfnsdfn,dsm
                    </div>
                    <div class="col s6 text-font" id="right">
                      gfklhjfhjgk
                    </div>
                  </div>
                  <textarea id="comm" class="materialize-textarea">

                  </textarea>

                </div>
                <div class="modal-footer">
                  <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Cancel</a>
                  <button class="modal-action modal-close waves-effect waves-green btn-flat" id="response">Submit</a>
                </div>
              </div>

      <script>
      $(document).ready(function() {
           $(".button-collapse").sideNav();
         $('.modal').modal();
          $('select').material_select();
          $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15, // Creates a dropdown of 15 years to control year,
            today: 'Today',
            clear: 'Clear',
            close: 'Ok',
            closeOnSelect: false // Close upon selecting a date,
          })
          });
          $('.endorsement').on('submit',function(e){
            e.preventDefault();

            var btn = $(this);
            var token = $('#token').val();
            var rec = $('#rec').val();
            var student_name = $('#first_name').val();
            var org  = $('#org').val();
            var date1 = $('#date1').val();
            var date2 = $('#date2').val();
            var endorse = $('#endorse').val();
            var position = $('#position').val();
            var emp_name = $('#emp_name').val();
            var prof_s = $('#prof_s').val();
            var current_date = $('#current_date').val();
            var comments = $('#comments').val();

            $('#summary').html('I certify that ' +student_name+ ' was employed by '
             +org+' between ' +date1+ ' and ' + date2 + ' and has satisfactorily completed the activities '
             +' summarised above.');

            $('#left').html('<p>Status: '+endorse+ '<br />'
            +' Name: ' +emp_name+ ' <br /> Organization: '+org+'</p>');

            $('#right').html('<p>Position held: '+position+ '<br />'
            +'Professional status: ' +prof_s+ ' <br /> Date: '+current_date+'</p>')
            $('#comm').html(comments);

            $('#modal1').modal('open');
            $('#response').one('click',function(e){
              e.preventDefault();
              $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'post',
                url:'/intern/endorse/'+token+'/'+rec,
                data:{
                 student_name: student_name,
                 org      :  org,
                 date1    : date1,
                 date2    : date2,
                 endorse  : endorse,
                 position : position,
                 emp_name : emp_name,
                 prof_s   : prof_s,
                 current_date : current_date,
                 comments : comments,
                  '_token':$('input[name=_token]').val()
                },
                success:function(data)
                {
                  window.location.replace('/');
                }
              })
            })
          })
      </script>

    </body>
</html>
