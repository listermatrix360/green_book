@push('scripts')
<script>
  $(document).ready(function(){
    function side_nav(){
      $.ajax({
        type:'get',
        url:'/side_nav',
        success:function(data){
          $('#sidenav').html(data)
        }
      })
    }
    CKEDITOR.replace( 'bodytext');
      $('#profSkillsForm').submit(function(event){
          event.preventDefault();
          for (instance in CKEDITOR.instances)
          {
            CKEDITOR.instances[instance].updateElement();
            CKEDITOR.instances[instance].setData('');
          }
          var getRegNum = $('#reg_num').val();
          var getTitle  = $('#title').val();
          var getBody  = $('#textbody').val();

          console.log(getRegNum,getTitle,getBody);
          $.ajax({
                  type:'post',
                  url:'{{route('alpha-add')}}',
                  datatype:'json',
                  data: {
                  getRegNum: getRegNum,
                  getTitle : getTitle,
                  getBody  : getBody,
                  '_token':$('input[name=_token]').val()
                },
                  success:function(data){
                        console.log(data);
                        side_nav();
                  $('#profSkillsForm').trigger("reset");
                  },
                  error:function(){
                    alert('all fields are required');
                  }
                });

      });
  });
    </script>
@endpush

@extends('blueprint')
@section('content')
              <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="text-font">Professional Skill</h3>
                    </div>
                    <div class="panel-body">
                      <form class="text-font" id="profSkillsForm">
                          {{csrf_field()}}
                        <div class="row">
                          <div class="col-md-6">
                              <label class="col-sm-4">ID NUMBER</label>
                              <div class="col-sm-5">
                                <input type="number" class="form-control" value="{{Auth::user()->reg_num}}" id="reg_num" readonly/>
                              </div>
                          </div>

                          <div class="col-md-6  ">
                            <label class="col-sm-2">TITLE</label>
                                <div class="col-md-6">
                                  <input type="text" class="form-control" value="{{old('title')}}" id="title" required/>
                                </div>
                          </div>
                      </div>
                      <br />
                      <br />
                      <div style="margin-left:2% !important;">
                        <label style="mar">Details of task and Skill Demonstrated</label>
                        <textarea rows="10" id="textbody" name="bodytext" class="form-control" required></textarea>
                      </div>
                      <br />
                      <div class="text-center">
                          <button class="btn btn-primary" type="submit" id="btn-save">Save Info</button>
                      </div>
                      </form>
                    </div>
                    <div class="panel-footer">
                        Regent University College of Science and Technology
                    </div>
                </div>
              </div>
    @endsection
