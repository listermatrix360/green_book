<script>
      $(document).ready(function(){
        CKEDITOR.replace( 'textbody');
        document.getElementById('textbody').innerHTML = "{{$pinfo->prof_skill_body}}";

        $('#ajax2 a').on('click',function(event){
          event.preventDefault();
          var $this = $(this);
          target = $this.data('target');
          $.ajax({
            method:'GET',
            url:target,
            beforeSend: function(){
                $("#replace").html('Working On...');
            },
            success:function(data){
              $("#replace").html(data);
              $("select").selectpicker('refresh');
              $('#bread').empty();
              if (target == '{{'/prof-skills/'.$pinfo->id.'/edit'}}'){
                  $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#">Industrial Attachment</li><li><a href="#"><b>Professional Skill</b></li></a>');
              }
            }
          });
        });
      });
</script>



<div class="col-md-9">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="text-font">Showing Professioal Skills</h4>
    </div>
  <div class="panel-body">
        @if(!empty($pinfo))
          <form  method="post" action="{{route('alpha-add')}}" class="text-font">
            {{csrf_field()}}
            <div class="row">
              <div class="col-md-6">
                <label class="col-sm-4">ID NUMBER</label>
                <div class="col-sm-5">
                  <input type="number" class="form-control" value="{{Auth::user()->reg_num}}" name="reg_num" readonly/>
                </div>
              </div>

              <div class="col-md-6  ">
              <label class="col-sm-2">TITLE</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control" value="{{$pinfo->title}}" name="title" required/>
                  </div>
                </div>
              </div>
            <br />

            <div style="margin-left:2% !important;">
              <label style="mar"><h5>Details of task and Skill Demonstrated</h5></label>
              <textarea rows="10" name="prof_skill_body" id="textbody" class="form-control" readonly></textarea>
            </div>
            <br />
            <div class="text-center" id="ajax2">
                <a class="btn btn-primary" data-target="{{'/prof-skills/'.$pinfo->id.'/edit'}}">Edit Info <i class="fa fa-pencil" aria-hidden="true"></i></a>
            </div>
          </form>
          @else
          <h4 class="text-font">There Is No Record For You</h4>
        @endif
  </div>
  <div class="panel-footer text-font">
    Regent University College Of Science and Technology
  </div>
  </div>
</div>
