<!DOCTYPE html>
<html lang="en">
@include('tools.head')
<body class="lato">
<div class="wrapper-page">
    <div class="card overflow-hidden account-card mx-3">
        <div class="bg-success  p-4 text-white text-center position-relative" style="background-color: #007E33 !important;">
            <h4 class="font-20 m-b-5">Welcome Back !</h4>
            <p class="text-white-50 mb-4">Sign in to continue to The Green Book.</p>
            <a href="" class="logo logo-admin"><img src="{{asset('logoo.png')}}" height="60" alt="logo"></a>
        </div>
        <div class="account-card-content">
            <form class="form-horizontal m-t-30" method="post">
                @csrf
                <div class="form-group">
                    <label for="username">Email / Student ID</label>
                    <input type="text" class="form-control" name="login" id="username" placeholder="Enter username">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Enter password">
                </div>
                <div class="form-group row m-t-20">
                    <div class="col-sm-12 text-center">
                        <button class="btn text-white w-md waves-effect waves-light " type="submit" style="background-color: #007E33 !important;">
                           <a i class="fa fa-key"></a> Log In
                        </button>
                    </div>
                </div>
                <div class="form-group m-t-10 mb-0 row">
                    <div class="col-12 m-t-20"><a href="#"><i class="mdi mdi-lock"></i> Forgot your password?</a></div>
                </div>
            </form>
        </div>
    </div>
    <div class="m-t-40 text-center">
        <p>© 2019 Veltrix. Crafted with <i class="mdi mdi-heart text-danger"></i> by Ice-T Ofosu Boateng</p>
    </div>
</div>
@include('tools.script')
</body>
</html>
