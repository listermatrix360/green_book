<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				Change Password
			</h4>
		</div>
		<div class="panel-body">
			<div class="col-md-8 well">
			<form class="text-font form form-horizontal" role="form">
				{{ csrf_field() }}

					<div class="form-group">
						<label class="col-md-4 control-label">NEW PASSWORD</label>
							<div class="col-md-5">
								<input type="password" id="type1" class="form-control" required/>
							</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label">RE-ENTER PASSWORD</label>
							<div class="col-md-5">
								<input type="password" id="type2" class="form-control" required/>
							</div>
					</div>


						
					<div class="col-md-offset-4">
						<button class="btn btn-primary">SUBMIT</button>
					</div>
			</form>
			</div>
			<div class="col-md-4">
				<div class="alert alert-danger alert-dismissible" role="alert" id="error">
	  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	 				 <strong>Warning!</strong> Wrong Password Combination Retry.
				</div>

				<div class="alert alert-success alert-dismissible" role="alert" id="success">
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  <strong>Congrats!!</strong> Password Successfully Changed.
				</div>
			</div>
		</div>
	</div>
	</div>

<script>
	$(document).ready(function(e) {

		$('#success').hide();
		$('#error').hide();

		$('.form').submit(function(e){
			e.preventDefault();

			var first  = $('#type1').val();
			var second = $('#type2').val();
			
			if (first==second) {

			$.ajax({
				type:'post',
				url	:'pass',
				data:{
					first:first,
					'_token':$('input[name=_token]').val()
				},
				success:function(data){
					$('#error').hide();
					$('#success').show();
					window.location.replace('/book');
				},
				error:function(data){
					console.log(data);
				}
			})
			}
			else{
				$('#error').show();
			}
		})
	})
</script>