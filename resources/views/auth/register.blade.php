
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading ft-color">
                  <h4 class="text-font panel-title white">Student Registeration   <small class="pull-right"><i class="fa fa-tree" aria-hidden="true"></i>
                    <i class="fa fa-university fa-2x" aria-hidden="true"></i> <i class="fa fa-gavel" aria-hidden="true"></i> <i class="fa fa-hospital-o" aria-hidden="true"></i></small></h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal text-font register" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">TITLE</label>

                            <div class="col-md-6">
                                <input id="getTitle" type="text" class="form-control" name="title" value="{{ old('title') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">ID NO.</label>

                            <div class="col-md-6">
                                <input  type="number" class="form-control" name="reg_num" value="{{ old('reg_num') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('reg_num') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">SURNAME</label>

                            <div class="col-md-6">
                                <input id="surname" type="text" class="form-control" name="surname" required autofocus>

                                @if ($errors->has('getFirst'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">LAST NAME</label>

                            <div class="col-md-6">
                                <input id="getLast" type="text" class="form-control" name="othernames" value="{{ old('othernames') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('othernames') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">LEVEL</label>

                            <div class="col-md-6">
                              <select class="selectpicker" name="level" data-live-search="true" required>
                                <option data-tokens="" ></option>
                                <option data-tokens="" >100</option>
                                <option data-tokens="" >200</option>
                                <option data-tokens="" >300</option>
                                <option data-tokens="" >400</option>
                              </select>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('level') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">E-MAIL ADDRESS</label>

                            <div class="col-md-6">
                                <input id="getEmail" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="text-center">
                                <button type="submit" class="btn ft-color">
                                    <i class="fa fa-send-o" aria-hidden="true"></i> Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="panel-footer sub-color-bg text-font">
                    Regent University College of Science and Technology
                </div>
            </div>
        </div>

        <div class="col-md-6 col-md-offset-">
          <div class="panel panel-default">
            <div class="panel-heading ft-color">
              <h5 class="text-font panel-title">Student Bulk Registeration</h5>
            </div>
            <div class="panel-body">
              <form class="form-inline" id="bulky-file" method="post" enctype="multipart/form-data" action="{{route('excel')}}">
                {{csrf_field()}}
                  <div class="form-group">
                    <input type="file" class="form-control" name="bulky-file" required>
                  </div>

                  <button type="submit" class="btn ft-color"><i class="fa fa-upload" aria-hidden="true"></i></button>
                  </form>
            </div>
          </div>
        </div>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker('refresh');
    function reload(){
          $.ajax({
            type:'GET',
            url:'{{route('register')}}',
            success:function(data)
            {
              $('#pager').html(data);
            }
            })
    }
    $('.register').on('submit',function(e){
      e.preventDefault();
    var myform = $(this).closest('form');

        $.ajax({
        type:'POST',
        url:'{{route('register')}}',
        data:myform.serialize(),
        success:function(data)
        {
            reload();
            alertify.set('notifier','position', 'top-center');
            alertify.set('notifier','delay',10);
            alertify.success('DATA SAVED SUCCESSFULLY !!!!');
        },error:function()
        {
          alertify.set('notifier','position', 'top-center');
          alertify.set('notifier','delay',10);
          alertify.error('CHECK FOR FIELD NAMES UNIQUENESS OF EMAIL OR ID !!!!');
        }
      })

  })
})
</script>
