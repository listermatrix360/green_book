@extends('blueprint')
@section('content')
<div class="col-md-9">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="cinzel">Autobiography</h4>
    </div>

    <div class="panel-body">
      @if(!empty($get))
          <textarea class="form-control textbody" name="textbody" id="body" rows="50" readonly>
              {!!html_entity_decode($get->auto_body)!!}
          </textarea>
          <br />
          <div class="text-center text-font">
            <a class="btn btn-primary" data-toggle="modal" data-target="#bioModal">edit</a>
          </div>
      @else
      <form class="form">
        {{csrf_field()}}
        <div>
          <textarea class="form-control textbody" name="textbody" id="body" rows="50" required></textarea>
        </div>
          <br />
          <div class="text-center">
              <button class="btn btn-primary" id="save">Save </button>
          </div>
      </form>
      @endif
    </div>
    <div class="panel-footer">
      Regent University College of Science and Technology
    </div>
  </div>
</div>

<div class="modal fade" id="bioModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title cinzel" id="myModalLabel">Edit Autobiography Info.</h4>
            </div>
            <div class="modal-body">
                <form class="text-font form-horizontal mod-form">
                    {{ csrf_field() }}
                    @if(!empty($get))
                        <textarea class="form-control" name="editbody" id="body" rows="50">
              {!!html_entity_decode($get->auto_body)!!}
          </textarea>
                    @endif
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary" type="submit" id="mod-button">Save changes</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script>
$(document).ready(function(){
  CKEDITOR.replace('textbody');
  CKEDITOR.replace('editbody');
  CKEDITOR.config.height ='500px';

    $('.form').on('submit',function(e){
      e.preventDefault();
      for (instance in CKEDITOR.instances)
         {
           CKEDITOR.instances[instance].updateElement();
           CKEDITOR.instances[instance].setData('');
         };

      var myform = $(this).closest("form");

      $.ajax({
        headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
        type:'post',
        url:'{{route('auto-post')}}',

        data:myform.serialize(),
        // beforeSend:function(){}
        success:function(data){
          alert(data);
        },
        error:function(){
          alert('there is an error');
        }
      })

    })
    $('.mod-form').on('submit',function(e){
      e.preventDefault();
      for (instance in CKEDITOR.instances)
         {
           CKEDITOR.instances[instance].updateElement();
           CKEDITOR.instances[instance].setData('');
         };

      var myform = $(this).closest("form");
          $.ajax({
          headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
          type:'post',
          url:'{{route('auto-update')}}',
          data:myform.serialize(),
          // beforeSend:function(){}
          success:function(data){
            setTimeout(function(){
      					$('#bioModal').modal('hide');

      					     $.ajax({
                       headers: {
                                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                             },
                       type:'get',
                       url:'{{route('biography')}}',
                       success:function(data){
                       $("#replace").html(data);
                       }
                     })

      				},300)
          },
          error:function(){
            alert('there is an error editing');
          }
        })

      })
    })
// });
</script>
    @endpush
