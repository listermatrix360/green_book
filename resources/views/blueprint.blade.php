<!Doctype html>
<html lang="{{ config('app.locale')}}">
    <head>
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="expires" content="-1" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Green Book | User</title>
        <link rel="stylesheet" href="{{asset('css/paper.min.css')}}" />
        <link rel="stylesheet" href="{{asset('css/style.css')}}" />
        <link rel="stylesheet" href="{{asset('css/picker.min.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/css/icons.css')}}" />
        <link rel="shortcut icon" href="{{{ asset('logoo.png') }}}">
        <link rel="stylesheet" href="{{asset('css/bootstrap-select.min.css')}}" />
        <link rel="stylesheet" href="{{asset('css/alertify.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/alertify-bootstrap.min.css')}}" />
        <link href="https://fonts.googleapis.com/css?family=Alegreya|Allura|Calligraffitti|Cinzel|Cinzel+Decorative|Cormorant+Garamond|Cormorant+Upright|Courgette|Fontdiner+Swanky|Great+Vibes|Italianno|Josefin+Slab|Kurale|Lora|Merienda|Mogra|Mr+De+Haviland|Noto+Serif|Playball|Raleway|Sacramento|Slabo+13px|Slabo+27px|Spectral|Tangerine|Yellowtail" rel="stylesheet">
        <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" />
        <!-- <script src="{{ asset('js/jquery-3.2.1.js') }}"></script> -->
        <!-- <script src="{{ asset('js/bootstrap.min.js') }}"></script> -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/picker.min.js')}}"></script>
        <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
        <script src="{{asset('js/alertify.min.js')}}"></script>
        <script src="{{asset('js/ckeditor1/ckeditor.js')}}"></script>
        <script>
          $(document).ready(function(){

            $(function () {
              $('[data-toggle="popover"]').popover()
            });

          })
          </script>
    </head>

@include('layout.user-header')
<body>
    <main id="ess">
        <div class="container" style="width: 90%">
            <div class="row">
                <div class="col-md-9">
                    <h2 style="color:white" class="cinzel text-uppercase"><i class="fa fa-university" aria-hidden="true"></i> The Green book
                        <small style="color:#e0e0e0;" class="text-uppercase text-font"> Practical Experience Record</small></h2>
                </div>

                <div class="col-md-3" style="background-color: ; margin-top: 10px">
                    <h3 style="color:white" class="cinzel text-uppercase">
                        <small style="color:#e0e0e0;" class="text-capitalize text-font"><em>Level : {{\Illuminate\Support\Facades\Auth::user()->level}}</em></small>
                    </h3>
                </div>
            </div>
        </div>
    </main>

    <section id="breadcrumb">
        <div class="container" style="width: 90%">
            <ol class="breadcrumb" id="bread">
                <li class="active">Dashboard</li>
                <li class="cinzel"><b>REGENT UNIVERSITY COLLEGE OF SCIENCE AND TECHNOLOGY</b></li>
            </ol>
        </div>
    </section>



    <div class="container" style="width: 90%">
        @if (count($errors) > 0)
            <div class="alert alert-warning">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                </div>
            @endif


      @if($defer->defer_status == '0')
        <div class="row">
            <div class="col-md-3" id="sidenav">
                <div class="list-group text-font">
                    <a href="#" class="list-group-item panel-color"><i class="fa fa-cogs fa-fw fa-2x" aria-hidden="true"></i> Menu</a>
                    @if(!$personal_details)
                    <div id="ajax">
                        <a  href="{{route('personal.create')}}" class="list-group-item">PERSONAL DETAILS<i class="fa fa-address-book pull-right text-success" aria-hidden="true"></i></a>
                    </div>
                    @else
                        <div id="ajax">
                            <a  href="{{route('personal.show')}}" role="button" class="list-group-item">PERSONAL DETAILS<i class="fa fa-check pull-right" aria-hidden="true" style="padding-left:30% !important;"></i></a>
                        </div>
                    @endif
                    <div id="ajax">
                        <a  class="list-group-item" role="button" href="{{route('mentor.create')}}">MENTOR<span class="badge" style="background-color:#263;">{{$ctm}}</span></a>
                    </div>
                    <a class="list-group-item" role="button" data-toggle="collapse" href="#diaryCollapse" aria-expanded="false" aria-controls="diaryCollapse">DIARY <i class="fa fa-user"></i> <span class="badge" style="background-color:#263;">{{$diary->count()}}</span></a>
                    <div class="collapse" id="diaryCollapse">
                        <div id="ajax">
                            @foreach ($diary as $diaries)
                                <a class="list-group-item text-style" style="color:#CC0000;"  href="{{route('diary.edit',[$diaries->id])}}"><i class="fa fa-book"></i> <em>{{$diaries->title}}</em><i class="badge">{{$diaries->date}}</i></a>
                            @endforeach
                        </div>
                    </div>
                    <a class="list-group-item" role="button" data-toggle="collapse" href="#attachCollapse" aria-expanded="false" aria-controls="attachCollapse">INDUSTRIAL ATTACHMENT <span class="badge" style="background-color:#263;">{{$attachment->count()}}</span></a>
                    <div class="collapse" id="attachCollapse">
                        <div id="ajax">
                            @if($attachment->isEmpty())
                                <a class="list-group-item"> You havent Provided Any data Yet</a>
                            @else
                                @foreach ($attachment as $attach)
                                    <a class="list-group-item text-style" href="{{ route('attach.show',[$attach->id])}}" >{!! html_entity_decode('<b>'.$attach->start_date .'</b> to <b>' .$attach->end_date.'</b>') !!}</a>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <a class="list-group-item" href="#profCollapse"  role="button" data-toggle="collapse" aria-expanded="false" aria-controls="profCollapse">PROFESSIONAL SKILL <span class="badge" style="background-color:#263;">{{$pskill_count}}</span></a>
                    <div class="collapse" id="profCollapse">
                        @if($pskills->isEmpty())
                            <a class="list-group-item" href="#">You havent Provided Any data Yet</a>
                        @else
                            <div id="ajax">
                                @foreach($pskills as $skills)
                                    <a class="list-group-item text-style" style="color:#CC0000;" href="{{'/prof-skills/'.$skills->id}}" ><i class="fa fa-eye" ></i> <em>{{$skills->title}}</em></a>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <a class="list-group-item" href="#eduCollapse" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="profCollapse">EDUCATION AND EXAMINATION <span class="badge" style="background-color:#263;">{{$times}}</span></a>
                    <div class="collapse" id="eduCollapse">
                        @if($education->isEmpty())
                            <a class="list-group-item" href="#">You havent Provided Any data Yet</a>
                        @else
                            <div id="ajax">
                                @foreach($education as $educ)
                                    <a class="list-group-item text-style" style="color:#CC0000;" href="{{'/edu-show/'.$educ->id}}" ><i class="fa fa-eye" ></i> <em>{{$educ->sch_name}}</em></a>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <a class="list-group-item" href="#courseCollapse" role="button" data-toggle="collapse" aria-expanded="false">OTHER COURSES <span class="badge" style="background-color:#263;">{{$count_courses}}</span></a>
                    <div class="collapse" id="courseCollapse">
                        @if($course->isEmpty())
                            <a class="list-group-item" href="#">You havent Provided Any data Yet</a>
                        @else
                            <div id="ajax">
                                @foreach($course as $courses)
                                    <a class="list-group-item text-style" style="color:#CC0000;" href="{{'/seeCourses/'.$courses->id}}" ><i class="fa fa-eye" ></i> <em>{{$courses->cours_name}}</em></a>
                                @endforeach
                            </div>
                        @endif
                    </div>

                    <a class="list-group-item" href="#health" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="profCollapse">HEALTH AND FITNESS <span class="badge" style="background-color:#263;">3</span></a>
                    <div class="collapse" id="health">
                        <div id="ajax">
                            <a class="list-group-item text-style" style="color:#CC0000;" href="{{route('community-index')}}" ><em>Community Service </em>  <span class="badge text-font" style="background-color:#263;">{{$community_count}}</span></a>
                            <a class="list-group-item text-style" style="color:#CC0000;" href="{{route('spirit-index')}}" ><em>Spiritual Fitness</em>   <span class="badge text-font" style="background-color:#263;">{{$chapel_count}}</span></a>
                            <a class="list-group-item text-style" style="color:#CC0000;" href="{{route('physical-index')}}" ><em>Physical Fitness </em>   <span class="badge text-font" style="background-color:#263;">{{$physical_count}}</span></a>
                        </div>

                    </div>
                    <!-- <a class="list-group-item" href="#profCollapse" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="profCollapse">PHYSICAL EXERCISES <span class="badge">0</span></a>
                    <a class="list-group-item" href="#profCollapse" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="profCollapse">SPIRITUAL EXERCISES <span class="badge">0</span></a> -->

                </div>
            </div>

                @yield('content')



            </div>
        </div>
      @else
      <div>
        <h1 class="cinzel text-center  text-muted"><b>You can't perform any activity unless you are reinstated !!</b></h1>
        <h3 class="text-center text-muted">
          <i class="fa fa-exclamation-triangle fa-3x" aria-hidden="true"></i>
        </h3>
      </div>
      <br /><br /><br />
      @endif



    <div class="modal fade" id="photo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title cinzel" id="myModalLabel">CHANGE PROFILE PHOTO</h4>
                </div>
                <div class="modal-body">
                  <form class="form-inline" id="body-pic" action="{{route('upload')}}" enctype="multipart/form-data" method="post">
                      {{csrf_field()}}
                      <div class="form-group">
                        <label for="exampleInputName2">Name</label>
                        <input type="hidden" value="{{@$pass->password}}" id="pass"/>
                        <input type="file" class="form-control" name="picture" id="file_upload" required>
                      </div>
                      <button type="submit" class="btn btn-primary">Upload</button>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
  </div>



  @include('book.page-footer')
@stack('scripts')
</body>
</html>
