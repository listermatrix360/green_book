@extends('blueprint')
@section('content')
<div class="col-md-9">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title text-font">Personal Details</h4>
    </div>
    <div class="panel-body">
      @if(!empty($personal_details))
        <div class="col-md-12">
          <h4 class="text-font">Personal Details  submitted.
            You can use the Quick View navigation or use the button below to view details</h4>

            <div id="ajax2" class="text-center">
              <a class="btn btn-primary text-font" data-target="{{'/book/' .$personal_details->reg_num}}">View <i class="fa fa-eye"></i></a>
            </div>
        </div>
        @if($personal_details->img =='null')
        <div class="col-md-12">
          <h4 class="text-font">Add Profile Picture</h4>
          <form class="form-inline" action="{{route('upload')}}" enctype="multipart/form-data" method="post">
              {{csrf_field()}}
              <div class="form-group">
                <label for="exampleInputName2">Name</label>
                <input type="file" class="form-control" name="picture">
              </div>
              <button type="submit" class="btn btn-primary">Upload</button>
          </form>
        </div>
        @endif

        <br />
        @else
        <form  class="form-horizontal" role="form" method="post" >
            {{csrf_field()}}
            <div class="form-group">
                <label for="first_name" class="col-md-4 control-label"><i class="fa fa-user-circle-o" aria-hidden="true"></i> First Name</label>
                <div class="col-md-6">
                    <input type="text" id="first_name" class="form-control" value="{{Auth::user()->surname}}" readonly>
                </div>
            </div>

            <div class="form-group">
                <label for="first_name" class="col-md-4 control-label"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Last Name</label>
                <div class="col-md-6">
                    <input type="text" id="last_name" class="form-control" value="{{Auth::user()->othernames}}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="last_name" class="col-md-4 control-label"><i class="fa fa-id-card prefix" aria-hidden="true"></i> ID Number</label>
                <div class="col-md-6">
                    <input name="reg_num" type="number" class="form-control" value="{{ Auth::user()->reg_num }}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="last_name" class="col-md-4 control-label"><i class="fa fa-id-card prefix" aria-hidden="true"></i> Gender</label>
                <div class="col-md-6">
                  <select class="selectpicker" name="gender"required>
                    <option  value="M">Male</option>
                    <option  value="F">Female</option>
                  </select>
                </div>
            </div>

            <div class="form-group">
                <label for="last_name" class="col-md-4 control-label"><i class="fa fa-id-card prefix" aria-hidden="true"></i> Date of Birt</label>
                <div class="col-md-6">
                    <input name="dob" type="date" class="form-control" value="" required>
                </div>
            </div>

            <div class="form-group text-edit">
               <label for="last_name" class="col-md-4 control-label"><i class="fa fa-graduation-cap prefix" aria-hidden="true"></i> Nationality</label>
               <div class="col-md-6">
                   <select class="selectpicker" name="nationality_id" data-live-search="true" required>
                     <option ></option>
                     <option selected>Ghananian</option>
                     <option >Togolaise</option>
                     <option >French</option>
                   </select>
               </div>
            </div>

            <div class="form-group">
                <label for="last_name" class="col-md-4 control-label"><i class="fa fa-id-card prefix" aria-hidden="true"></i> Session</label>
                <div class="col-md-6">
                  <select class="selectpicker" name="session_id" data-live-search="true" required>
                    <option  ></option>
                    <option   selected>Morning</option>
                    <option  >Evening</option>
                    <option  >Weekend</option>
                  </select>
                </div>
            </div>

            <div class="form-group">
               <label for="last_name" class="col-md-4 control-label"><i class="fa fa-graduation-cap prefix" aria-hidden="true"></i> School</label>
               <div class="col-md-6">
                   <select class="form-control selectpicker" name="school_id" data-live-search="true"  id="school" required>
                       <option></option>
                       @foreach ($school as $schools)
                           <option value="{{$schools->id}}">{{$schools->name}}</option>
                       @endforeach
                   </select>
               </div>
            </div>

            <div class="form-group">
               <label for="last_name" class="col-md-4 control-label"><i class="fa fa-graduation-cap prefix" aria-hidden="true"></i> Department</label>
               <div class="col-md-6">
                   <select class="form-control" id="department" name="department_id" required>

                   </select>
               </div>
            </div>

            <div class="form-group">
                <label for="last_name" class="col-md-4 control-label"><i class="fa fa-book prefix" aria-hidden="true"></i> Program</label>
                <div class="col-md-6">
                    <select class="form-control" id="program" name="program_id"  required>

                    </select>

                </div>
            </div>
            <div class="form-group">
                <label for="last_name" class="col-md-4 control-label"><i class="fa fa-users prefix" aria-hidden="true"></i> Society</label>
                <div class="col-md-6">
                  <select class="form-control selectpicker" id="society" name="society_id" data-live-search="true" required>
                      <option></option>
                    @foreach ($soc as $socs)
                      <option value="{{$socs->id}}">{{$socs->soc_name}}</option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="form-group">
                <label for="last_name" class="col-md-4 control-label"><i class="fa fa-phone prefix" aria-hidden="true"></i> Mobile No. 1</label>
                <div class="col-md-6">
                    <input name="phone1" type="number" class="form-control" value="{{old('phone1')}}" required>
                </div>
            </div>
            <div class="form-group">
                <label for="last_name" class="col-md-4 control-label"><i class="fa fa-phone prefix" aria-hidden="true"></i> Mobile No. 2</label>
                <div class="col-md-6">
                    <input name="phone2" type="number" value="{{old('phone2')}}" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label for="last_name" class="col-md-4 control-label"><i class="fa fa-envelope prefix" aria-hidden="true"></i> Regent E-mail</label>
                <div class="col-md-6">
                    <input name="email1" type="email" value="{{Auth::user()->email}}" class="form-control" readonly required>
                </div>
            </div>
            <div class="form-group">
                <label for="last_name" class="col-md-4 control-label"><i class="fa fa-envelope prefix" aria-hidden="true"></i> Personal E-mail</label>
                <div class="col-md-6">
                    <input name="email2" type="email" value="{{old('email2')}}" class="form-control" required>
                </div>
            </div>
            <div class="text-center col-md-offset-2">
                <button type="submit" id="btn-save" class="btn btn-custom">Save</button>
            </div>
        </form>
        @endif
    </div>
  </div>

          </div>
@endsection


@push('scripts')
    <script>
        $(document).ready(function(){
            const department = $('#department'),
                school = $('#school'),
                program =  $('#program');

            school.on('change',function(e){
                let InputID = $(this).val();

                let route = "{{route('school.department',['id'])}}",
                    $_route = route.replace('id',InputID);

                $.ajax  ({
                    method:'GET',
                    datatype:'json',
                    url:$_route,
                    contentType:"application/json; charset=UTF-8",
                    success:function(data){
                        department.empty();
                        program.empty();
                        department.append("<option value='0'>--Select Department--</option>");
                        $.each(data,function(i){
                            $('#department').append('<option value="'+ data[i].id +'">'+ data[i].name+'</option>');
                        });
                    },
                    error: function(data){
                        console.log(data);
                    }
                });
            });
            department.on('change',function(e){
                let InputID = $(this).val(),
                    route = "{{route('department.program',['department'])}}",
                    $_route  = route.replace('department',InputID);
                $.ajax  ({
                    method:'GET',
                    datatype:'json',
                    url:$_route,
                    contentType:"application/json; charset=UTF-8",
                    success:function(data){
                        program.empty();
                        program.append("<option value='0'>--Select Program--</option>");
                        $.each(data,function(i,item){
                            program.append('<option value="'+ data[i].id +'">'+ data[i].name+'</option>');
                        });
                    },

                });
            });

        });
    </script>
@endpush
