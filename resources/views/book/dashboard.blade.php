@extends('blueprint')
    @section('content')
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading panel-color">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-book fa-4x" aria-hidden="true"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="text-edit">Practical Experience Record Status</div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    @if($profile_status)
                        @if($profile_status->prof_status=='Rejected')
                            <p class="text-danger text-style">
                                Your Profile Has been REJECTED!!!
                            </p>
                            <p class="text-style">
                                {{$profile_status->ess_comm}}
                            </p>
                        @elseif($profile_status->prof_status=='Approved')

                            <p class="cinzel">
                                <strong> Your PER Has been approved !!!! {{$profile_status->ess_comm}}.</strong>
                            </p>

                            <div class="pull-right text-font">
                                <button class="btn btn-xs white" style="background-color:#263;"><i class="fa fa-handshake-o fa-2x" aria-hidden="true"></i>
                                    Great</button>
                            </div>
                        @endif
                    @else
                        <p class="cinzel">
                            <strong>Your PER is still pending for approval</strong>
                        </p>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-dafault">
                <div class="panel-heading panel-color">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-bolt fa-4x" aria-hidden="true"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="text-edit">Upcoming Event Of RUCST</div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <p class="cinzel" title="Designed By HighPriest">
                        @if($event->isEmpty())
                            <strong>
                                Nothing to show!!
                            </strong>
                        @else
                            @foreach($event as $events)
                                <strong>
                                    <i>{{$events->date}},</i>
                                    <u>{{$events->title}}</u>
                                    {{$events->description}}
                                    <br />
                                </strong>
                            @endforeach
                        @endif
                    </p>
                    <div class="pull-right text-font">
                        <button class="btn btn-xs white" style="background-color:#263;"><i class="fa fa-exclamation" aria-hidden="true"></i> Must Be Well Noted</button>
                    </div>

                </div>

            </div>
        </div>

        <div class="col-md-9">
            <div class="panel panel-default">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="{{asset('css/img/dbook2.jpg')}}">
                            <div class="carousel-caption">
                                Hello
                            </div>
                        </div>
                        <div class="item">
                            <img src="{{asset('css/img/notebook.jpg')}}">
                            <div class="carousel-caption">
                                ...
                            </div>
                        </div>

                        <div class="item">
                            <img src="{{asset('css/img/penbook1.jpg')}}">
                            <div class="carousel-caption">
                                ...
                            </div>
                        </div>

                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        @endsection
