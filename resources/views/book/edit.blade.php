@extends('blueprint')
@section('content')
  <div class="container">
    <div class="row">
      @if(!empty($item))
      <div class="col-md-9">
        <div class="panel panel-default">
          <div class="panel-heading">
             <h4 class="cinzel"> Personal Details</h4>
          </div>
            <div class="panel-body">
                <form  class="form-horizontal text-font" method="post">
                    @csrf
                    <div class="row justify-content-center">
                     <div class="form-group">
                         <label for="first_name" class="col-md-4 control-label"><i class="fa fa-user-circle-o prefix" aria-hidden="true"></i> First Name</label>
                         <div class="col-md-6">
                             <input type="text" name="first_name" class="form-control" value="{{Auth::user()->surname}}" readonly>
                         </div>
                       </div>

                       <div class="form-group">
                           <label for="first_name" class="col-md-4 control-label"><i class="fa fa-user-circle-o prefix" aria-hidden="true"></i> Last Name</label>
                           <div class="col-md-6">
                               <input type="text" name="last_name" class="form-control" value="{{Auth::user()->othernames}}" readonly>
                           </div>
                         </div>

                     <div class="form-group">
                         <label for="last_name" class="col-md-4 control-label"><i class="fa fa-id-card prefix" aria-hidden="true"></i> Registration No.</label>
                         <div class="col-md-6">
                           <input name="reg_num" type="number" class="form-control" value="{{$item->reg_num}}" readonly>
                         </div>
                       </div>

                       <div class="form-group">
                           <label for="gender" class="col-md-4 control-label"><i class="fa fa-id-card prefix" aria-hidden="true"></i> Gender</label>
                           <div class="col-md-6">
                               @php $gender  = $item->gender ?? null  @endphp
                             <select class="selectpicker text-font" name="gender" required>
                               <option  ></option>
                               <option  value="M" {{$gender == 'M' ? 'selected' : ''}}>Male</option>
                               <option  value="F" {{$gender == 'F' ? 'selected' : ''}}>Female</option>
                             </select>
                           </div>
                       </div>

                       <div class="form-group">
                           <label for="last_name" class="col-md-4 control-label"><i class="fa fa-id-card prefix" aria-hidden="true"></i> Date of Birth</label>
                           <div class="col-md-6">
                               <input name="dob" type="date" class="form-control" value="{{$item->dob}}" required>
                           </div>
                       </div>

                       <div class="form-group">
                          <label for="last_name" class="col-md-4 control-label"><i class="fa fa-graduation-cap prefix" aria-hidden="true"></i> Nationality</label>
                          <div class="col-md-6">
                              @php $nation  = $item->nationality_id ?? null  @endphp
                              <select class="selectpicker text-font" name="nationality_id" data-live-search="true" required>
                                <option data-tokens="" ></option>

                                <option {{ $nation == 'Ghanaian' ? 'selected' : ''}}>  Ghanaian  </option>

                              </select>

                          </div>
                       </div>

                       <div class="form-group">
                           <label for="last_name" class="col-md-4 control-label"><i class="fa fa-id-card prefix" aria-hidden="true"></i> Session</label>
                           <div class="col-md-6">
                               @php $session = $item->session_id ?? null @endphp
                             <select class="selectpicker text-font" name="session" required>
                               <option data-tokens="" ></option>
                               <option data-tokens="" {{$session == 'Morning' ? 'selected' : ''}}>Morning</option>
                               <option data-tokens="" {{$session == 'Evening' ? 'selected' : ''}}>Evening</option>
                               <option data-tokens="" {{$session == 'Weekend' ? 'selected' : ''}}>Weekend</option>
                             </select>
                           </div>
                       </div>


                       <div class="form-group">
                          <label for="last_name" class="col-md-4 control-label"><i class="fa fa-graduation-cap prefix" aria-hidden="true"></i> School</label>
                          <div class="col-md-6">
                              @php $school_id  = $item->program->department->school->id ?? null  @endphp
                              <select class="selectpicker text-font" name="school_id" id="school" data-live-search="true" required>
                                <option></option>
                                  <option>--select School--</option>
                                @foreach ($school as $schools)
                                  <option value="{{$schools->id}}" {{ $school_id == $schools->id ? 'selected' : ''}}>{{$schools->name}} </option>
                                @endforeach
                              </select>
                          </div>
                       </div>

                    <div class="form-group">
                       <label for="last_name" class="col-md-4 control-label"><i class="fa fa-graduation-cap prefix" aria-hidden="true"></i> Department</label>
                       <div class="col-md-6">
                           <select class="form-control" id="department" name="department" required>
                               <option>{{$item->program->department->name}}</option>
                           </select>
                       </div>
                    </div>



                    <div class="form-group">
                        <label for="program" class="col-md-4 control-label"><i class="fa fa-book prefix" aria-hidden="true"></i> Program</label>
                        <div class="col-md-6">
                            <select class="form-control" id="program_id" name="program_id" required>
                                <option value="{{$item->program->id}}">{{$item->program->name}}</option>
                            </select>

                        </div>
                    </div>

                     <div class="form-group">
                         <label for="society" class="col-md-4 control-label"> <i class="fa fa-users prefix" aria-hidden="true"></i> Society</label>
                         <div class="col-md-6">
                           <div class="col-md-6">
                             <select class="form-control selectpicker" name="society_id" id="society_id" required>
                                 @php $id = $item->society_id ?? null @endphp
                                 <option></option>
                               @foreach ($soc as $socs)
                                 <option value="{{$socs->id}}" {{$id ==  $socs->id ? 'selected' : '' }}>{{$socs->name}}</option>
                               @endforeach
                             </select>
                           </div>
                         </div>
                    </div>

                     <div class="form-group">
                         <label for="phone1" class="col-md-4 control-label"><i class="fa fa-phone prefix" aria-hidden="true"></i> Mobile No. 1</label>
                         <div class="col-md-6">
                             <input name="phone1"  type="number" class="form-control" value="{{$item->phone1}}" >
                         </div>
                      </div>

                     <div class="form-group">
                         <label for="last_name" class="col-md-4 control-label"><i class="fa fa-phone prefix" aria-hidden="true"></i> Mobile No. 2</label>
                         <div class="col-md-6">
                           <input name="phone2" type="number" value="{{$item->phone2}}" class="form-control">
                         </div>
                       </div>
                     <div class="form-group">
                         <label for="last_name" class="col-md-4 control-label"><i class="fa fa-envelope prefix" aria-hidden="true"></i> Regent E-mail</label>
                         <div class="col-md-6">
                             <input name="email1" type="email" class="form-control" value="{{Auth::user()->email}}" readonly>
                         </div>
                       </div>
                     <div class="form-group">
                         <label for="last_name" class="col-md-4 control-label"><i class="fa fa-envelope prefix" aria-hidden="true"></i> Persoanl E-mail</label>
                         <div class="col-md-6">
                             <input name="email2" type="email" class="form-control" value="{{$item->email2}}">
                         </div>
                       </div>

                  <div class="text-center text-font col-md-offset-1" id="button">
                      <button type="submit" id="btn-save" class="btn btn-primary">Save</button>
                    </div>

               </div>
                </form>
              </div>
          <div class="panel-footer">
           Regent University College Of Science and Technology
         </div>
        </div>
        </div>
        @else
        <h4 class="text-style">You Havent addded any information yet</h4>
        @endif
      </div>
    </div>
  @endsection


@push('scripts')
<script>
  $(document).ready(function(){


    const department = $('#department'),
          school     = $('#school'),
          program    = $('#program_id');

       school.on('change',function(e){
          let InputID = $(this).val();

          let route = "{{route('school.department',['id'])}}",
              $_route = route.replace('id',InputID);

          $.ajax  ({
            method:'GET',
            datatype:'json',
            url:$_route,
            contentType:"application/json; charset=UTF-8",
            success:function(data){
                department.empty();
                program.empty();
                department.append("<option value='0'>--Select Department--</option>");
                $.each(data,function(i){
                    $('#department').append('<option value="'+ data[i].id +'">'+ data[i].name+'</option>');
                });
            },
            error: function(data){
            }
          });
      });

       department.on('change',function(e){
          let InputID = $(this).val(),
              route = "{{route('department.program',['department'])}}",
              $_route  = route.replace('department',InputID);
          $.ajax  ({
                method:'GET',
                datatype:'json',
                url:$_route,
                contentType:"application/json; charset=UTF-8",
                success:function(data){
                    console.log(data);
                    program.empty();
                    program.append("<option value='0'>--Select Program--</option>");
                    $.each(data,function(i,item){
                        console.log(item);
                        program.append('<option value="'+ data[i].id +'">'+ data[i].name+'</option>');
                    });
                },
              error:function (error) {
              }

              });
      });
    });
</script>
    @endpush
