<div class="col-md-9">
    <div class="col-md-6">
      <h4 class="cinzel"><strong>Introduction</strong></h4>
      <p class="text-font t2 text-justify">
        The main purpose of the Practical Experience Record (PER) is to implement, monitor and fulfill the requirements of the various
        courses required for graduation. It is important to remember that examination success is only one aspect of qualifying as
        a University graduate in Regent-Ghana. The University will expect that, by the time you register for graduation,
        you have also obtained the appropriate training,
        practical experience and fulfilled the requirements of our Health and Fitness program.
        This is the reason why you are required to record your practical experience and other required activities in this
        Practical Experience Record (PER) and submit the Summaries Section of it with your registration for graduation.
        The summaries section must be endorsed by your University mentor, and the Director or Educational Support Service.
        The PER enables you to build up a complete and accurate summary of the skills and expertise you acquire in
        different areas of work during the course of your training and to have these verified as you go along, rather
        than retrospectively when you may even have changed your job.
      </p>

      <p class="text-font t2 text-justify">
        You will find that the PER includes both a Diary
        Section, in which you record, at a regular intervals, details of the work have been doing, and a number of
        summary Sheets.
        You are expected to fill one of these on completion of an assignment or whenever you move on to a different
        work; they summarize for the University, the experience you have obtained in each job placement and will
        form part of your application for graduation. Details about how to complete the PER are given below.
        The University accepts that the experience which you obtain will depend on the size, nature and structure
        of the organization which employs you and that you may work in fewer or more areas that other students.
        Your program of study covers a wider range of courses than you are likely to encounter during your
        training and it will not always be possible for you to arrange for your practical experience to be
        in the same area as the courses you are currently studying.

      </p>
    </div>

    <div class="col-md-6">
      <p class="text-font t2 text-justify">
        Nevertheless, you should look on your practical experience as reinforcing the program of study
        which you are undertaking for developing professional competence. For this reason, you should try
        to obtain as wide a range of experience as possible, taking practical responsibility for work and
        participating in as many of the main aspects of your area of specialization carried within your
        organization as you can. You should also try to arrange to spend longer periods in some areas
        rather than in others so that our broad training and experience is balanced by more detailed
        knowledge of one or two aspects of your specialization. This will enable you to make a worth
        while contribution to the work of your organization and will help to ensure that, by the time
      </p>

      <p class="text-font t2 text-justify">
        ou qualify, you have the managerial and technical knowledge, and skills appropriate for someone embarking on a professional career.
        In addition to acquiring technical and practical expertise, you will be expected to show that you have
        developed personal skills which will enable you to communicate information effectively, both orally
        and in writing, and to be able to plan work, to exercise judgment and to motivate and control others,
        and to understand importance of professional attitudes integrity, independences, impartiality and objectivity.
        If you follow the experience requirements set out in the PER, maintain it carefully and make sure that
        your employer confirms the details which include in the Summary Sheets, you should have no difficulty
        in acquiring the experience and skills, both technical and personal, appropriate for graduation.
      </p>
    </div>

</div>
