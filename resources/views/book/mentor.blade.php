<html>
<head>
    <title>Mentor</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-select.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Alegreya|Cinzel+Decorative|Cormorant|Courgette|Fontdiner+Swanky|Great+Vibes|Lobster|Lora|Mogra|Mr+Dafoe|Mr+De+Haviland|Noto+Serif|Oregano|Poiret+One|Pompiere|Raleway|Slabo+27px|Spectral|Vibur|Yellowtail" rel="stylesheet">
    <script src="{{asset('js/jquery-3.2.1.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
<style>
@import url('https://fonts.googleapis.com/css?family=Alegreya|Caveat|Cinzel|Cinzel+Decorative|Cormorant+Infant|Courgette|Dancing+Script|Fontdiner+Swanky|Great+Vibes|IM+Fell+Great+Primer+SC|Josefin+Slab|Kalam|Kurale|Lobster|Lora|Marck+Script|Mogra|Mr+Dafoe|Mr+De+Haviland|Noto+Serif|Oregano|Playball|Poiret+One|Pompiere|Sacramento|Satisfy|Slabo+27px|Spectral|Vibur|Yellowtail|Zilla+Slab');
</style>
</head>
<body>
  <br />
  <br />
  <div class="container-fluid">
    <div class="row">
      <div class=" col-md-8 col-md-offset-2">
        <div class="">

          <table class="table table-stripped">
            <thead>
                  <tr style="font-family:'Yellowtail', cursive;" >
                    <th>Academic Year</th>
                    <th>Name of Mentor</th>
                    <th>Request Auth</th>
                    <th>Status</th>
                  </tr>
            </thead>

              <tbody style="font-family: 'Cinzel', serif;">
                <form action="/send" method="get">
                  {{ csrf_field() }}
                  <tr>
                          <td>First <b>Year</b></td>
                          <td><select class="selectpicker" data-live-search="true">
                          <option data-tokens=""></option>
                          <option data-tokens="Prof Ayim Aboagye">Prof Ayim Aboagye</option>
                          <option data-tokens="Mr. Attah Opoku">Mr. Attah Opoku</option>
                          <option data-tokens="Mr. Samuel Chris Quist">Mr. Samuel Chris Quist</option>
                          </select></td>

                          <input type="hidden" name="message" value="The Followig Student Has Requested For Your Auth."/>
                          <input type="hidden" name="s_mail" value="ice-t.boateng@regent.edu.gh" />
                          <input type="hidden" name="sender_id" value="{{Auth::user()->reg_num}}" />

                          <td><button class="btn btn-primary"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send request</button></td>
                    <td><input type="text" class="form-control" placeholder="Pending Request " readonly/></td>
                  </tr>
                </form>

              <form action="send" method="post">
                  {{ csrf_field() }}
                  <tr>
                      <td>Second Year</td>
                      <td><select class="selectpicker" data-live-search="true">
                            <option data-tokens=""></option>
                            <option data-tokens="ketchup mustard">Prof Ayim Aboagye</option>
                            <option data-tokens="mustard">Mr. Attah Opoku</option>
                            <option data-tokens="frosting">Mr. Samuel Chris Quist</option>
                          </select>
                        </td>

                        <input type="hidden" name="message" value="The Followig Student Has Requested For Your Auth."/>
                        <input type="hidden" name="s_mail" value="listermatrix@yahoo.com" />
                        <td><button class="btn btn-primary">Send request</button></td>
                      <td><input type="text" class="form-control" placeholder="Pendind Request " readonly/></td>
                  </tr>
                </form>

            <form action="send" method="post">
                  {{ csrf_field() }}
                  <tr>
                    <td>Third Year</td>
                    <td><select class="selectpicker" data-live-search="true">
                          <option data-tokens=""></option>
                          <option data-tokens="Prof Ayim Aboagye">Prof Ayim Aboagye</option>
                          <option data-tokens="mustard">Mr. Attah Opoku</option>
                          <option data-tokens="frosting">Mr. Samuel Chris Quist</option>
                        </select></td>
                        <td><button class="btn btn-primary">Send request</button></td>
                    <td><input type="text" class="form-control" placeholder="Pendind Request " readonly/></td>
                  </tr>
                </form>

            <form action="send" method="post">
                  {{ csrf_field() }}
                  <tr>
                    <td>Fourth Year </td>
                    <td><select class="selectpicker" data-live-search="true">
                          <option data-tokens=""></option>
                          <option data-tokens="Prof Ayim Aboagye">Prof Ayim Aboagye</option>
                          <option data-tokens="mustard">Mr. Attah Opoku</option>
                          <option data-tokens="frosting">Mr. Samuel Chris Quist</option>
                        </select></td>
                        <td><button class="btn btn-primary">Send request</button></td>
                    <td><input type="text" class="form-control" placeholder="Pendind Request " readonly/></td>
                  </tr>
           </form>
              </tbody>

          </table>

        </div>
      </div>
    </div>

  </div>



</body>
</html>
