@section('alert-error')
      @if (count($errors)>0)
      @foreach ($errors->all() as $error)
      <script type="text/javascript">
      window.onload = function(){
           alertify.set('notifier','position', 'bottom-right');
           alertify.set('notifier','delay',10);
           alertify.error('{{$error}}');
      }
      </script>
      @endforeach

      @endif
@endsection
@section('form-error')
    @if (count($errors)>0)
        <div class="col-md-3 loader">
         <div class="alert alert-danger" role="alert">
    @foreach ($errors->all() as $error)
    <br />
    <h4 style="font-family:'Spectral', serif;">*{{$error}}</h4>
    @endforeach

    @endif
    </div>
    </div>
@endsection
