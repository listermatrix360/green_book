@if (session()->has('message'))
  <p class="text-center alert alert-success">{{session()->get('message')}}</p>
@endif
