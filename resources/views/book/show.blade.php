@extends('blueprint')
@section('content')
<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="cinzel">Personal Details <small>(viewing)</small></h4>
        </div>
        <div class="panel-body">
            @if(!empty($item))
            <form method="" action="/book" class="form-horizontal">
                {{csrf_field()}}
                <div class="row justify-content-center">
                    <div class="form-group">
                        <label for="first_name" class="col-md-4 control-label"><i class="fa fa-user-circle-o prefix" aria-hidden="true"></i> Name</label>
                        <div class="col-md-6">
                            <input type="text" name="name" class="form-control" value="{{Auth::user()->surname }} {{Auth::user()->othernames}}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="last_name" class="col-md-4 control-label"><i class="fa fa-id-card prefix" aria-hidden="true"></i> Registration No.</label>
                        <div class="col-md-6">
                            <input id="last_name" name="reg_num" type="text" class="form-control" value="{{$item->reg_num}}" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="last_name" class="col-md-4 control-label"><i class="fa fa-id-card prefix" aria-hidden="true"></i> Gender</label>
                        <div class="col-md-6">
                            <input id="gender" type="text" class="form-control" value="{{$item->gender}}" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="last_name" class="col-md-4 control-label"><i class="fa fa-id-card prefix" aria-hidden="true"></i> Date of Birt</label>
                        <div class="col-md-6">
                            <input id="dob" type="date" class="form-control" value="{{$item->dob}}" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="last_name" class="col-md-4 control-label"><i class="fa fa-id-card prefix" aria-hidden="true"></i> Nationality</label>
                        <div class="col-md-6">
                            <input id="dob" type="text" class="form-control" value="{{$item->nationality_id}}" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="last_name" class="col-md-4 control-label"><i class="fa fa-id-card prefix" aria-hidden="true"></i> Session</label>
                        <div class="col-md-6">
                            <input id="dob" type="text" class="form-control" value="{{$item->session_id}}" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="last_name" class="col-md-4 control-label"><i class="fa fa-graduation-cap prefix" aria-hidden="true"></i> School</label>
                        <div class="col-md-6">
                            <input id="school" name="school" type="text" class="form-control" value="{{$item->program->department->school->name  ?? null}}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="last_name" class="col-md-4 control-label"><i class="fa fa-book prefix" aria-hidden="true"></i> Program</label>
                        <div class="col-md-6">
                            <input id="program" name="program" type="text" class="form-control" value="{{$item->program->name ?? null }}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="last_name" class="col-md-4 control-label"> <i class="fa fa-users prefix" aria-hidden="true"></i> Society</label>
                        <div class="col-md-6">
                            <input id="society" name="society" type="text" class="form-control" value="{{$item->society->name ?? null}}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="last_name" class="col-md-4 control-label"><i class="fa fa-phone prefix" aria-hidden="true"></i> Mobile No. 1</label>
                        <div class="col-md-6">
                            <input name="phone1" type="number" class="form-control" value="{{$item->phone1}}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="last_name" class="col-md-4 control-label"><i class="fa fa-phone prefix" aria-hidden="true"></i> Mobile No. 2</label>
                        <div class="col-md-6">
                            <input name="phone2" type="number" value="{{$item->phone2}}" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="last_name" class="col-md-4 control-label"><i class="fa fa-envelope prefix" aria-hidden="true"></i> Regent E-mail</label>
                        <div class="col-md-6">
                            <input name="email1" type="email" class="form-control" value="{{Auth::user()->email}}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="last_name" class="col-md-4 control-label"><i class="fa fa-envelope prefix" aria-hidden="true"></i> Persoanl E-mail</label>
                        <div class="col-md-6">
                            <input name="email2" type="email" class="form-control" value="{{$item->email2}}" readonly>
                        </div>
                    </div>
                    <div class="text-center text-font col-md-offset-2" id="ajax2">
                        <a href="{{route('personal.edit')}}" class="btn btn-primary">Edit Info</a>
                    </div>
                </div>
            </form>
            @else
            <h4 class="text-style">No Record Available with this ID</h4>
            @endif
        </div>
        <div class="panel-footer">
            Regent University College Of Science and Technology
        </div>
    </div>
</div>
@endsection
<script>
    $(document).ready(function() {
        $('#ajax2 a').on('click', function(event) {
            event.preventDefault();
            var $this = $(this);
            target = $this.data('target');
            $.ajax({
                method: 'GET',
                url: target,
                beforeSend: function() {
                    $("#replace").html('Working On...');
                },

                success: function(data) {
                    $("#replace").html(data);
                    $('#bread').empty();
                    if (target == '{{' / book / '}}') {
                        $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#">Industrial Attachment</li><li><a href="#"><b>Professional Skill</b></li></a>');
                    } else if (target == '{{'/book/'.$item->reg_num. '/edit'}}') {
                        $('.selectpicker').selectpicker('refresh');
                        $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#">Basic Info</li><li><a href="#"><b>Edit Personal Details</b></li></a>');
                    }
                }
            });
        });
    });

</script>
