




  <script>
      $('#ajax a').on('click',function(event){
        event.preventDefault();
        var $this = $(this);
        target = $this.data('target');
        alert(target);
        $.ajax({
          method:'GET',
          url:target,
          beforeSend: function(){
              $("#replace").html('Working On...');
          },
          success:function(data){
            $("#replace").html(data);
            $('#bread').empty();
            $('.selectpicker').selectpicker('refresh');

            if (target == 'http://localhost:8000/book/create') {
              $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#"><b>Basic Info</b></li></a><li class="text-style"><a href="#"><b>Personal Details</b></li></a>');
            }
            else if (target == '{{route('educ-index')}}') {
              $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#"><b>Education & Examination <small>(Outside the University)</small></b></li></a>');
            }
            else if (target == '/book') {
              $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#"><b>Basic Info</b></li></a><li><a href="#"><b>Mentor</b></li></a>');
            }
            else if(target == 'http://localhost:8000/diary/create'){
              alertify.set('notifier','position', 'bottom-left');
              alertify.set('notifier','delay',5);
              alertify.success('Welcome Back');
            }
            else if (target == '{{route('otherCourses')}}'){
              $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#"><b>Other Courses Attended <small>(Organized within University)</small></b></li></a>');
            }
            else if (target == '{{route('community-index')}}') {
              $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#"><b>Community Service <small>(Organized within University)</small></b></li></a>');
            }
            else if (target == '{{route('physical-index')}}') {
              $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#"><b>Physical Fitness <small>(Organized within University)</small></b></li></a>');
            }
            else if (target == '{{route('spirit-index')}}') {
              $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a>Health & Fitness</a></li><li class="text-style"><a href="#"><b>Spiritual Fitness</b></li></a>');
            }
            else if (target == '{{route('biography')}}') {
              $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#"><b>Autobiography</b></li></a>');

            }
            else if (target == 'http://localhost:8000/attach/create') {
              $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#"><b>Internship</b></li></a><li><a href="#"><b>Industrial Attachment</b></li></a>');
            }
            else if (target == '{{route('prof-skills')}}') {
              $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#"><b>Internship</b></li></a><li><a href="#"><b>Professional Skills</b></li></a>');
            }

            else if (target == 'http://localhost:8000/mentor/create') {
              $('select').selectpicker('refresh');
              $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#"><b>Basic Info</b></li></a><li><a href="#"><b>Mentor</b></li></a>');
            }
            else if (target == '/book/create'){
              $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#"><b>Basic Info</b></li></a><li><a href="#"><b>Personal Details</b></li></a>');

            }
            else if (target == '{{'personalDetail'}}'){
              $('select').selectpicker('refresh');
              $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#"><b>Basic Info</b></li></a><li><a href="#"><b>Personal Details</b></li></a>');
            }
          },

          error:function(x,e){
            if(x.status==0){
              alert('You are offline..Check Your Internet Connection');
            }
            else if(x.status==401){
                window.location.replace('/login');

            }
            else{
              alert('unknown Error has occured');
            }

          }
        });
      });
  </script>
