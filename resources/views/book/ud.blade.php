<!Doctype html>
<html lang="{{ config('app.locale')}}">
    <head>
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="expires" content="-1" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Green Book | User</title>
        <link rel="stylesheet" href="{{asset('css/paper.min.css')}}" />
        <link rel="stylesheet" href="{{asset('css/style.css')}}" />
        <link rel="stylesheet" href="{{asset('css/picker.min.css')}}" />
        <link rel="shortcut icon" href="{{{ asset('logoo.png') }}}">
        <link rel="stylesheet" href="{{asset('css/bootstrap-select.min.css')}}" />
        <link rel="stylesheet" href="{{asset('css/alertify.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/alertify-bootstrap.min.css')}}" />
        <link href="https://fonts.googleapis.com/css?family=Alegreya|Allura|Calligraffitti|Cinzel|Cinzel+Decorative|Cormorant+Garamond|Cormorant+Upright|Courgette|Fontdiner+Swanky|Great+Vibes|Italianno|Josefin+Slab|Kurale|Lora|Merienda|Mogra|Mr+De+Haviland|Noto+Serif|Playball|Raleway|Sacramento|Slabo+13px|Slabo+27px|Spectral|Tangerine|Yellowtail" rel="stylesheet">
        <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" />
        <!-- <script src="{{ asset('js/jquery-3.2.1.js') }}"></script> -->
        <!-- <script src="{{ asset('js/bootstrap.min.js') }}"></script> -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/picker.min.js')}}"></script>
        <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
        <script src="{{asset('js/alertify.min.js')}}"></script>
        <script src="{{asset('js/ckeditor1/ckeditor.js')}}"></script>
        <script>
          $(document).ready(function(){
            $.ajax({
              type:'get',
              url:'/side_nav',
              success:function(data){
                $('#sidenav').html(data)
              }
            });

            $(function () {
              $('[data-toggle="popover"]').popover()
            });

              var pass = $('#pass').val();

              if(pass == '$2y$10$kQIKzRy/KiSrerlBUVDmPeZ/AsR83PVz8ORxv8bEE.JB2MR6TB6fa')
              {
                alertify.set('notifier','position', 'top-center');
                alertify.set('notifier','delay',20);
                alertify.error('PLEASE CHANGE YOUR DEFAULT PASSWORD');
              }

          })
          </script>
    </head>

@include('layout.user-header')
<body>
    @yield('dash-nav')
    <main id="ess">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <h2 style="color:white" class="cinzel"><i class="fa fa-university" aria-hidden="true"></i> The Greenbook
                        <small style="color:#e0e0e0;" class="text-font"> PPractical Experience Record</small></h2>
                </div>

                <div class="col-md-3">
                    <div style="padding-top: 20px;" class="pull-right">
                        @if(!empty($personal_details))
                          @if($personal_details->img=='null')
                        <i class="fa fa-user-circle-o fa-4x" aria-hidden="true"></i>

                        @else
                        <a href="">
                            <img src="{{asset('storage/greenFolder/'.$personal_details->img)}}" class="img-rounded pull-right shorder">
                        </a>
                                 @endif
                             @else
                         Personal Details First
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </main>

    <section id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb" id="bread">
                <li class="active">Dashboard</li>
                <li class="cinzel"><b>REGENT UNIVERSITY COLLEGE OF SCIENCE AND TECHNOLOGY</b></li>
            </ol>
        </div>
    </section>

    <div class="container">
      @if($defer->defer_status == '0')
        <div class="row">
            <div class="col-md-3" id="sidenav">

            </div>
            <div id="replace">

              <div class="col-md-3">
                <div class="panel panel-default">
                  <div class="panel-heading panel-color">
                        <div class="row">
                              <div class="col-xs-3">
                                  <i class="fa fa-book fa-4x" aria-hidden="true"></i>
                              </div>
                              <div class="col-xs-9 text-right">
                                  <div class="text-edit">Practical Experience Record Status</div>
                              </div>
                          </div>
                        </div>
                        <div class="panel-body">
                          @if(!empty($profile_status))
                             @if($profile_status->prof_status=='Rejected')
                              <p class="text-danger text-style">
                                Your Profile Has been REJECTED!!!
                              </p>
                                <p class="text-style">
                                  {{$profile_status->ess_comm}}
                                </p>
                            @elseif($profile_status->prof_status=='Approved')

                              <p class="cinzel">
                                <strong> Your PER Has been approved !!!! {{$profile_status->ess_comm}}.</strong>
                              </p>

                            <div class="pull-right text-font">
                              <button class="btn btn-xs white" style="background-color:#263;"><i class="fa fa-handshake-o fa-2x" aria-hidden="true"></i>
                                Great</button>
                            </div>
                              @endif
                          @else
                          <p class="cinzel">
                            <strong>Your PER is still pending for approval</strong>
                          </p>
                          @endif
                        </div>
                    </div>
              </div>

              <div class="col-md-6">
                <div class="panel panel-dafault">
                        <div class="panel-heading panel-color">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-bolt fa-4x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="text-edit">Upcoming Event Of RUCST</div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                                <p class="cinzel" title="Designed By HighPriest">
                                  @if($event->isEmpty())
                                  <strong>
                                    Nothing to show!!
                                  </strong>
                                    @else
                                @foreach($event as $events)
                                <strong>
                                <i>{{$events->date}},</i>
                                  <u>{{$events->title}}</u>
                                  {{$events->description}}
                                  <br />
                                </strong>
                                @endforeach
                                @endif
                              </p>
                                <div class="pull-right text-font">
                                  <button class="btn btn-xs white" style="background-color:#263;"><i class="fa fa-exclamation" aria-hidden="true"></i> Must Be Well Noted</button>
                                </div>

                        </div>

                    </div>
              </div>






              <div class="col-md-9">
                <div class="panel panel-default">
                  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                          <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                          </ol>

                          <!-- Wrapper for slides -->
                          <div class="carousel-inner" role="listbox">
                            <div class="item active">
                              <img src="{{asset('css/img/dbook2.jpg')}}">
                              <div class="carousel-caption">
                                Hello
                              </div>
                            </div>
                            <div class="item">
                              <img src="{{asset('css/img/notebook.jpg')}}">
                              <div class="carousel-caption">
                                ...
                              </div>
                            </div>

                            <div class="item">
                              <img src="{{asset('css/img/penbook1.jpg')}}">
                              <div class="carousel-caption">
                                ...
                              </div>
                            </div>

                          </div>

                          <!-- Controls -->
                          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
                  </div>
                </div>
              </div>

            </div>
        </div>
      @endif
    </div>



    <div class="modal fade" id="photo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">3
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title cinzel" id="myModalLabel">CHANGE PROFILE PHOTO</h4>
                </div>
                <div class="modal-body">
                  <form class="form-inline" id="body-pic" action="{{route('upload')}}" enctype="multipart/form-data" method="post">
                      {{csrf_field()}}
                      <div class="form-group">
                        <label for="exampleInputName2">Name</label>
                        <input type="hidden" value="{{$pass->password}}" id="pass"/>
                        <input type="file" class="form-control" name="picture" id="file_upload" required>
                      </div>
                      <button type="submit" class="btn btn-primary">Upload</button>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
  </div>



  @include('book.page-footer')
</body>
</html>
