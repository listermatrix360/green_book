<!doctype html>
<html lang="{{ config('app.locale') }}" >
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Welcome To The PER</title>

        <!-- Fonts -->
          <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
          <!-- <link href="{{asset('css/paper.css')}}" rel="stylesheet" type="text/css"/> -->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <link rel="stylesheet" href="{{asset('materialize/css/materialize.min.css')}}" />
          <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css"/>
          <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet"/>
          <link href="https://fonts.googleapis.com/css?family=Alegreya|Allura|Calligraffitti|Cinzel|Cinzel+Decorative|Cormorant+Garamond|Cormorant+Upright|Courgette|Fontdiner+Swanky|Great+Vibes|Italianno|Josefin+Slab|Kurale|Lora|Merienda|Mogra|Mr+De+Haviland|Noto+Serif|Playball|Raleway|Sacramento|Slabo+13px|Slabo+27px|Spectral|Tangerine|Yellowtail" rel="stylesheet">
          <link href="{{asset('css/welcome.css')}}" rel="stylesheet" type="text/css" />
          <script src="{{ asset('js/jquery-3.2.1.js') }}"></script>
          <script src="{{asset('materialize/js/materialize.min.js')}}"></script>
    </head>
    <body class="white bd-color">
        <div class="wrapper">

          <div class="navbar-fixed" >
              <nav class="transparent">
                <div class="nav-wrapper">
                  <a href="#!" class="brand-logo cinzel"><img src="{{asset('logoo.png')}}" style="height:65px"/></a>
                  <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                  <ul class="right hide-on-med-and-down cinzel">
                    <li class="active"><a href="{{route('book.index')}}">Student</a></li>
                    <li><a href="/admin">E.S.S</a></li>
                  </ul>
                  <ul class="side-nav cinzel" id="mobile-demo">
                    <li class="active"><a href="/book">Student</a></li>
                    <li><a href="/admin">E.S.S</a></li>
                  </ul>
                </div>
              </nav>
        </div>

          <div class="container" style="margin-top:10%;">
              <div class="row">
                <div class="col s12">
                  <h3 class="cinzel center-align">The Practical Experience Record,</h3>
                </div>
                    <div class="col s6 offset-s3">
                      <h3 class="cinzel center-align">Green Book</h3>
                    </div>

                  <div class="col s6 offset-s3">
                    <nav class="transparent">
                      <div class="nav-wrapper">
                        <form>
                          <div class="input-field">
                            <input id="search_field" type="search" required>
                            <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                            <i class="material-icons">close</i>
                          </div>
                        </form>
                        <div class="progress" id="animate">
                            <div class="indeterminate"></div>
                        </div>
                      </div>
                    </nav>
                  </div>


                  <div class="col s6 offset-s3" id="search-results" style="margin-top:5%">

                  </div>

                </div>
            </div>


      <script>
          $(document).ready(function(){
              $('#animate').hide();
              $(".button-collapse").sideNav();
              $('#search_field').on('keyup',function(e){
                  e.preventDefault();
                    const value = $('#search_field').val();
                      if(value !==''){
                        $('#search-results').show();
                        $.ajax({
                          headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                          type:'get',
                          url:'search/'+ value,
                          beforeSend:function(){
                            $('#animate').show();
                          },
                          success:function(data){
                            $('#search-results').html(data);
                              $('#animate').hide();
                          },
                          error:function(){
                            console.log('it failed')
                          }
                        })
                      }
                      else if (value ==='') {
                        $('#search-results').hide();
                      }
              })
          });
        </script>
      </div>
    </body>
</html>
