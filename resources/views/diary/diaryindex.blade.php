
<!Doctype html>
    <html>
      <head>
          <title></title>
          <link rel="stylesheet" href="{{asset('css/bootstrap4.min.css')}}">
          <link rel="stylesheet" href="{{asset('css/mdb.min.css')}}">
          <link rel="stylesheet" href="{{asset('CSS/font-awesome.min.css')}}">
          <link rel="stylesheet" href="{{asset('css/style.css')}}">
          <link href="https://fonts.googleapis.com/css?family=Alegreya|Cinzel+Decorative|Cormorant|Courgette|Fontdiner+Swanky|Great+Vibes|Lobster|Lora|Mogra|Mr+Dafoe|Mr+De+Haviland|Noto+Serif|Oregano|Poiret+One|Pompiere|Raleway|Slabo+27px|Spectral|Vibur|Yellowtail" rel="stylesheet">
          <script src="{{asset('js/jquery-3.2.1.js')}}"></script>
           <script src="{{asset('js/tether.js')}}"></script>
           <script src="{{asset('js/bootstrap4.min.js')}}"></script>
           <script src="{{asset('js/mdb.js')}}"></script>
     <style>
      @import url('https://fonts.googleapis.com/css?family=Alegreya|Caveat|Cinzel|Cinzel+Decorative|Cormorant+Infant|Courgette|Dancing+Script|Fontdiner+Swanky|Great+Vibes|IM+Fell+Great+Primer+SC|Josefin+Slab|Kalam|Kurale|Lobster|Lora|Marck+Script|Mogra|Mr+Dafoe|Mr+De+Haviland|Noto+Serif|Oregano|Playball|Poiret+One|Pompiere|Sacramento|Satisfy|Slabo+27px|Spectral|Vibur|Yellowtail|Zilla+Slab');
      </style>
    </head>

          <body>
              <div class="jumbotron" id="diary">

                        <h1 class="text-center animated fadeIn" style="padding-top:8%; font-family: 'Yellowtail', cursive;">Welcome To Your Diary  <i class="fa fa-book" aria-hidden="true"></i></h1>
                        </>
                        <br />

                        <div class="div-center">

                        <p class="text-justify text-font t1">This portion records all the GSGR
                        activities as well as any memorable events in the University. It records details of relevant training and workshops organised
                        inside and outside The University.Example: Student Internships, Seminars, Programmes and Workshops Relevant to the program of
                        study.</p>

                      </div>

                            <div class="text-center" style="padding-bottom:8%; padding-top: 5%">
                                  <a class="btn btn-amber" href="/diary/create"><i class="fa fa-dashboard" aria-hidden="true"></i> Proceed....</a>
                            </div>
                      <div class="text-center">
                        <a href="#order" class="non-style"><i class="fa fa-angle-double-down fa-5x animated bounce infinite" aria-hidden="true"></i></a>
                      </div>


              </div>
              <div class="container-fluid " id="order" >
                  <div class="row">
                    <div class="col-md-3 wow fadeInUp" data-wow-delay="0.10s">
                      <div class="">
                        <div class="card-block blocker">
                          <h2><a href="/attach/create" class="non-fa"><i class="fa fa-newspaper-o fa-5x" aria-hidden="true"></i></h2></a>
                          <h4>Internship</h4>
                        </div>
                      </div>
                   </div>

                   <div class="col-md-3 wow fadeInUp" data-wow-delay="0.20s">
                     <div class="">
                       <div class="card-block blocker">
                         <h2><i class="fa fa-newspaper-o fa-5x fa-spin" aria-hidden="true"></i></h2>
                         <h4>Internship</h4>
                       </div>
                     </div>
                  </div>

                  <div class="col-md-3 wow fadeInUp" data-wow-delay="0.30s">
                    <div class="">
                      <div class="card-block blocker">
                        <h2><i class="fa fa-clone fa-5x" aria-hidden="true"></i></h2>
                        <h4>Internship</h4>
                      </div>
                    </div>
                 </div>
                  </div>


              </div>
          </body>

        <script>
        new WOW().init();
        </script>

    </html>
