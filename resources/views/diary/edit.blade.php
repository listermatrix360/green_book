<script>
$('#graph').hide();
  $(document).ready(function(){
    CKEDITOR.replace( 'textbody').required;

  });
  </script>
@extends('blueprint')
@section('content')
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="text-font">Diary <small>(Details Of Training Received / Work Undertaken In Each Department)</small></h4>
            </div>
            <div class="panel-body text-font">
                <form method="post" action="{{route('diary.update',[$user_diary->id])}}">
                    @csrf

                    <div class="row">

                        <div class="form-group col-md-6">
                            <label class="col-md-4 ">DATE</label>
                            <div class="col-sm-8">
                                <input type="date" name="date" value="{{$user_diary->date}}"  class="form-control" id="date"  placeholder="date" autofocus/>
                            </div>
                        </div>


                        <div class="form-group col-md-6">
                            <label class="col-md-4" >TITLE</label>
                            <div class="col-md-8">
                                <input type="text" name="title" value="{{$user_diary->title}}" class="form-control" id="title" placeholder="Must Be Precise and Short"/>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <label class="col-md-6">BODY (<small> Details of training received / Work undertaken in each department </small>)</label>
                            <div class="col-md-12">
                                <textarea  class="form-control" rows="20" name="body" id="textbody">{{$user_diary->body}}</textarea>
                            </div>
                        </div>
                    </div>


                    <div class="text-center">
                        <button class="btn new-btn-green" id="btn-save">Save Record</button>
                    </div>
                </form>
            </div>
            <div class="panel-footer text-font">
                Regent University College Of Science and Technology
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            function side_nav(){
                $.ajax({
                    type:'get',
                    url:'/side_nav',
                    success:function(data){
                        $('#sidenav').html(data)
                    }
                })
            }
            CKEDITOR.replace( 'textbody').required;
        });
    </script>
@endpush

