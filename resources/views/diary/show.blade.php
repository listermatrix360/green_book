<script>
$(document).ready(function(){
  CKEDITOR.replace( 'textbody');

  $('#ajax2 a').on('click',function(event){
    event.preventDefault();
    var $this = $(this);
    target = $this.data('target');
    $.ajax({
      method:'GET',
      url:target,
      beforeSend: function(){
          $("#replace").html('Working On...');
      },

      success:function(data){
        $("#replace").html(data);
        $("select").selectpicker('refresh');
        $('#bread').empty();
        if (target == '{{'/book/'}}'){
            $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#">Industrial Attachment</li><li><a href="#"><b>Professional Skill</b></li></a>');
        }
      }
    });
  });
});
</script>

      <div class="col-md-9">
        <div class="panel panel-default">
          <div class="panel-heading">
              <h4 class="text-font">Diary <small>(Details Of Training Received / Work Undertaken In Each Department)</small></h4>
          </div>
          <div class="panel-body">
            @if(count($dinfo)===1)
            <form method="" action="">
                  {{csrf_field()}}
                  <div class="form-group row" style="float:left;">
                    <label class="col-sm-2 ">DATE</label>
                        <div class="col-sm-8">
                            <input type="date" value="{{$dinfo->date}}"  class="form-control"  placeholder="date" autofocus readonly/>
                        </div>
                  </div>

                    <div class="form-group row" style="float:right;">
                      <label class="col-sm-4  col-md-offset-2">ID NUMBER</label>
                      <div class="col-sm-6">
                          <input type="number" name="reg_num" class="form-control" value="{{$dinfo->reg_num}}" readonly />
                      </div>
                  </div>
                        <br /><br />

                      <div class="panel-heading panel-danger">
                        <div class="row">
                          <label class="col-md-1" >TITLE</label>
                          <div class="col-sm-11">
                              <input type="text" value="{{$dinfo->title}}" class="form-control" placeholder="Enter title here" readonly/>
                          </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <label>BODY (<small> Details of training received / Work undertaken in each department </small>)</label>
                        <textarea  class="form-control" value="" rows="5" name="body" id="textbody" readonly>{{$dinfo->body}}</textarea>
                  </div>
                  <div class="text-center" id="ajax2">
                    <a class="btn btn-primary" data-target="{{'/diary/'.$dinfo->id.'/edit'}}"><i class="fa fa-edit"></i> Make Changes</a>
                  </div>

            </form>

          </div>
          <div class="panel-footer text-font">
            Regent University College Of Science and Technology
          </div>
        </div>
      </div>

      @else
      <h4>No Data Available</h4>
      @endif
