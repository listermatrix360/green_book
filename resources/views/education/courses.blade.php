@extends('blueprint')
@section('content')
    <div class="col-md-9">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="cinzel"><b>Other Courses Attended <small>(Organized within the University)</small></b></h4>
          </div>
          <div class="panel-body">

          <form id="CoursesForm" class="form-horizontal col-md-offset-2">
            {{csrf_field()}}

            <div class="form-group">
              <label class="col-sm-3 control-label"><i class="fa fa-id-card"></i> ID NUMBER</label>
                <div class="col-sm-5">
                  <input type="number" class="form-control" id="reg_num" value="{{Auth::user()->reg_num}}" readonly/>
                </div>
              </div>

        <div class="form-group">
          <label class="col-sm-3 control-label"><i class="fa fa-clock-o"></i> Start Date</label>
            <div class="col-sm-5">
              <input type="date" class="form-control" id="date1" placeholder="date" autofocus required/>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label"><i class="fa fa-clock-o"></i> End Date</label>
              <div class="col-sm-5">
                <input type="date" class="form-control" id="date2" placeholder="date"  required/>
              </div>
          </div>

            <div class="form-group">
              <label class="col-sm-3 control-label"><i class="fa fa-graduation-cap"></i> Name of Course</label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" id="course" placeholder="date"  required/>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label"><i class="fa fa-comments"></i> Description of Course</label>
                  <div class="col-sm-5">
                    <textarea type="date" class="form-control" id="description" placeholder="date"  required>

                    </textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-3 control-label"><i class="fa fa-comments"></i> Remarks</label>
                    <div class="col-sm-5">
                      <textarea type="date" class="form-control" id="remarks" placeholder="date"  required>

                      </textarea>
                    </div>
                  </div>

                  <div class="col-md-offset-5 text-font">
                      <button class="btn btn-primary" id="btn-submit">Save !!</button>
                  </div>
                </form>
              </div>
              <div class="panel-footer">
                  Regent University College Of Science of Technology
              </div>
        </div>
      </div>
    @endsection

@push('scripts')
<script>
  $(document).ready(function(){
      $('#CoursesForm').submit('click',function(event){
        event.preventDefault();
        var getRegNum = $('#reg_num').val();
        var getDate1  = $('#date1').val();
        var getDate2  = $('#date2').val();
        var getCourse = $('#course').val();
        var getDes    = $('#description').val();
        var getRemarks= $('#remarks').val();

        $.post('PostCourses',
        {
          'getRegNum'  :  getRegNum,
          'getDate1'   :  getDate1,
          'getDate2'   :  getDate2,
          'getCourse'  :  getCourse,
          'getDes'     :  getDes,
          'getRemarks' : getRemarks,
          '_token':$('input[name=_token]').val()
        },
        function(data){
          console.log(data);
          alert('data saved');
        });
        $('#CoursesForm').trigger('reset');

      });
  });

</script>
    @endpush
