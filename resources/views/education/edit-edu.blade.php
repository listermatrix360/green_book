
@extends('blueprint')
@section('content')

  <div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="cinzel"><b>Details of Education & Examination <small>(Outside the University)</small></b></h4>
        </div>
        <div class="panel-body">

        <form id="EducationForm">
            {{csrf_field()}}
            <input type="hidden" id="reg_num" value="{{Auth::user()->reg_num}}"/>

            <div class="row">
              <div class="">
                <label class="col-sm-1 control-label">Start Date</label>
                <div class="col-sm-3">
                  <input type="date" name="" class="form-control" id="st_date" value="{{$edit->st_dt}}"/>
                </div>
              </div>

              <div class="">
                <label class="col-sm-1 control-label">End Date</label>
                <div class="col-sm-3">
                  <input type="date" name="" class="form-control" id="en_date"  value="{{$edit->en_dt}}"/>
                </div>
              </div>

            <div class="">
              <label class="col-sm-1 control-label">Name of School</label>
              <div class="col-sm-3">
                <input type="text" name="" class="form-control" id="school"  value="{{$edit->sch_name}}"/>
              </div>
            </div>

            </div>
<br />
          <div class="row">
              <div class="">
                <label class="col-sm-1 control-label">Study Method</label>
                <div class="col-sm-3">
                  <input type="text" name="" class="form-control" id="study_metd"  value="{{$edit->study_mtd}}"/>
                </div>
              </div>

              <div class="">
                <label class="col-sm-1 control-label">Exams taken</label>
                <div class="col-sm-3">
                  <input type="text" name="" class="form-control" id="exam_taken" value="{{$edit->ex_taken}}"/>
                </div>
              </div>

              <div class="">
                <label class="col-sm-1 control-label">Level </label>
                <div class="col-sm-3">
                  <input type="text" name="" class="form-control" id="level" value="{{$edit->level}}"/>
                </div>
              </div>
          </div>
      <br />
            <div class="row">
                <div class="">
                  <label class="col-sm-1 control-label">Paper </label>
                  <div class="col-sm-3">
                    <input type="text" name="" class="form-control" id="Paper" value="{{$edit->paper}}"/>
                  </div>
                </div>

                <div class="">
                  <label class="col-sm-1 control-label">Result </label>
                  <div class="col-sm-3">
                    <input type="text" name="" class="form-control" id="Results" value="{{$edit->results}}"/>
                  </div>
                </div>

                <div class="">
                  <label class="col-sm-1 control-label">Remarks </label>
                  <div class="col-sm-3">
                    <input type="text" name="" class="form-control" id="Remarks" value="{{$edit->remarks}}"/>
                  </div>
                </div>
           </div>


           <br />
          <div class="text-center">
            <button class="btn btn-primary text-info" id="btn-save">Save Info</button>
          </div>
        </form>
      </div>
      <div class="panel-footer">
        Regent University College Of Science and Technology
      </div>
      </div>
  </div>
@endsection


@push('scripts')
  <script>
    $(document).ready(function(){
        $('#EducationForm').submit(function(event){
            event.preventDefault();

            var getRegNum  = $('#reg_num').val();
            var getDate1  = $('#st_date').val();
            var getDate2  = $('#en_date').val();
            var getSchool = $('#school'). val();
            var getStudy  = $('#study_metd').val();
            var getExam   = $('#exam_taken').val();
            var getLevel  = $('#level').val();
            var getPaper  = $('#Paper').val();
            var getResult = $('#Results').val();
            var getRemark = $('#Remarks').val();
            console.log(getDate1,getDate2,getSchool,getStudy,getExam,getLevel,getPaper,getPaper,getResult,getRemark);
            $.ajax({
                type:'post',
                url:'edu-update/{{$edit->id}}',
                datatype:'json',
                data:{
                getRegNum: getRegNum,
                getDate1 : getDate1,
                getDate2 : getDate2,
                getSchool: getSchool,
                getStudy : getStudy,
                getExam  : getExam,
                getLevel : getLevel,
                getPaper : getPaper,
                getResult: getResult,
                getRemark: getRemark,
                '_token':$('input[name=_token]').val()
              },
                  success:function(data){
                    $.ajax({
                      type:'get',
                      url:'edu-show/{{$edit->id}}',
                      success:function(data){
                        $("#replace").html(data);
                        console.log('personal_details Loaded');
                      },
                      error:function(){
                        console.log('Page Loading Failed!!')
                      }
                    });
                }
            });
        });
    });
  </script>
@endpush
