

@extends('blueprint')
@section('content')
  <div class="col-md-9">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="text-font">Details of Education & Examincatin <small>(Outside the University)</small></h3>
      </div>
      <div class="panel-body">
        <br />
        <form id="EducationForm">
            {{csrf_field()}}
            <input type="hidden" id="reg_num" value="{{Auth::user()->reg_num}}"/>

            <div class="row">
              <div class="">
                <label class="col-sm-1 control-label">Start Date</label>
                <div class="col-sm-3">
                  <input type="date" name="" class="form-control" id="st_date" placeholder="Start Date" required autofocus/>
                </div>
              </div>

              <div class="">
                <label class="col-sm-1 control-label">End Date</label>
                <div class="col-sm-3">
                  <input type="date" name="" class="form-control" id="en_date"  placeholder="End Date" required/>
                </div>
              </div>

            <div class="">
              <label class="col-sm-1 control-label">Name of School</label>
              <div class="col-sm-3">
                <input type="text" name="" class="form-control" id="school"  placeholder="Name of School" required/>
              </div>
            </div>

            </div>
            <br />
          <div class="row">
              <div class="">
                <label class="col-sm-1 control-label">Study Method</label>
                <div class="col-sm-3">
                  <input type="text" name="" class="form-control" id="study_metd"  placeholder="Study Method" required/>
                </div>
              </div>

              <div class="">
                <label class="col-sm-1 control-label">Exams taken</label>
                <div class="col-sm-3">
                  <input type="text" name="" class="form-control" id="exam_taken" placeholder="Examinations Taken" required/>
                </div>
              </div>

              <div class="">
                <label class="col-sm-1 control-label">Level </label>
                <div class="col-sm-3">
                  <input type="text" name="" class="form-control" id="level" placeholder="Level" required/>
                </div>
              </div>
          </div>
          <br />
            <div class="row">
                <div class="">
                  <label class="col-sm-1 control-label">Paper </label>
                  <div class="col-sm-3">
                    <input type="text" name="" class="form-control" id="Paper" placeholder="Paper" required/>
                  </div>
                </div>

                <div class="">
                  <label class="col-sm-1 control-label">Result </label>
                  <div class="col-sm-3">
                    <input type="text" name="" class="form-control" id="Results" placeholder="Results" required/>
                  </div>
                </div>

                <div class="">
                  <label class="col-sm-1 control-label">Remarks </label>
                  <div class="col-sm-3">
                    <input type="text" name="" class="form-control" id="Remarks" placeholder="Remarks" required/>
                  </div>
                </div>
           </div>


           <br />
          <div class="text-center">
            <button class="btn btn-primary text-info" id="btn-save">Save Info</button>
          </div>
        </form>
      </div>
      <div class="panel-footer">Regent University College of Science and Technology</div>
    </div>

  </div>
@endsection
  @push('scripts')
  <script>
    $(document).ready(function(){
      function side_nav(){
        $.ajax({
          type:'get',
          url:'/side_nav',
          success:function(data){
            $('#sidenav').html(data)
          }
        })
      }
        $('#EducationForm').submit(function(event){
            event.preventDefault();

            var getRegNum  = $('#reg_num').val();
            var getDate1  = $('#st_date').val();
            var getDate2  = $('#en_date').val();
            var getSchool = $('#school'). val();
            var getStudy  = $('#study_metd').val();
            var getExam   = $('#exam_taken').val();
            var getLevel  = $('#level').val();
            var getPaper  = $('#Paper').val();
            var getResult = $('#Results').val();
            var getRemark = $('#Remarks').val();
            console.log(getDate1,getDate2,getSchool,getStudy,getExam,getLevel,getPaper,getPaper,getResult,getRemark);
            $.post('edu-post',
            {
            'getRegNum': getRegNum,
            'getDate1' : getDate1,
            'getDate2' : getDate2,
            'getSchool': getSchool,
            'getStudy' : getStudy,
            'getExam'  : getExam,
            'getLevel' : getLevel,
            'getPaper' : getPaper,
            'getResult': getResult,
            'getRemark': getRemark,
            '_token':$('input[name=_token]').val()
          },
            function(data){
                  alert(data);
                  side_nav();
            });
            $('#EducationForm').trigger("reset");
        });
    });
  </script>
  @endpush
