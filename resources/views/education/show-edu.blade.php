@extends('blueprint')
@section('content')
  <div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="cinzel"><b>Details of Education & Examination <small>(Outside the University)</small></b></h4>
        </div>
        <div class="panel-body">
        <form id="EducationForm">
            {{csrf_field()}}
            <input type="hidden" id="reg_num" value="{{Auth::user()->reg_num}}"/>

            <div class="row">
              <div class="">
                <label class="col-sm-1 control-label">Start Date</label>
                <div class="col-sm-3">
                  <input type="date" name="" class="form-control" id="st_date" value="{{$show->st_dt}}" readonly/>
                </div>
              </div>

              <div class="">
                <label class="col-sm-1 control-label">End Date</label>
                <div class="col-sm-3">
                  <input type="date" name="" class="form-control" id="en_date"  value="{{$show->en_dt}}" readonly/>
                </div>
              </div>

            <div class="">
              <label class="col-sm-1 control-label">Name of School</label>
              <div class="col-sm-3">
                <input type="text" name="" class="form-control" id="school"  value="{{$show->sch_name}}" readonly/>
              </div>
            </div>

            </div>
<br />
          <div class="row">
              <div class="">
                <label class="col-sm-1 control-label">Study Method</label>
                <div class="col-sm-3">
                  <input type="text" name="" class="form-control" id="study_metd"  value="{{$show->study_mtd}}" readonly/>
                </div>
              </div>

              <div class="">
                <label class="col-sm-1 control-label">Exams taken</label>
                <div class="col-sm-3">
                  <input type="text" name="" class="form-control" id="exam_taken" value="{{$show->ex_taken}}" readonly/>
                </div>
              </div>

              <div class="">
                <label class="col-sm-1 control-label">Level </label>
                <div class="col-sm-3">
                  <input type="text" name="" class="form-control" id="level" value="{{$show->level}}" readonly/>
                </div>
              </div>
          </div>
      <br />
            <div class="row">
                <div class="">
                  <label class="col-sm-1 control-label">Paper </label>
                  <div class="col-sm-3">
                    <input type="text" name="" class="form-control" id="Paper" value="{{$show->paper}}" readonly/>
                  </div>
                </div>

                <div class="">
                  <label class="col-sm-1 control-label">Result </label>
                  <div class="col-sm-3">
                    <input type="text" name="" class="form-control" id="Results" value="{{$show->results}}" readonly/>
                  </div>
                </div>

                <div class="">
                  <label class="col-sm-1 control-label">Remarks </label>
                  <div class="col-sm-3">
                    <input type="text" name="" class="form-control" id="Remarks" value="{{$show->remarks}}" readonly/>
                  </div>
                </div>
           </div>


           <br />
          <div class="text-center" id="ajax2">
            <a class="btn btn-primary text-info" data-target="edu-edit/{{$show->id}}"><i class="fa fa-edit"></i> Edit</a>
          </div>
        </form>
      </div>
      <div class="panel-footer">
        Regent University College Of Science and Technology
      </div>
      </div>
  </div>
@endsection

  @push('scripts')
  <script>
    $(document).ready(function(){
        $('#ajax2 a').on('click',function(event){
          event.preventDefault();
          var $this = $(this);
          target = $this.data('target');
          $.ajax({
            method:'GET',
            url:target,
            beforeSend: function(){
                $("#replace").html('Working On...');
            },
            success:function(data){
              $("#replace").html(data);
            }
          });
        });
    });
  </script>
@endpush
