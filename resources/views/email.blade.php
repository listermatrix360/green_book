@component('mail::message')
# Mentor Endorsement

The Following student is requesting for endorsement as a mentor
<strong>
{{$std_name}}
<br />
{{$id}}
</strong>

@component('mail::button',['url'=>$url])
Click  here
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
