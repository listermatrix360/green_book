<!Doctype html>
    <html>
      <head>
          <title>Authourization Request</title>
          <style>
              html, body {
                  background-color: #fff;
                  color: #636b6f;
                  font-family: 'Raleway', sans-serif;
                  font-weight: 100;
                  height: 100vh;
                  margin: 0;
              }
              .text-font{
                font-family: 'Lora', serif;
              }
              .text-center {
                text-align: center;
              }
              .btn {
                display: inline-block;
                margin-bottom: 0;
                font-weight: normal;
                text-align: center;
                vertical-align: middle;
                -ms-touch-action: manipulation;
                    touch-action: manipulation;
                cursor: pointer;
                background-image: none;
                border: 1px solid transparent;
                white-space: nowrap;
                padding: 6px 16px;
                font-size: 13px;
                line-height: 1.846;
                border-radius: 3px;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
              }
              .btn:focus,
              .btn:active:focus,
              .btn.active:focus,
              .btn.focus,
              .btn:active.focus,
              .btn.active.focus {
                outline: 5px auto -webkit-focus-ring-color;
                outline-offset: -2px;
              }
              .btn:hover,
              .btn:focus,
              .btn.focus {
                color: #444444;
                text-decoration: none;
              }
              .btn:active,
              .btn.active {
                outline: 0;
                background-image: none;
                -webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
                box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
              }
              .btn-primary{
                color: #ffffff;
                background-color: #2196f3;
                border-color: transparent;
              }
              .btn-primary:focus,
              .btn-primary.focus {
                color: #ffffff;
                background-color: #0c7cd5;
                border-color: rgba(0, 0, 0, 0);
              }
              .btn-primary:hover {
                color: #ffffff;
                background-color: #0c7cd5;
                border-color: rgba(0, 0, 0, 0);
              }
              .btn-danger {
                color: #ffffff;
                background-color: #e51c23;
                border-color: transparent;
              }
              .btn-danger:focus,
              .btn-danger.focus {
                color: #ffffff;
                background-color: #b9151b;
                border-color: rgba(0, 0, 0, 0);
              }
              .btn-danger:hover {
                color: #ffffff;
                background-color: #b9151b;
                border-color: rgba(0, 0, 0, 0);
              }
              .btn-danger:active,
              .btn-danger.active,
              .open > .dropdown-toggle.btn-danger {
                color: #ffffff;
                background-color: #b9151b;
                border-color: rgba(0, 0, 0, 0);
              }
          </style>
    </head>

    <body>
      <div class="container">
        <h3>To The Warden</h3>
          <h4 style="text-align:justify">
            The Following student has requested for your remarks for participating in Community Chapel Service,
             <br /> ID Number :{{$reg_num}}
             <br /> Name :{{$sender_name}} {{$sender_last_name}}

          <h5>Click on the Link Below to See Details of Actvities</h5>

          </h4>

        <div class="">
        <a href="{{route('chaplain_remark_validation',["token"=>$sender_token])}}" class="btn btn-primary">Click Here</a>
        </div>

      </div> 

    </body>
    </html>
