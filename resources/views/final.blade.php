<!Doctype>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>STUDENT | PER @if(!empty($pd)) {{$pd->reg_num}} @endif </title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Alegreya|Allura|Calligraffitti|Cinzel|Cinzel+Decorative|Cormorant+Garamond|Cormorant+Upright|Courgette|Fontdiner+Swanky|Great+Vibes|Italianno|Josefin+Slab|Kurale|Lora|Merienda|Mogra|Mr+De+Haviland|Noto+Serif|Playball|Raleway|Sacramento|Slabo+13px|Slabo+27px|Spectral|Tangerine|Yellowtail" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link rel="shortcut icon" href="{{{ asset('logoo.png') }}}">
    <link rel="stylesheet" href="{{asset('materialize/css/materialize.min.css')}}" />
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link rel="shortcut icon" href="{{{ asset('admin.png') }}}">
    <script src="{{ asset('js/jquery-3.2.1.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{asset('materialize/js/materialize.min.js')}}"></script>
    <script src="{{ asset('pdf/printThis.js') }}"></script>
  </head>
    <body class="text-font">
      <!-- <a class="waves-effect waves-light btn" href="javascript:genPDF()">button</a> -->
      <nav class="green darken-4 cinzel">
         <div class="nav-wrapper">
           <a href="#" class="brand-logo right"><img src="{{asset('logoo.png')}}" style="height:65px"/></a>
           <ul id="nav-mobile" class="left hide-on-med-and-down">
             <li><a href="#">REGENT UNIVERSITY COLLEGE OF SCIENCE AND TECHNOLOGY</a></li>
             <li><a href="#">STUDENT PRACTICAL EXPERIENCE RECORD </a></li>
              <li><a href="#" id="pdf">GET PDF </a></li>
           </ul>
         </div>
      </nav>
      <br />
      @if(!empty($pd))
      <div class="container">
        <div class="row">
          <div class="col s12 center-align" style="margin-top:5%">
              <img src="{{asset('404.jpg')}}" class="col-md-offset-2" style="height:300px; width:450px"/>
            <div class="">
              <h3 class="text-style text-center text-danger">OOOPS NOTHING HERE!!</h3>
            </div>
        </div>
      </div>
    </div>
      @else

      <div class="container text-font" id="printable">
        <div class="row">

          <div class="col s4 m4">
              <div class="card-image">
                @if($pd->img=='null')
                <img src="{{asset('css/img/Kristy.png')}}" class="responsive-img" alt="nothing here"/>
                  @else
                <img src="{{asset('storage/greenFolder/'.$pd->img)}}" class="z-depth-1 responsive-img materialboxed" width="300" height="300">
                @endif
              </div>
            </div>

            <div class="col s8">
        <h4 class="text-font">{{$pd->title}} {{$pd->othernames}} {{$pd->surname}}</h4>
        <h4 class="text-font">{{$pd->reg_num}}, Level: {{$pd->level}}</h4>
        <h4 class="text-font">{{$pd->program}}</h4>
      </div>

            <div class="col s12">
                <br /><br />
                <div class="row">
                  <form class="col s12">
                    <div class="row">
                      <div class="input-field col s3">
                        <input placeholder="Placeholder" value="{{$pd->session}}" type="text" class="validate" readonly>
                        <label for="first_name">SESSION</label>
                      </div>

                      <div class="input-field col s2">
                        <input id="last_name" type="text" value="{{$pd->gender}}" class="validate" readonly>
                        <label for="last_name">GENDER</label>
                      </div>
                      <div class="input-field col s3">
                        <input id="last_name" type="text" value="{{$pd->dob}}" class="validate" readonly>
                        <label for="last_name">DATE OF BIRTH</label>
                      </div>
                      <div class="input-field col s4">
                        <input id="last_name" type="text" value="{{$pd->society}}" class="validate" readonly>
                        <label for="last_name">SOCIETY</label>
                      </div>
                    </div>
                    <div class="row">

                      <div class="input-field col s6">
                        <input id="last_name" type="text" value="{{$pd->email}}" class="validate" readonly>
                        <label for="last_name">EMAIL 1</label>
                      </div>

                      <div class="input-field col s6">
                        <input id="last_name" type="text" value="{{$pd->email2}}" class="validate" readonly>
                        <label for="last_name">EMAIL 2</label>
                      </div>
                    </div>

                    <div class="row">
                      <div class="input-field col s4">
                        <input placeholder="Placeholder" value="{{$pd->nationality}}" type="text" class="validate" readonly>
                        <label for="first_name">NATIONALITY</label>
                      </div>

                      <div class="input-field col s4">
                        <input id="last_name" type="text" value="{{$pd->phone1}}" class="validate" readonly>
                        <label for="last_name">PHONE 1</label>
                      </div>

                      <div class="input-field col s4">
                        <input id="last_name" type="text" value="{{$pd->phone2}}" class="validate" readonly>
                        <label for="last_name">PHONE 2</label>
                      </div>
                    </div>

                  </form>
                </div>
              </div>

            <div class="col s12">
                        <br /><br />
                    <h4 class="cinzel center-align">Mentor</h4>
                      <hr />
                    @if($mentor->isEmpty())
                      <h5 class="text-font">No Data Submitted</h5>
                      @else
                      <table class="bordered striped centered">
                        <thead>
                          <tr><th>Level</th>
                            <th>Mentor</th>
                          </tr></thead>
                          <tbody>
                            @foreach($mentor as $ment)
                            <tr>
                              <td>{{$ment->level}}</td>
                              <td>{{$ment->title}} {{$ment->f_name}} {{$ment->las_name}}</td>

                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      @endif
              </div>

        </div>



          <!-- Diary -->
            <div class="col s12">
            <br />
            <h4 class="cinzel center-align">Diary</h4>
            <hr />
              @if($diary->isEmpty())
              <blockquote style="text-align:justify">
                <p>
                  No data available !!!
                </p>
              </blockquote>
                @else
              @foreach($diary as $dd)

              {{$dd->title}}    {{$dd->date}}

                    <blockquote style="text-align:justify">
                      {!!html_entity_decode($dd->body)!!}
                    </blockquote>
                @endforeach
                @endif
          </div>
            <div class="col s12 text-font">
            <br />
            @if($intern->isEmpty())
              <h4 class="center-align cinzel">INDUSTRIAL ATTACHMENT
              </h4>
              <hr />
              No Information Available
              @else
              <h4 class="center-align cinzel">INDUSTRIAL ATTACHMENT
              </h4>
              <hr />
              @foreach($intern as $attach)
              <h5 class="center-align">From <b>{{$attach->st_date}}</b> to <b>{{$attach->en_date}}</b></h5>
              <p class="t2" style="text-align:justify">
                <h5 class="center-align">Employer Details</h5>
                <div  class="row">
                  <form class="col s12">
                    <div class="row">
                      <div class="input-field col s6">
                        <input placeholder="Placeholder" id="first_name" value="{{$attach->emp_name}}" type="text" readonly>
                        <label for="first_name">EMPLOYER</label>
                      </div>
                      <div class="input-field col s6">
                        <input id="last_name" type="text" class="validate" value="{{$attach->emp_email}}" readonly>
                        <label for="last_name">EMPLOYEE EMAIL</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s6">
                      <input placeholder="Placeholder" id="first_name" value="{{$attach->address}}" type="text" readonly>
                      <label for="first_name">ADDRESS</label>
                    </div>
                    <div class="input-field col s6">
                      <input id="last_name" type="text" class="validate" value="{{$attach->department}}" readonly>
                      <label for="last_name">DEPARTMENT</label>
                  </div>
                </div>

                <div class="row">
                  <div class="input-field col s6">
                    <input placeholder="Placeholder" id="first_name" value="{{$attach->job_title}}" type="text" readonly>
                    <label for="first_name">JOB TITLE</label>
                  </div>
                  <div class="input-field col s6">
                    <input id="last_name" type="text" class="validate" value="{{$attach->exp_area}}" readonly>
                    <label for="last_name">EXPERIENCE AREA</label>
                </div>
              </div>
                </form>
              </div>
                <h5 class="center-align">
                  <em>Work as summarised by student</em>
                </h5>
                <p>
                  <blockquote style="text-align:justify">
                    {!!html_entity_decode($attach->work_sum)!!}
                  </blockquote>
                </p>
              </p>
              <p>
                <h5 class="center-align"><em>
                  Employer / Supervisor's Endorsement
                </em>
              </h5>
              <blockquote style="text-align:justify">
                I certify that <b>{{$pd->title}} {{$pd->surname}} {{$pd->othernames}}</b> was
                employed by <b>{{$attach->org_name}}</b> between <b>{{$attach->st_date}}</b> and <b>{{$attach->en_date}}</b>
                and has satisfactorily completed the activities summarised above.
                <br />
                <label><b>Postion held</b></label>: {{$attach->pos_held}},
                <br />
                {{$attach->emp_name}}
                <br />
                <label><b>Professional Status</b></label>: {{$attach->emp_prof_status}}
                <label><b>Organisation</b></label>: {{$attach->org_name}}
                <!-- <blockquote> -->
                <h5><em>
                  Comments on attitude to work, ability to communicate, willingness to act on own initiative
                  and to excercise judgement.
                </em>
              </h5>
              <hr />

              {!!html_entity_decode($attach->comments)!!}

              <!-- </blockquote> -->
            </blockquote>
              </p>
              @endforeach
            @endif
            </div>
            <div class="col s12 text-font">
              <br />
              <h4 class="center-align">PROFESSIONAL SKILLS</h4>
              <hr />
              @if($prof->isEmpty())
              No details submitted
              @else

              @foreach($prof as $skills)
              <blockquote style="text-align:justify">

                <h5 class="">{{$skills->title}}</h5>
                {!!html_entity_decode($skills->prof_skill_body)!!}

              </blockquote>
              @endforeach
              @endif
            </div>
            <div class="col s12 text-font">
              <br />
              <h4 class="center-align cinzel">EDUCATION AND EXAMINATION<small>(Outside the University)</small></h4>
              <hr />
              @if($educ->isEmpty())
              No details submitted
              @else
              <table class="striped highlight responsive-table">
                <thead>
                  <tr>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>School</th>
                    <th>Method</th>
                    <th>Exams</th>
                    <th>Level</th>
                    <th>Paper</th>
                    <th>Results</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($educ as $learn)
                  <tr>
                    <td>{{$learn->st_dt}}</td>
                    <td>{{$learn->en_dt}}</td>
                    <td>{{$learn->sch_name}}</td>
                    <td>{{$learn->study_mtd}}</td>
                    <td>{{$learn->ex_taken}}</td>
                    <td>{{$learn->level}}</td>
                    <td>{{$learn->paper}}</td>
                    <td>{{$learn->results}}</td>
                    <td>{{$learn->remarks}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
            </div>
            <div class="col s12 text-font">
              <br />
              <h4 class="center-align cinzel">OTHERS COURSES ATTENDED <small>(Organized Within the University)</small></h4>
              <hr />
              @if($other->isEmpty())
              No details submitted
              @else
              <table class="striped table-responsive highlight">
                <thead>
                  <tr>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Course</th>
                    <th>Description</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($other as $course)
                  <tr>
                    <td>{{$course->date1}}</td>
                    <td>{{$course->date2}}</td>
                    <td>{{$course->cours_name}}</td>
                    <td>{{$course->descrip}}</td>
                    <td>{{$course->remarks}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
            </div>
            <br />
              <hr />

            <div class="row">

          <!-- Chapel -->
          <div class="col s12">
            <h5 class="cinzel center-align">Chapel Service</h5>
            <hr />
            <table class="bordered striped centered">
               <thead>
                 <tr><th>Date</th>
                 <th>Activity</th>
               </tr></thead>
               <tbody>
                 @foreach($chap as $ch)
                   <tr>
                     <td>{{$ch->date}}</td>
                     <td>{{$ch->activity}}</td>
                   </tr>
                 @endforeach

               </tbody>
           </table>
          </div>
            </div>
        <hr />

            <!-- Community Services -->
          <div class="col s12">
            <h5 class="cinzel center-align">Community Services</h5>
            <hr />
            <table class="bordered striped centered">
               <thead>
                 <tr><th>Date</th>
                 <th>Activity</th>
               </tr></thead>
               <tbody>
                 @foreach($com as $comm)
                   <tr>
                     <td>{{$comm->date}}</td>
                     <td>{{$comm->activity}}</td>
                   </tr>
                 @endforeach

               </tbody>
           </table>
          </div>
            <!-- Physical Excercises -->
          <div class="col s12">
            <h5 class="cinzel center-align">Physical Excercise</h5>
            <hr />
            @if($phy->isEmpty())
                No Data Yet !!
                  @else

                    <table class="bordered striped centered">
                      <thead>
                        <tr>
                          <th>Date</th>
                          <th>Activity</th>
                        </tr>

                      </thead>
                      <tbody>
                        @foreach($phy as $ph)
                          <tr>
                            <td>{{$ph->date}}</td>
                            <td>{{$ph->activity}}</td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                @endif
          </div>

          <div class="col s12 text-font">
            <h5 class="center-align cinzel">AUTOBIOGRAPHY</h5>
            <hr />
            @if(!empty($auto))
            <h6>There is nothing here !!</h6>
            @else
            <blockquote class="t2  text-font" style="text-align:justify">
              {!!html_entity_decode($auto->auto_body)!!}
            </blockquote>
            @endif
          </div>

    </div>


          <footer class="page-footer">
                  <div class="footer-copyright">
                    <div class="container">
                    © 2018 P.E.R , 2018
                    </div>
                  </div>
        </footer>
        @endif
    </body>

    <script type="text/javascript">
      $(document).ready(function(){
          $('#pdf').on('click',function(e){
            e.preventDefault();
            var me = $(this);
            me.hide();
            setTimeout(function(){
              $('body').printThis({
              debug: false,           // show the iframe for debugging
              importCSS: true,        // import parent page css
              importStyle: false,     // import style tags
              printContainer: false,   // print outer container/$.selector
              loadCSS: "{{asset('materialize/css/materialize.min.css')}}",  // load an additional css file - load multiple stylesheets with an array []
              pageTitle: "P.E.R @if(!empty($pd)){{$pd->reg_num}} @endif",          // add title to print page
              removeInline: false,    // remove all inline styles
              printDelay: 555,        // variable print delay
              header: null,           // prefix to html
              footer: null,           // postfix to html
              formValues: true,       // preserve input/form values
              canvas: true,          // copy canvas content (experimental)
              base: false,            // preserve the BASE tag, or accept a string for the URL
              doctypeString: '<!DOCTYPE html>', // html doctype
              removeScripts: false,   // remove script tags before appending
              copyTagClasses: false   // copy classes from the html & body tag
              });
            },555);
            setTimeout(function(){
              me.show();
            },1000);
          })
      })
    </script>
</html>
