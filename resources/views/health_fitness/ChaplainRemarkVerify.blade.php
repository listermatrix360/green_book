<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title></title>
  <link rel="stylesheet" href="{{asset('css/paper.css')}}">
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <link rel="shortcut icon" href="{{{ asset('logoo.png') }}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap-select.min.css')}}">
  <link href="https://fonts.googleapis.com/css?family=Alegreya|Cinzel+Decorative|Cormorant|Courgette|Fontdiner+Swanky|Great+Vibes|Lobster|Lora|Mogra|Mr+Dafoe|Mr+De+Haviland|Noto+Serif|Oregano|Poiret+One|Pompiere|Raleway|Slabo+27px|Spectral|Vibur|Yellowtail" rel="stylesheet">
  <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
</head>
<body>

  <div class="container-fluid">
    @if(!empty($chapelActivity))
    <br />
      <h4 class="text-center text-danger">Get Out Of Here !!!! By the Count of 5!!!! Thief</h4>
      @else


        <div class="col-md-8 col-md-offset-2"style="margin-top:10%;">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="text-center text-font"><u>Student Community Chapel Attendance</u>
                <small>(Chaplains's Remark)</small></h4>
            </div>

            <div class="panel-body">
              <table class="table table-striped text-font">
                <thead>
                  <tr>
                    <th>ID Number</th>
                    <th>Date</th>
                    <th>Activity</th>
                    <th>Approval Status</th>
                  </tr>
                </thead>

                <tfoot>
                  <tr>
                    <th>ID Number</th>
                    <th>Date</th>
                    <th>Activity</th>
                    <th>Approval Status</th>
                  </tr>
                </tfoot>

                <tbody class="table-hover table-striped">
                  @foreach($chapelActivity as $all)
                  <tr>
                    <td>{{$all->reg_num}}</td>
                    <td>{{$all->date}}</td>
                    <td>{{$all->activity}}</td>
                    <td>{{$all->status}}</td>
                  </tr>
                  @endforeach
                </tbody>

              </table>
              <form class="form-horizontal" action="{{route('remark_of_chaplain')}}" method="post" role="form">

                {{csrf_field()}}
                <input type="hidden" value="{{$all->token}}" name="hashed_token" />
                <div class="form-group">
                  <label class="col-md-4 control-label"><i class="fa fa-user"></i> Name </label>
                  <div class="col-md-4">
                    <input type="text" class="form-control col-xm-3" value="{{$all->name}} {{$all->la_name}}" readonly/>
                  </div>
                </div>


                <div class="form-group">
                  <label class="col-md-4 control-label"> <i class="fa fa-comment"></i> Comment</label>
                  <div class="col-md-4">
                    <textarea class="form-control"name="comment" required></textarea>
                  </div>
                </div>

                <div class="text-center">
                  <button class="btn btn-primary" type="submit">Send Response <i class="fa fa-send"></i></button>
                </div>
                <p class=" text-center" style="margin-top:2%;">

                </p>
              </form>
            </div>
            <div class="panel-footer">
              Regent University College Of Science And Technology
            </div>
          </div>
        </div>
        @endif
  </div>
</body>


</html>
