<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title></title>
  <link rel="stylesheet" href="{{asset('css/paper.css')}}">
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <link rel="shortcut icon" href="{{{ asset('logoo.png') }}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap-select.min.css')}}">
  <link href="https://fonts.googleapis.com/css?family=Alegreya|Cinzel+Decorative|Cormorant|Courgette|Fontdiner+Swanky|Great+Vibes|Lobster|Lora|Mogra|Mr+Dafoe|Mr+De+Haviland|Noto+Serif|Oregano|Poiret+One|Pompiere|Raleway|Slabo+27px|Spectral|Vibur|Yellowtail" rel="stylesheet">
  <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
</head>
<body>
  <nav class="navbar navbar-default text-font">
      <div class="container">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
              <a class="" href="#"><img src="{{asset('logoo.png')}}" style="height:65px"/></a>
          </div>
          <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                  <li><a href="#">THE PRACTICAL EXPERIENCE RECORD</a></li>
                  <li><a href="#">HEALTH & FITNESS <i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
                  <li>
                    <a href="#">Community Service Endorsement</a>
                  </li>
              </ul>
          </div>
      </div>
    </nav>

  <div class="container-fluid">
    @if(!empty($community))
    <br />
    <div class="col-md-6 col-md-offset-3" style="margin-top:5%">
        <img src="{{asset('404.jpg')}}" class="col-md-offset-2" style="height:300px; width:450px"/>
      <div class="">
        <h3 class="text-style text-center text-danger">THIS URL HAS EXPIRED!!</h3>
      </div>
    </div>
      @else

        <div class="col-md-6 col-md-offset-3 text-font" style="margin-top:5%;">
          <div class="panel-heading">
            <h4 class="text-center text-font"><u>Community Service Endorsement</u></h4>
          </div>
          <div class="text-center">
            <img src="{{asset('storage/greenFolder/'.$community->img)}}" alt="Image not available" class="img-rounded" style="height:200px; width:300px ">
          </div>
              <form class="form-horizontal" action="{{route('ess_verify')}}" method="post" role="form">
                    {{csrf_field()}}
                  <input type="hidden" value="{{$community->verifyToken}}" name="hashed_token" />
                <div class="form-group">
                  <label class="col-md-4 control-label"><i class="fa fa-user"></i> Name </label>
                    <div class="col-md-4">
                      <input type="text" class="form-control col-xm-3" value="{{$community->title}} {{$community->surname}} {{$community->othernames}}" readonly/>
                    </div>
                </div>

                <div class="form-group">
                  <label class="col-md-4 control-label"><i class="fa fa-user"></i> Program </label>
                    <div class="col-md-6">
                      <input type="text" class="form-control col-xm-3" value="{{$community->program}}" readonly/>
                    </div>
                </div>

                <div class="form-group">
                  <label class="col-md-4 control-label"><i class="fa fa-id-card"></i> ID number</label>
                  <div class="col-md-4">
                    <input type="number" name="id"  value="{{$community->reg_num}}" class="form-control col-xm-3"  readonly/>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-4 control-label"><i class="fa fa-id-card"></i> Activity</label>
                  <div class="col-md-7">
                    <input type="text" name="id"  value="{{$community->activity}}" class="form-control col-xm-3"  readonly/>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-4 control-label"><i class="fa fa-id-card"></i> Date</label>
                  <div class="col-md-4">
                    <input type="date" name="id"  value="{{$community->date}}" class="form-control col-xm-3"  readonly/>
                  </div>
                </div>

                  <div class="form-group">
                    <label class="col-md-4 control-label"> <i class="fa fa-gavel"></i> Action</label>
                    <div class="col-md-4">
                    <select class="selectpicker" name="action" required>
                      <option></option>
                      <option>Approved</option>
                      <option>Rejected</option>
                    </select>
                  </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-4 control-label"> <i class="fa fa-comment"></i> Comment</label>
                    <div class="col-md-4">
                      <textarea class="form-control"name="comment" required></textarea>
                    </div>
                  </div>

                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Send Response <i class="fa fa-send"></i></button>
                  </div>
                  <p class=" text-center" style="margin-top:2%;">

                  </p>
          </form>
        </div>
        @endif
  </div>
</body>


</html>
