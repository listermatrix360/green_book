<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title></title>
  <link rel="stylesheet" href="{{asset('css/paper.css')}}">
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <link rel="shortcut icon" href="{{{ asset('logoo.png') }}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap-select.min.css')}}">
  <link href="https://fonts.googleapis.com/css?family=Alegreya|Cinzel+Decorative|Cormorant|Courgette|Fontdiner+Swanky|Great+Vibes|Lobster|Lora|Mogra|Mr+Dafoe|Mr+De+Haviland|Noto+Serif|Oregano|Poiret+One|Pompiere|Raleway|Slabo+27px|Spectral|Vibur|Yellowtail" rel="stylesheet">
  <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
</head>
<body>
  <nav class="navbar navbar-default text-font">
      <div class="container">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
              <a class="" href="#"><img src="{{asset('logoo.png')}}" style="height:65px"/></a>
          </div>
          <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                  <li><a href="#">THE PRACTICAL EXPERIENCE RECORD</a></li>
                  <li><a href="#">HEALTH & FITNESS <i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
                  <li>
                    <a href="#">Warden Community Service Endorsement</a>
                  </li>
              </ul>
          </div>
      </div>
    </nav>
  <div class="container-fluid">
    @if(count($warden)==0)
    <br />
    <div class="col-md-6 col-md-offset-3" style="margin-top:5%">
        <img src="{{asset('404.jpg')}}" class="col-md-offset-2" style="height:300px; width:450px"/>
      <div class="">
        <h3 class="text-style text-center text-danger">THIS URL HAS EXPIRED!!</h3>
      </div>
    </div>
      @else


        <div class="col-md-8 col-md-offset-2"style="margin-top:5%;">
          <div class="text-center">
            <img src="{{asset('storage/greenFolder/'.$data->img)}}" alt="image not available" class="img-rounded" style="height:200px; width:300px ">
          </div>
          <br />
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="text-center text-font"><u>Student Community Service</u>
                <small>(Warden's Remark)</small></h4>
            </div>

            <div class="panel-body">
              <table class="table table-striped text-font">
                <thead>
                  <tr>
                    <th>ID Number</th>
                    <th>Date</th>
                    <th>Activity</th>
                    <th>Status</th>
                  </tr>
                </thead>

                <tfoot>
                  <tr>
                    <th>ID Number</th>
                    <th>Date</th>
                    <th>Activity</th>
                    <th>Status</th>
                  </tr>
                </tfoot>

                <tbody class="table-hover table-striped">
                  @foreach($warden as $all)
                  <tr>
                    <td>{{$all->reg_num}}</td>
                    <td>{{$all->date}}</td>
                    <td>{{$all->activity}}</td>
                    <td>{{$all->status}}</td>
                  </tr>
                  @endforeach
                </tbody>

              </table>
              <form class="form-horizontal" action="{{route('community_warden_verify')}}" method="post" role="form">

                {{csrf_field()}}
                <input type="hidden" value="{{$all->token}}" name="hashed_token" />
                <div class="form-group">
                  <label class="col-md-4 control-label"><i class="fa fa-user"></i> Name </label>
                  <div class="col-md-4">
                    <input type="text" class="form-control col-xm-3" value="{{$all->surname}} {{$all->othernames}}" readonly/>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-4 control-label"><i class="fa fa-user"></i> Program </label>
                  <div class="col-md-4">
                    <input type="text" class="form-control col-xm-3" value="{{$data->program}}" readonly/>
                  </div>
                </div>


                <div class="form-group">
                  <label class="col-md-4 control-label"> <i class="fa fa-comment"></i> Comment</label>
                  <div class="col-md-4">
                    <textarea class="form-control"name="comment" required></textarea>
                  </div>
                </div>

                <div class="text-center">
                  <button class="btn btn-primary" type="submit">Send Response <i class="fa fa-send"></i></button>
                </div>
                <p class=" text-center" style="margin-top:2%;">

                </p>
              </form>
            </div>
            <div class="panel-footer">
              Regent University College Of Science And Technology
            </div>
          </div>
        </div>
        @endif
  </div>
</body>


</html>
