







@extends('blueprint')
@section('content')
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="cinzel">Regent-Ghana Community Services <small class="pull-right"><i class="fa fa-tree" aria-hidden="true"></i>
                        <i class="fa fa-university" aria-hidden="true"></i> <i class="fa fa-wheelchair" aria-hidden="true"></i>
                        <i class="fa fa-gavel" aria-hidden="true"></i> <i class="fa fa-hospital-o" aria-hidden="true"></i></small></h3>
            </div>
            <div class="panel-body">
                @if(!empty($details))
                    <form class="text-font community">
                        {{ csrf_field() }}
                        <div class="row">

                            <div class="">
                                <label class="col-sm-1">Date</label>
                                <div class="col-sm-3" id="col1">
                                    <input type="date" name="date" class="form-control date" placeholder="date" autofocus  required />
                                </div>
                            </div>

                            <div class="">
                                <label class="col-sm-1">Activity</label>
                                <div class="col-sm-4" id="pen">

                                    <input type="text" name="activity" class="form-control activity" placeholder="e.g clean up at mallam market" autofocus  required />

                                </div>
                            </div>
                            <div class="email" id="editBtn">
                                <button class="btn btn-primary" type="submit" id="btn-save">Save</button>
                            </div>
                        </div>
                    </form>
                    <hr />
                    @foreach($community as $labour)
                        <form class="text-font" id="com_frm_1">
                            {{ csrf_field() }}
                            <div class="row">
                                <input type="hidden" id="reg_num" value="{{Auth::user()->reg_num}}"/>
                                <input type="hidden" id="sender_name" value="{{Auth::user()->surname}} {{Auth::user()->othernames}}"/>
                                <input type="hidden" value="{{$labour->verifyToken}}" id="token1" />


                                <div class="">
                                    <label class="col-sm-1">Date</label>
                                    <div class="col-sm-3" id="col1">
                                        <input type="date" id="date" class="form-control date" value="{{$labour->date}}" readonly/>
                                    </div>
                                </div>

                                <div class="">
                                    <label class="col-sm-1">Activity</label>
                                    <div class="col-sm-4" id="pen">
                                        <input type="text" id="activity" class="form-control" value="{{$labour->activity}}" readonly/>
                                    </div>
                                </div>
                                <div class="email" id="editBtn">

                                    @if($labour->status=='pending')
                                        <a class="btn btn-primary trigger"  id="request" href="{{$labour->verifyToken}}"><i class="fa fa-telegram"></i> Request End</a>
                                        <a class="btn btn-success"  id="edit" href="{{$labour->id}}"><i class="fa fa-pencil-square-o fa-1x"></i></a>
                                    @elseif($labour->status=='Approved')
                                        <a class="btn btn-success">Approved</a>
                                    @elseif($labour->status=='Rejected')
                                        <a class="btn btn-danger" data-toggle="popover" tabindex="0" role="button" data-trigger="focus" title="Reason for rejection" data-content="{{$labour->comment}}" data-placement="top" data-container="body" ><i class="fa fa-thumbs-o-down"></i> Click Here</a>

                                        <a class="btn btn-success"  id="edit" href="{{$labour->id}}"><i class="fa fa-pencil-square-o fa-1x"></i></a>
                                    @endif
                                </div>
                            </div>
                        </form>
                        <br />
                    @endforeach

                    <form class="text-font" id="RemarkForm">
                        {{ csrf_field() }}
                        <div class="row">

                            <div class="">
                                <label class="col-md-2">FINAL REMARKS</label>
                                <div class="col-md-6">
                                    @if(empty($remarks))
                                        <input type="text" id="remark" class="form-control" placeholder="Comments By Student" autofocus  required />
                                    @else
                                        <input type="text" id="" class="form-control" value="{{$remarks->student_remark}}" readonly />
                                    @endif
                                </div>
                            </div>

                            <div>
                                @if(!empty($remarks))
                                    <a class="btn btn-primary" data-toggle="modal" data-target="#RemarkModal"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                @else
                                    <button class="btn btn-primary" type="submit" id="btn-remark">Save Remark</button>
                                @endif
                            </div>
                        </div>
                    </form>
                    <br /> <br />
                    <form class="text-font">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="">
                                <label class="col-md-4">FINAL REMARKS By E.S.S Director</label>
                                <div class="col-md-6">
                                    @if(empty($remarks))
                                        <input type="text" id="" class="form-control" placeholder="Not Yet Seen By The Ess Diretor" readonly/>
                                    @else
                                        <input type="text" id="" class="form-control" value="{{$remarks->ess_remark}}" readonly/>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                    <br />
                    <form class="text-font">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="">
                                <label class="col-md-4">FINAL REMARKS BY SOCIETAL WARDEN</label>
                                <div class="col-md-6">
                                    @if(!empty($remarks))
                                        <input type="hidden" value="{{$remarks->verifyToken}}" id="cherubim"/>
                                        <input type="hidden" value="{{$details->society->name}}" id="society" />
                                        @if($remarks->warden_remark=='null')
                                            <div class="text-font">
                                                <a class="btn btn-danger" id="WardenBtn"><i class="fa fa-telegram"></i> Get Warden's Remark</a>
                                            </div>
                                        @else
                                            <input type="text" id="" class="form-control" value="{{$remarks->warden_remark}}" readonly/>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                @else
                    <div class="text-center col-md-6 col-md-offset-3 text-font" id="ajax2">
                        <h4 class="text-muted">
                            <i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i>
                            <br />
                            Hello {{Auth::user()->surnames}} {{Auth::user()->othernames}} ,
                            Your Pesonal Details Information Is Needed Before Accessing This Section
                            <br />
                            Use The Link Bellow To Navigate To The Required Page
                            <br />

                            <i class="fa fa-angle-double-down fa-2x" aria-hidden="true"></i>
                        </h4>
                        <a class="btn btn-danger" data-target="/book/create"> Basic Info</a>
                    </div>
                @endif

            </div>
            <div class="panel-footer sub-color-bg">
                Regent University College Of Science and Technology
            </div>
        </div>
    </div>

    <div class="modal fade" id="CommunityModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Info. For Community Service</h4>
                </div>
                <div class="modal-body">
                    <form class="text-font form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-2">Date</label>
                            <div class="col-md-6" id="mod-div-date">
                                <input type="date" id="date" class="form-control date" placeholder="date" autofocus  required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2">Activity</label>
                            <div class="col-md-7" id="mod-div-activity">
                                <input type="text" id="activity" class="form-control activity" placeholder="e.g clean up at mallam market" autofocus  required />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" type="submit" id="mod-button">Save changes</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="RemarkModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-font" id="myModalLabel">Change Remarks</h4>
                </div>
                <div class="modal-body">
                    <form class="text-font form-horizontal" id="RemarkModal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-3">FINAL REMARKS</label>
                            <div class="col-md-6" id="mod-div-date">
                                @if(!empty($remarks))
                                    <input type="text" class="form-control"id="modal-div" value="{{$remarks->student_remark}}" autofocus  required />
                                @else
                                    <input type="text" class="form-control" value="" autofocus  required />
                                @endif
                            </div>

                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" type="submit">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
    @push('scripts')
        <script>
            $(document).ready(function(){
                $('#WardenBtn').on('click',function(event){
                    event.preventDefault();
                    var code    = $('#cherubim').val();
                    var id      = $('#reg_num').val();
                    var society = $('#society').val();
                    console.log(code,id,society);
                    $.ajax({
                        type:'get',
                        url:'{{route('return_warden_email')}}',
                        data: {
                            society:society
                        },
                        datatype:'json',
                        beforeSend:function(){
                            $('#WardenBtn').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>...Sending');
                        },
                        success:function(data){
                            var email = data.email;
                            console.log(email);
                            $.ajax({
                                type:'get',
                                url: '/warden_mail',
                                data: {
                                    email:email,
                                    code:code,
                                    id : id,
                                },
                                datatype: 'json',
                                success:function(data){
                                    alert(data);
                                },
                                error:function (x,e){
                                    if(x.status==0){
                                        alert('You are offline..Check Your Internet Connection');
                                    }
                                    else if(x.status==500){
                                        alert('Email Could Not Be Sent Check Your Connection');
                                        $('#WardenBtn').html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>...Retry');
                                    }
                                    else{
                                        alert('unknown Error has occured');
                                    }
                                }
                            })
                        },

                    })
                });
            });
            $('#ajax2 a').on('click',function(event){
                event.preventDefault();
                var $this = $(this);
                target = $this.data('target');
                $.ajax({
                    method:'GET',
                    url:target,
                    beforeSend: function(){
                        $("#replace").html('Working On...');
                    },

                    success:function(data){
                        $("#replace").html(data);
                        $('.selectpicker').selectpicker('refresh');
                        // if (target == '{{'/book/'}}'){
                        //     $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#">Industrial Attachment</li><li><a href="#"><b>Professional Skill</b></li></a>');
                        // }
                    }
                });
            });


        </script>


        <script>
            $(document).ready(function(){
                function side_nav(){
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type:'get',
                        url:'/side_nav',
                        success:function(data){
                            $('#sidenav').html(data)
                        }
                    })
                }

                $(function () {
                    $('[data-toggle="popover"]').popover()
                });

                $('.email #edit').on('click',function(event){
                    event.preventDefault();
                    var rid = this.getAttribute('href');

                    console.log(rid);
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method:'get',
                        datatype: 'json',
                        url:'/community-edit/'+rid,
                        cache: false,
                        success:function(data){
                            $.each(data,function(i,item){
                                $('#CommunityModal').modal();
                                $('#mod-div-date').html('<input type="date" id="mod-date" value="'+data[i].date+'" class="form-control"/>');
                                $('#mod-div-activity').html('<input type="text" id="mod-activity" value="'+data[i].activity+'" class="form-control"/>');
                            });
                            $('#CommunityModal').on('hide.bs.modal',function(){
                                setTimeout(function(){
                                    $.ajax({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        type:'get',
                                        url:'{{route('community-index')}}',
                                        success:function(data){
                                            $("#replace").html(data);
                                            console.log('Page Loading worked');
                                        },
                                        error:function(){
                                            console.log('Page Loading Failed!!')
                                        }
                                    });
                                },300)
                            })
                        }
                    });
                    $('#mod-button').on('click',function(event){
                        event.preventDefault();
                        var getDate = $('#mod-date').val();
                        var getActivity   = $('#mod-activity').val();

                        console.log(getDate,getActivity,rid);
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type:'post',
                            url:'/community-post/'+rid,
                            data:{
                                getDate      : getDate,
                                getActivity  : getActivity,
                                '_token':$('input[name=_token]').val()
                            },
                            datatype: 'json',
                            success:function(data){

                                console.log(data);
                                $('#CommunityModal').modal('hide');
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    type:'get',
                                    url:'{{route('community-index')}}',
                                    success:function(data){
                                        $("#replace").html(data);
                                        console.log('Page Loading worked');
                                    },
                                    error:function(){
                                        console.log('Page Loading Failed!!')
                                    }
                                });
                            },
                            error:function(){
                                console.log('it failed!!')
                            }
                        });
                    });

                });
                $('.email #request').on('click',function(event){
                    event.preventDefault();
                    var token = this.getAttribute('href');
                    var btn_id = $(this);
                    console.log(btn_id);

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },type:'GET',url: 'community-token/'+token,
                        beforeSend:function(){ $(btn_id).html('<i class="fa  fa-spinner fa-pulse fa-fw"></i>...Please Wait');},
                        success:function(data){$.each(data,function(i,itme){var name = $('#sender_name').val();
                            var getToken = data[i].verifyToken; var getDate = data[i].date; var getRegNum = data[i].reg_num;
                            var getActivity = data[i].activity; $.ajax({ type:'GET', url:'/com_mail',
                                data: { name      :    name,  getToken:    getToken,  getDate :    getDate,
                                    getRegNum:   getRegNum, getActivity: getActivity, },
                                datatype: 'json', beforeSend:function(){
                                    $(btn_id).html('<i class="fa  fa-spinner fa-pulse fa-fw"></i>...Sending');
                                },
                                success:function(data){
                                    $(btn_id).html('Mail Sent!!!');
                                },
                                error:function(x,e){ if(x.status==0){ alert('You are offline..Check Your Internet Connection');
                                }  else if(x.status==500){ alert('Email Could Not Be Sent Check Your Connection');
                                    $(btn_id).html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>...Retry');
                                }  else{ alert('unknown Error has occured');
                                }

                                }
                            });
                        });
                        },
                        error:function(){
                            console.log('it failed')
                        }
                    });
                });
                $('.community').one('submit',function(e){
                    e.preventDefault();  var myform = $(this).closest("form");
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method:'post', url:'{{route('comm-store')}}', data:myform.serialize(),
                        success:function(data)
                        {
                            side_nav();
                        },
                        error:function(x,e){
                            if(x.status==401){

                                window.location.replace('/login');
                            }
                            else if (x.status ==500){
                                alert('data could not be posted');}}
                    })


                    $.ajax({type:'get',url:'{{route('community-index')}}',success:function(data){
                            $("#replace").html(data);console.log('Page Loading worked');},error:function(){
                            console.log('Page Loading Failed!!')}});});

                $('#RemarkForm').submit(function(e){
                    e.preventDefault();
                    var formID = $(this).attr('id');

                    console.log(formID);
                    var  getRegNum   = $('#reg_num').val();
                    var  getRemark   = $('#remark').val();
                    console.log(getRegNum,getRemark);
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type:'POST',
                        url :'{{route('comm-remark')}}',
                        datatype: 'json',
                        data: {
                            getRegNum   :getRegNum,
                            getRemark   :getRemark,
                            '_token':$('input[name=_token]').val()
                        },
                        success:function(data){
                            console.log(data);
                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                type:'get',
                                url:'{{route('community-index')}}',
                                success:function(data){
                                    $("#replace").html(data);
                                    console.log('Page Loading worked');
                                },
                                error:function(){
                                    console.log('Page Loading Failed!!')
                                }
                            });
                        },
                        error:function(){
                            console.log('Post Failed');
                        }
                    })
                });
                $('#RemarkModal').submit(function(e){
                    e.preventDefault();
                    var formID = $(this).attr('id');
                    console.log(formID);
                    var  getRegNum   = $('#reg_num').val();
                    var  getRemark   = $('#modal-div').val();
                    console.log(getRegNum,getRemark);
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type:'POST',
                        url :'{{route('comm-update')}}',
                        datatype: 'json',
                        data: {
                            getRegNum   :getRegNum,
                            getRemark   :getRemark,
                            '_token':$('input[name=_token]').val()
                        },
                        success:function(data){
                            $('#RemarkModal').modal('hide');
                            console.log(data);
                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                type:'get',
                                url:'{{route('community-index')}}',
                                success:function(data){
                                    $("#replace").html(data);
                                    console.log('Page Loading worked');
                                },
                                error:function(){
                                    console.log('Page Loading Failed!!')
                                }
                            });
                        },
                        error:function(){
                            console.log('Post Failed');
                        }
                    })
                });
            });
        </script>
    @endpush
