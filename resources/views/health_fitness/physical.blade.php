@extends('blueprint')
@section('content')
    <div class="col-md-9">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="cinzel">Physical Fitness <small>(Gym Workout & Endurance Walks)</small>   <small class="pull-right"><i class="fa fa-university" aria-hidden="true"></i>
           <i class="fa fa-gavel" aria-hidden="true"></i> <i class="fa fa-book" aria-hidden="true"></i> <i class="fa fa-hospital-o" aria-hidden="true"></i> <i class="fa fa-plus" aria-hidden="true"></i></small>
    </h4>
        </div>

        <div class="panel-body">
            @if(!empty($details))
          <!-- Testing single form submission -->
          <form class="text-font form" id="">
            {{ csrf_field() }}
            <div class="row">
              <div class="">
                <label class="col-sm-1">Date</label>
                <div class="col-sm-3" id="col1">
                  <input type="date" name="getDate" class="form-control" placeholder="date" autofocus  required />
                    </div>
              </div>

              <div class="">
                <label class="col-sm-1">Activity</label>
                <div class="col-sm-4">
                  <input type="text" name="getActivity" class="form-control" placeholder="e.g chapel service attended" autofocus  required />
                </div>
              </div>
              <div>
                <button class="btn btn-info" type="submit" id="btn-save">Save</button>
              </div>
            </div>
          </form>
    <hr />
    <!-- <br /> -->
            <div class="">
              @if($data->isEmpty())
              <div class="alert alert-warning alert-dismissible" role="alert" id="error">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                         <strong>{{Auth::user()->othernames}},</strong> You havent saved any Physical fitness activity yet!!
                    </div>
                @else
                    @foreach($data as $fitness)
                        <form class="text-font">
                          {{ csrf_field() }}
                          <div class="row">
                            <div class="">
                              <label class="col-sm-1">Date</label>
                              <div class="col-sm-3" id="col1">
                                <input type="date" name="getDate" class="form-control" value="{{$fitness->date}}" readonly />
                                <input type="hidden" id="sender_name" class="form-control" value="{{Auth::user()->surname}} {{Auth::user()->othernames}}" readonly />
                                  </div>
                            </div>

                            <div class="">
                              <label class="col-sm-1">Activity</label>
                              <div class="col-sm-4">
                                <input type="text" name="getActivity" class="form-control" value="{{$fitness->activity}}" readonly/>
                              </div>
                            </div>
                            <div class="email" id="editBtn">
                              @if($fitness->status=='pending')
                              <a class="btn btn-primary"  id="request" href="{{$fitness->verifyToken}}"><i class="fa fa-telegram"></i> Request END.</a>
                              <a class="btn btn-success" id="edit" href="{{$fitness->id}}"><i class="fa fa-pencil-square-o fa-1x"></i></a>
                              @elseif($fitness->status=='Approved')
                              <a class="btn btn-success">Approved</a>
                              @elseif($fitness->status=='Rejected')
                            <a class="btn btn-danger" data-toggle="popover" tabindex="0" role="button" data-trigger="focus" title="Reason for rejection" data-content="{{$fitness->comment}}" data-placement="top" data-container="body" ><i class="fa fa-thumbs-o-down"></i> Click Here</a>
                              <a class="btn btn-success" id="edit" href="{{$fitness->id}}"><i class="fa fa-pencil-square-o fa-1x"></i></a>
                              @endif
                            </div>
                          </div>
                        </form>
                        <br />
                      @endforeach
                @endif
              </div>

          @else
          <div class="text-center col-md-6 col-md-offset-3 text-font" id="ajax2">
            <h4 class="text-muted">
                  <i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i>
                  <br />
              Hello {{Auth::user()->name}} {{Auth::user()->la_name}} , Your Pesonal Details Information Is Needed Before Accessing This Section
              <br />
              Use The Link Bellow To Navigate To The Required Page
              <br />

              <i class="fa fa-angle-double-down fa-2x" aria-hidden="true"></i>
            </h4>
            <a class="btn btn-danger" data-target="/book/create"> Basic Info</a>
          </div>
          @endif
        </div>
        <div class="panel-footer sub-color-bg">
          Regent University College of Science and Technology.
        </div>
      </div>
    </div>

    <div class="modal fade" id="PhysicalModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
            <h4 class="modal-title text-font" id="myModalLabel">Make Changes For Physical Excercises  <small class="pull-right"><i class="fa fa-university" aria-hidden="true"></i>
             <i class="fa fa-gavel" aria-hidden="true"></i> <i class="fa fa-book" aria-hidden="true"></i>
             <i class="fa fa-hospital-o" aria-hidden="true"></i> <i class="fa fa-plus" aria-hidden="true"></i></small></h4>
          </div>
          <div class="modal-body">
            <form class="text-font form-horizontal">
              {{ csrf_field() }}
              <div class="form-group">
                <label class="col-md-2">Date</label>
                <div class="col-md-6" id="mod-div-date">
                  <input type="date" id="date" class="form-control date" placeholder="date" autofocus  required />
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-2">Activity</label>
                <div class="col-md-7" id="mod-div-activity">
                  <input type="text" id="activity" class="form-control activity" placeholder="e.g clean up at mallam market" autofocus  required />
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="submit" id="mod-button">Save changes</button>
              </div>

            </form>

          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="RemarkModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-font" id="myModalLabel">Change Remarks</h4>
      </div>
      <div class="modal-body">
        <form class="text-font form-horizontal" id="RemarkModal">
          {{ csrf_field() }}
          <div class="form-group">
            <label class="col-md-3">FINAL REMARKS</label>
            <div class="col-md-6" id="mod-div-date">
              @if(!empty($remark))
              <input type="text" class="form-control"id="modal-div" value="{{$remark->ess_remark}}" autofocus  required />
              @else
              <input type="text" class="form-control" value="" autofocus  required />
              @endif
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button class="btn btn-primary" type="submit">Save changes</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

    @endsection



@push('scripts')
    <script type="text/javascript">
    $(document).ready(function(){
        function side_nav(){
            $.ajax({
                type:'get',
                url:'/side_nav',
                success:function(data){
                    $('#sidenav').html(data)
                }
            })
        }
        $(function () {
            $('[data-toggle="popover"]').popover()
        });

        $('#PhysicalModal').on('hide.bs.modal',function(){
            $.ajax({
                type:'get',
                url:'{{route('physical-index')}}',
                success:function(data){
                    $("#replace").html(data);
                    console.log('Page Loading worked');
                },
                error:function(){
                    console.log('Page Loading Failed!!')
                }
            });
        });


        $('.email #edit').on('click',function(e){
            e.preventDefault();
            var rid = this.getAttribute('href');
            console.log(rid);
            $.ajax({
                method:'GET',
                datatype: 'json',
                url:'/physical-edit/'+rid,
                success:function(data){
                    $.each(data,function(i,item){
                        $('#PhysicalModal').modal();
                        $('#mod-div-date').html('<input type="date" id="mod-date" value="'+data[i].date+'" class="form-control"/>');
                        $('#mod-div-activity').html('<input type="text" id="mod-activity" value="'+data[i].activity+'" class="form-control"/>');
                    });
                }
            });
            $('#mod-button').on('click',function(e){
                e.preventDefault();
                var getDate = $('#mod-date').val();
                var getActivity   = $('#mod-activity').val();

                console.log(getDate,getActivity,rid);
                $.ajax({
                    type:'post',
                    url:'/physical-post/'+rid,
                    data:{
                        getDate      : getDate,
                        getActivity  : getActivity,
                        '_token':$('input[name=_token]').val()
                    },
                    datatype: 'json',
                    success:function(data){

                        console.log(data);
                        $('#PhysicalModal').modal('hide');
                        $.ajax({
                            type:'get',
                            url:'{{route('physical-index')}}',
                            success:function(data){
                                $("#replace").html(data);
                                console.log('Page Loading worked');
                            },
                            error:function(){
                                console.log('Page Loading Failed!!')
                            }
                        });
                    },
                    error:function(){
                        console.log('it failed!!')
                    }
                });
            });

        });
        $('#RemarkForm').one('submit',function(e){
            e.preventDefault();
            var formID = $(this).attr('id');
            console.log(formID);
            var  getRemark   = $('#remark').val();
            console.log(getRemark);
            $.ajax({
                type:'POST',
                url :'{{route('spirit-remark')}}',
                datatype: 'json',
                data: {
                    getRemark   :getRemark,
                    '_token':$('input[name=_token]').val()
                },
                success:function(data){
                    console.log(data);
                    $.ajax({
                        type:'get',
                        url:'{{route('spirit-index')}}',
                        success:function(data){
                            $("#replace").html(data);
                            console.log('Page Loading worked');
                        },
                        error:function(){
                            console.log('Page Loading Failed!!')
                        }
                    });
                },
                error:function(){
                    console.log('Post Failed');
                }
            })
        });
        $('#RemarkModal').one('submit',function(e){
            e.preventDefault();
            var formID = $(this).attr('id');
            console.log(formID);
            var  getRemark   = $('#modal-div').val();
            console.log(getRemark);
            $.ajax({
                type:'POST',
                url :'{{route('chapel-remark-update')}}',
                datatype: 'json',
                data: {
                    getRemark   :getRemark,
                    '_token':$('input[name=_token]').val()
                },
                success:function(data){
                    $('#RemarkModal').modal('hide');
                    console.log(data);
                    $.ajax({
                        type:'get',
                        url:'{{route('spirit-index')}}',
                        success:function(data){
                            $("#replace").html(data);
                            console.log('Page Loading worked');
                        },
                        error:function(){
                            console.log('Page Loading Failed!!')
                        }
                    });
                },
                error:function(){
                    console.log('Post Failed');
                }
            })
        });
        $('.form').one('submit',function(e){
            e.preventDefault();
            var myForm = $(this).closest('form');
            // var formID = $(this).attr('id');
            $.ajax({
                type:'post',
                url:'{{route('physical-post')}}',
                data: myForm.serialize(),
                success:function(data){
                    side_nav();
                    //Refreshing the page after form submission
                    $.ajax({
                        type:'get',
                        url:'{{route('physical-index')}}',
                        success:function(data){
                            $("#replace").html(data);
                            console.log('Page Loading worked');
                        },
                        error:function(){
                            console.log('Page Loading Failed!!')
                        }
                    });
                }
            })
        })

    });
    $('.email #request').on('click',function(e){
        e.preventDefault()
        var token = this.getAttribute('href');
        var btn_id = $(this);
        console.log(btn_id);
        $.ajax({
            type:'GET',
            url: 'physical-token/'+token,
            beforeSend:function(){
                $(btn_id).html('<i class="fa  fa-spinner fa-pulse fa-fw"></i>...Please Wait');
            },
            success:function(data){
                $.each(data,function(i,itme){
                    var name = $('#sender_name').val();
                    var getToken = data[i].verifyToken;
                    var getDate = data[i].date;
                    var getRegNum = data[i].reg_num;
                    var getActivity = data[i].activity;
                    $.ajax({
                        type:'GET',
                        url:'/physical_mail',
                        data: {
                            getToken:    getToken,
                            name:    name,
                            getDate :    getDate,
                            getRegNum:   getRegNum,
                            getActivity: getActivity,
                        },
                        datatype: 'json',
                        beforeSend:function(){
                            $(btn_id).html('<i class="fa  fa-spinner fa-pulse fa-fw"></i>....Sending');
                        },
                        success:function(data){
                            console.log(data);
                            $(btn_id).html('<i class="fa  fa-send"></i> Sent!!!');
                        },
                        error:function(x,e){
                            if(x.status==0){
                                alert('You are offline..Check Your Internet Connection');
                            }
                            else if(x.status==500){
                                alert('Email Could Not Be Sent Check Your Connection');
                                $(btn_id).html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>...Retry');
                            }
                            else{
                                alert('unknown Error has occured');
                            }
                        }
                    });
                });
            },
            error:function(){
                console.log('it failed')
            }
        });
    });
    $('#ajax2 a').on('click',function(event){
        event.preventDefault();
        var $this = $(this);
        target = $this.data('target');
        $.ajax({
            method:'GET',
            url:target,
            beforeSend: function(){
                $("#replace").html('Working On...');
            },

            success:function(data){
                $("#replace").html(data);
                $('.selectpicker').selectpicker('refresh');
                // if (target == '{{'/book/'}}'){
                //     $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#">Industrial Attachment</li><li><a href="#"><b>Professional Skill</b></li></a>');
                // }
            }
        });
    });
</script>
    @endpush
