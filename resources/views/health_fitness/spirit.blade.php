

@extends('blueprint')
@section('content')
    <div class="col-md-9">
      <div class="panel panel-default">
        <div class="panel-heading text-font">
          <h4 class="cinzel">Spiritual Fitness: Community Chapel Attendance   <small class="pull-right"><i class="fa fa-university" aria-hidden="true"></i>
           <i class="fa fa-gavel" aria-hidden="true"></i> <i class="fa fa-book" aria-hidden="true"></i> <i class="fa fa-hospital-o" aria-hidden="true"></i> <i class="fa fa-plus" aria-hidden="true"></i></small>
      </h4>
          </div>
          <div class="panel-body">
            @if(!empty($details))
            <form class="text-font main-form">

              {{ csrf_field() }}
              <div class="row">
                <div class="">
                  <label class="col-sm-1">Date</label>
                  <div class="col-sm-3" id="col1">
                    <input type="date" name="getDate" class="form-control" placeholder="date" autofocus  required />

                  </div>
                </div>

                <div class="">
                  <label class="col-sm-1">Activity</label>
                  <div class="col-sm-4" id="pen">
                    <input type="text" name="getActivity" class="form-control" placeholder="e.g chapel service attended" autofocus  required />
                  </div>
                </div>

                <div class="email" id="editBtn">
                  <button class="btn btn-info" type="submit" id="btn-save">Save</button>
                </div>
              </div>
            </form>
            <br />
                @if($spirit->isEmpty())
                  <p>
                    No Data Here Yet!!
                  </p>
                    @else
                        @foreach($spirit as $chapel)
                      <form class="text-font">
                        {{ csrf_field() }}
                        <div class="row">

                          <div class="">
                            <label class="col-sm-1">Date</label>
                            <div class="col-sm-3" id="col1">

                              <input type="date" id="date" class="form-control" value="{{$chapel->date}}" autofocus  required />

                            </div>
                          </div>

                          <div class="">
                            <label class="col-sm-1">Activity</label>
                            <div class="col-sm-4" id="pen">
                              <input type="text" id="activity" class="form-control" value="{{$chapel->activity}}" readonly/>
                            </div>
                          </div>
                          <div class="email" id="editBtn">

                            @if($chapel->status=='pending')
                            <a class="btn btn-primary trigger"  id="request" href="{{$chapel->verifyToken}}"><i class="fa fa-telegram"></i> Request Auth.</a>
                            <a class="btn btn-success" id="edit" href="{{$chapel->id}}"><i class="fa fa-pencil-square-o fa-1x"></i></a>
                            @elseif($chapel->status=='Approved')
                            <a class="btn btn-success" data-toggle="popover" tabindex="0" role="button" data-trigger="focus" title="Comment" data-content="{{$chapel->comment}}" data-placement="top" data-container="body" ><i class="fa fa-thumbs-o-up"></i> Approved</a>
                            @elseif($chapel->status=='Rejected')
                            <a class="btn btn-danger" data-toggle="popover" tabindex="0" role="button" data-trigger="focus" title="Reason for rejection" data-content="{{$chapel->comment}}" data-placement="top" data-container="body" ><i class="fa fa-thumbs-o-down"></i> Click Here</a>
                            <a class="btn btn-success"  id="edit" href="{{$chapel->id}}"><i class="fa fa-pencil-square-o fa-1x"></i></a>
                            @endif

                          </div>
                        </div>
                      </form>
                      <br />
                @endforeach
                @endif


              <br />
              <form class="text-font" id="RemarkForm">
                {{ csrf_field() }}
                <div class="row">
                <div class="">
                  <label class="col-md-2">FINAL REMARKS</label>
                  <div class="col-md-6">
                      @if(!empty($remark))
                          <input type="text" id="remark" class="form-control" value="{{$remark->student_remark}}" readonly/>
                      @else
                          <input type="text" id="remark" class="form-control" placeholder="Remarks of students" required autofocus/>
                      @endif
                  </div>
                </div>

                <div>
                    @if(!empty($remark))
                  <a class="btn btn-primary" data-toggle="modal" data-target="#RemarkModal"><i class="fa fa-pencil-square-o"></i> Edit</a>
                    @else
                  <button class="btn btn-primary" type="submit" id="btn-remark">Save Remark</button>
                    @endif
                </div>
              </div>
              </form>
                <br />
              <form class="text-font">
                {{ csrf_field() }}

                <div class="row">
                <div class="">
                  <label class="col-md-4">FINAL REMARKS BY CHAPLAIN</label>
                  <div class="col-md-6">
                    @if(!empty($remark))
                      <input type="hidden" value="{{$remark->verifyToken}}" id="cherubim"/>
                      <input type="hidden" id="reg_num" value="{{Auth::user()->reg_num}}"/>
                      @if($remark->chaplain_remark=='null')
                      <div class="text-font">
                        <a class="btn btn-danger" id="ChaplainBtn"><i class="fa fa-telegram"></i> Get Chaplain's Remark</a>
                      </div>
                      @else
                      <input type="text" id="" class="form-control" value="{{$remark->chaplain_remark}}" readonly/>
                      @endif
                    @endif
                  </div>
                </div>
              </div>
              </form>
            @else
            <div class="text-center col-md-6 col-md-offset-3 text-font" id="ajax2">
              <h4 class="text-muted">
                    <i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i>
                    <br />
                Hello {{Auth::user()->surname}} {{Auth::user()->othernames}} , Your Pesonal Details Information Is Needed Before Accessing This Section
                <br />
                Use The Link Bellow To Navigate To The Required Page
                <br />

                <i class="fa fa-angle-double-down fa-2x" aria-hidden="true"></i>
              </h4>
              <a class="btn btn-danger" data-target="/book/create"> Basic Info</a>
            </div>
            @endif
          </div>
          <div class="panel-footer text-font">
              Regent University College Of Science and Technology.
          </div>
        </div>
    </div>

    <div class="modal fade" id="CommunityModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Make Changes For Chapel Service</h4>
                </div>
                <div class="modal-body">
                    <form class="text-font form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-2">Date</label>
                            <div class="col-md-6" id="mod-div-date">
                                <input type="date" id="date" class="form-control date" placeholder="date" autofocus  required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2">Activity</label>
                            <div class="col-md-7" id="mod-div-activity">
                                <input type="text" id="activity" class="form-control activity" placeholder="e.g clean up at mallam market" autofocus  required />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" type="submit" id="mod-button">Save changes</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="RemarkModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-font" id="myModalLabel">Change Remarks</h4>
            </div>
            <div class="modal-body">
                <form class="text-font form-horizontal" id="RemarkModal">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-md-3">FINAL REMARKS</label>
                        <div class="col-md-6" id="mod-div-date">
                            @if(!empty($remark))
                                <input type="text" class="form-control"id="modal-div" value="{{$remark->student_remark}}" autofocus  required />
                            @else
                                <input type="text" class="form-control" value="" autofocus  required />
                            @endif
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    @endsection

@push('scripts')
    <script type="text/javascript">
    $(document).ready(function(){
      $(function () {
        $('[data-toggle="popover"]').popover()
      });
      function side_nav(){
        $.ajax({
          type:'get',
          url:'/side_nav',
          success:function(data){
            $('#sidenav').html(data)
          }
        })
      }
        $('.main-form').one('submit',function(e){
          e.preventDefault();
            var myform = $(this).closest("form");
              $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'post',
                url:'{{route('chapel-post')}}',
                data:myform.serialize(),
                datatype:'json',
                success:function(data){
                  console.log(data);
                  side_nav();
                  $.ajax({
                    type:'get',
                    url:'{{route('spirit-index')}}',
                    success:function(data){
                      $("#replace").html(data);
                      console.log('Page Loading worked');
                    },
                    error:function(){
                      console.log('Page Loading Failed!!')
                    }
                  });
                },
                error:function(){
                  console.log('Post Failed')
                }
              });
        })

        $('.email #edit').on('click',function(e){
               e.preventDefault();
              var rid = this.getAttribute('href');
              console.log(rid);
              $.ajax({
                method:'GET',
                datatype: 'json',
                url:'/chapel-edit/'+rid,
                cache: false,
                success:function(data){
                  $.each(data,function(i,item){
                    $('#CommunityModal').modal();
                    $('#mod-div-date').html('<input type="date" id="mod-date" value="'+data[i].date+'" class="form-control"/>');
                    $('#mod-div-activity').html('<input type="text" id="mod-activity" value="'+data[i].activity+'" class="form-control"/>');
                  });
                          $('#CommunityModal').on('hide.bs.modal',function(){
                            $.ajax({
                              type:'get',
                              url:'{{route('spirit-index')}}',
                              success:function(data){
                                $("#replace").html(data);
                                console.log('Page Loading worked');
                              },
                              error:function(){
                                console.log('Page Loading Failed!!')
                              }
                            });
                          })
                }
              });
                  $('#mod-button').on('click',function(e){
                    e.preventDefault();
                    var getDate = $('#mod-date').val();
                    var getActivity   = $('#mod-activity').val();

                    console.log(getDate,getActivity,rid);
                      $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type:'post',
                        url:'/spirit-post/'+rid,
                        data:{
                          getDate      : getDate,
                          getActivity  : getActivity,
                          '_token':$('input[name=_token]').val()
                        },
                        datatype: 'json',
                        success:function(data){

                           console.log(data);
                           $('#CommunityModal').modal('hide');
                          $.ajax({
                            type:'get',
                            url:'{{route('spirit-index')}}',
                            success:function(data){
                              $("#replace").html(data);
                              console.log('Page Loading worked');
                            },
                            error:function(){
                              console.log('Page Loading Failed!!')
                            }
                          });
                        },
                        error:function(){
                          console.log('it failed!!')
                        }
                      });
                    });

            });
        $('.email #request').on('click',function(e){
              e.preventDefault()
              var token = this.getAttribute('href');
              var btn_id = $(this);
                console.log(btn_id);
                      $.ajax({
                          type:'GET',
                          url: 'spirit-token/'+token,
                          beforeSend:function(){
                                  $(btn_id).html('<i class="fa  fa-spinner fa-pulse fa-fw"></i>....PLEASE WAIT');
                          },

                          success:function(data){
                            $.each(data,function(i,itme){
                              var getToken = data[i].verifyToken;
                              var getDate = data[i].date;
                              var getRegNum = data[i].reg_num;
                              var getActivity = data[i].activity;
                              var getName = '{{Auth::user()->title}} {{Auth::user()->surname}} {{Auth::user()->othernames}}';
                              // console.log(getToken,getDate,getRegNum,getActivity,getName);
                            $.ajax({
                              type:'GET',
                              url:'/spirit_mail',
                              data: {
                                    getToken:    getToken,
                                    getDate :    getDate,
                                    getRegNum:   getRegNum,
                                    getActivity: getActivity,
                                    getName:  getName
                              },
                              datatype: 'json',

                              beforeSend:function(){
                                      $(btn_id).html('<i class="fa  fa-spinner fa-pulse fa-fw"></i>....Sending');
                              },

                              success:function(data){
                                  $(btn_id).html('<i class="fa  fa-telegram"></i>...Mail Sent!!');
                              },
                              error:function(x,e){
                                if(x.status==0){ alert('You are offline..Check Your Internet Connection'); }
                                else if(x.status==500){
                                alert('Email Could Not Be Sent Check Your Connection');
                                $(btn_id).html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>...Retry');

                                }
                                else{
                                  alert('unknown Error has occured');
                                }

                              }
                            });
                          });
                          },
                          error:function(){
                            console.log('it failed')
                          }
                      });
            });
        $('#RemarkForm').one('submit',function(e){
          e.preventDefault();
          var formID = $(this).attr('id');
          console.log(formID);
          var  getRemark   = $('#remark').val();
          console.log(getRemark);
            $.ajax({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
                type:'POST',
                url :'{{route('spirit-remark')}}',
                datatype: 'json',
                data: {
                  getRemark   :getRemark,
                  '_token':$('input[name=_token]').val()
                },
                success:function(data){
                  console.log(data);
                      $.ajax({
                        type:'get',
                        url:'{{route('spirit-index')}}',
                        success:function(data){
                          $("#replace").html(data);
                          console.log('Page Loading worked');
                        },
                        error:function(){
                          console.log('Page Loading Failed!!')
                        }
                      });
                },
                error:function(){
                  console.log('Post Failed');
                }
            })
            });
        $('#RemarkModal').one('submit',function(e){
                e.preventDefault();
              var formID = $(this).attr('id');
              console.log(formID);
              var  getRemark   = $('#modal-div').val();
              console.log(getRemark);
                $.ajax({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                    type:'POST',
                    url :'{{route('chapel-remark-update')}}',
                    datatype: 'json',
                    data: {
                      getRemark   :getRemark,
                      '_token':$('input[name=_token]').val()
                    },
                    success:function(data){
                      $('#RemarkModal').modal('hide');
                      console.log(data);
                          $.ajax({
                            type:'get',
                            url:'{{route('spirit-index')}}',
                            success:function(data){
                              $("#replace").html(data);
                              console.log('Page Loading worked');
                            },
                            error:function(){
                              console.log('Page Loading Failed!!')
                            }
                          });
                    },
                    error:function(){
                      console.log('Post Failed');
                    }
                })
              });
        $('#ChaplainBtn').on('click',function(e){
                e.preventDefault();
                 var code    = $('#cherubim').val();
                 var id      = $('#reg_num').val();
                    console.log(code,id);
                    $.ajax({
                      type:'get',
                      url:'{{route('return_chap_email')}}',
                      datatype:'json',
                      beforeSend:function(){
                        $('#ChaplainBtn').html('<i class="fa fa-spinner fa-pulse fa-fw"></i>...Please Wait');
                      },
                      success:function(data){
                          var email = data.email;
                          console.log(email);
                        $.ajax({
                            type:'get',
                            url: '/chaplain_mail',
                            data: {
                              email:email,
                              code:code,
                              id : id,
                            },
                            datatype: 'json',
                            success:function(data){
                              alert(data);
                            },
                            error:function (x,e){
                              if(x.status==0){
                                alert('You are offline..Check Your Internet Connection');
                              }
                              else if(x.status==500){
                                  alert('Email Could Not Be Sent Check Your Connection');
                                  $('#ChaplainBtn').html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>...Retry');
                              }
                              else{
                                alert('unknown Error has occured');
                              }
                            }
                        })
                      },

                    })
              });
        $('#ajax2 a').on('click',function(event){
                event.preventDefault();
                var $this = $(this);
                target = $this.data('target');
                $.ajax({
                  method:'GET',
                  url:target,
                  beforeSend: function(){
                      $("#replace").html('Working On...');
                  },

                  success:function(data){
                    $("#replace").html(data);
                    $('.selectpicker').selectpicker('refresh');
                    // if (target == '{{'/book/'}}'){
                    //     $('#bread').append('<li><a href="/book">Dashboard</li></a><li><a href="#">Industrial Attachment</li><li><a href="#"><b>Professional Skill</b></li></a>');
                    // }
                  }
                });
              });
    });
    </script>
    @endpush






