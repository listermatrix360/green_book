<!doctype html>
<html lang="{{ config('app.locale') }}" manifest="greenbook.appcache">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel @yield('title')</title>
        <!-- Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/mdb.min.css">
        <link rel="stylesheet" href="fawesome/css/font-awesome.min.css">




      <script src="bootstrap/js/bootstrap.min.js"></script>


    </head>
    <body>
      <header>
          <nav class="navbar navbar-toggleable-md navbar-dark mdb-color ">
              <div class="container">
                  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                  </button>
                  <a class="navbar-brand" href="#">
                      <img src="" class="d-inline-block align-top" alt="Greenbook">
                  </a>
                  <div class="collapse navbar-collapse" id="navbarNav">
                      <ul class="navbar-nav mr-auto">
                          <li class="nav-item active">
                              <a class="nav-link">Home <span class="sr-only">(current)</span></a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link">Features</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link">Pricing</a>
                          </li>
                          <li class="nav-item btn-group">
                              <a class="nav-link dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                  <a class="dropdown-item">Action</a>
                                  <a class="dropdown-item">Another action</a>
                                  <a class="dropdown-item">Something else here</a>
                              </div>
                          </li>
                      </ul>
                      <ul class="navbar-nav nav-flex-icons">
                          <li class="nav-item">
                              <a class="nav-link"><i class="fa fa-facebook"></i></a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link"><i class="fa fa-twitter"></i></a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link"><i class="fa fa-instagram"></i></a>
                          </li>
                      </ul>
                  </div>
              </div>
          </nav>

      </header>







                <div class="container">
<form>
  <div class="row">
    <div class="col-md-4">
         <div class="md-form">
    <label for="sell">Title</label>
    <input type="text" class="form-control" id="title" name="title"  required placeholder="">
                     </div>
             </div>
    <div class="col-md-4">
      <div class="md-form">
          <label for="first_name">Name</label>
        <input type="text" class="form-control">

      </div>
    </div>

    <div class="col-md-4">
      <div class="md-form">
          <label for="last_name">Last Name</label>
        <input id="last_name" type="text" class="form-control">
      </div>
      </div>

  </div>
  <div class="text-center">
    <button type="submit" class="btn btn-primary">Save</button>
</div>
  </div>
</form>
</div>


        <!-- Jquery-->
        <script
          src="https://code.jquery.com/jquery-3.2.1.min.js"
          integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
          crossorigin="anonymous"></script>

         <!--MDB JS-->
         <script src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.2/js/mdb.min.js"></script>

          <!--bootstrap Tether-->
         <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
         integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
         crossorigin="anonymous"></script>

         <script src="https://cdn.jsdelivr.net/mdbootstrap/4.3.2/tether.js"></script>

         <!--bootstrap Js---->
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
          integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
          crossorigin="anonymous"></script>
    </body>
</html>
