<nav class="navbar navbar-default text-font">
    <div class="container" style="width: 90%">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed ft-color" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
            <a class="" href="#"><img src="{{asset('logoo.png')}}" style="height:65px"/></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{route('book.index')}}">Dashboard</a></li>
                <li id="ajax"><a href="#" data-target="{{route('help')}}">Help</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Basic Info <span class="caret"></span></a>
                    <ul class="dropdown-menu cinzel" id="ajax">
                        <li><a href="{{route('personal.create')}}" >Personal Details <i class="fa fa-graduation-cap" aria-hidden="true"></i></a></li>
                        <li><a href="{{route('mentor.create')}}">Mentor <i class="fa fa-user-circle" aria-hidden="true"></i></a></li>
                    </ul>
                </li>
                <li class="" id="ajax">
                    <a href="{{route('diary.create')}}" data-target="diary/create"> Diary</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Internship <span class="caret"></span></a>
                    <ul class="dropdown-menu cinzel" id="ajax">
                        <li><a href="{{route('attach.create')}}" >Industrial Attachment <i class="fa fa-book"></i></a></li>
                        <li><a href="{{route('prof-skills')}}">Professional skills</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Education<span class="caret"></span></a>
                    <ul class="dropdown-menu cinzel" id="ajax">
                        <li><a href="{{route('educ-index')}}" id="education" data-target="">Education and Examination <small>(Outside the University)</small></a></li>
                        <li><a href="{{route('education.otherCourses')}}" id="otherCourses" data-target="">Other Courses Attended <small>(Organized within the University)</small></a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Health & Fitness<span class="caret"></span></a>
                    <ul class="dropdown-menu cinzel" id="ajax">
                        <li><a  href="{{route('community-index')}}">Community Services</a></li>
                        <li><a  href="{{route('physical-index')}}">Physical Exercise</a></li>
                        <li><a  href="{{route('spirit-index')}}">Spiritual Fitness</a></li>
                    </ul>
                </li>
                <li id="ajax">
                    <a  href="{{route('biography')}}">Autho-biography</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Welcome, {{Auth::user()->surname}}<span class="caret"></span></a>
                    <ul class="dropdown-menu cinzel">
                        <li id="ajax"><a href="#" data-target="{{route('key')}}">Change Password</a></li>
                        <li><a href="#photo" data-toggle="modal" data-target="#photo">Change Profile Photo</a></li>
                        <li>
                          <a href="#" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                          Logout
                        </a>
                        <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                        </form>
                      </li>
                    </ul>
                </li>
            <!-- </ul> -->
            </ul>
            <ul class="nav navbar-nav navbar">
               <a href="{{asset('storage/greenFolder/'.@$personal_details->img)}}">
                <img src="{{asset('storage/greenFolder/'.@$personal_details->img)}}"  style="border-radius: 10px; margin-top: 10px" alt="" height="50px">
               </a>

            </ul>
        </div>
    </div>
</nav>
