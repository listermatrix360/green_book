@extends('blueprint')
    @section('content')

        <div class=" col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="cinzel">Student Mentor</h4>
                        </div>
                        <div class="panel-body">
                          <!-- level 100 -->
                          <input type="hidden" value="{{Auth::user()->name}} {{Auth::user()->la_name}}" id="std_name"/>

                          <form class="form text-font" id="first">
                              {{ csrf_field() }}
                              <div class="row">
                                  <div>
                                    <label class="col-sm-1">LEVEL</label>
                                    <div class="col-sm-1">
                                      <input type="text" value="100" name="level" id="level" class="form-control" readonly/>
                                    </div>
                                  </div>

                                  <div>
                                  <label class="col-sm-3">NAME OF MENTOR</label>
                                  <div class="col-sm-4 text-edit">
                                      @if(!empty($l1))
                                      <input class="form-control" type="text" value="{{$l1->title}} {{$l1->f_name}} {{$l1->las_name}}" id="mentor" readonly/>
                                        @else
                                        <select class="selectpicker" id="ment_name"  name="ment_name" data-live-search="true" required>
                                                     <option value=""></option>
                                                        @foreach ($ment_list as $staff)
                                                     <option data-tokens="{{$staff->title}} {{$staff->f_name}} {{$staff->las_name}}" value="{{$staff->staff_id}}" name="ment_name">{{$staff->title}} {{$staff->f_name}} {{$staff->las_name}}</option>
                                                        @endforeach
                                          </select>
                                        @endif
                                  </div>
                              </div>
                              <div class="email" id="editBtn">
                              @if(!empty($l1))
                                @if($l1->status=='pending')
                              <a class="btn btn-primary trigger" id="request" href="{{$l1->verifyToken}}"><i class="fa fa-send"></i> Request End..</a>
                                <input type="hidden" value="{{$l1->staff_id}}" />
                              <a class="btn btn-info" href="{{$l1->id}}" id="edit"><i class="fa fa-edit"></i></a>
                              @elseif($l1->status=='Approved')
                                  <a class="btn btn-success">Approved</a>
                                  <a class="btn btn-info" href="{{$l1->id}}" id="edit"><i class="fa fa-edit"></i></a>
                                  @elseif($l1->status=='Rejected')
                                  <a class="btn btn-danger">Rejected</a>
                                  <a class="btn btn-info" href="{{$l1->id}}" id="edit"><i class="fa fa-edit"></i></a>
                              @endif
                              @else
                              <button type="submit" class="btn btn-primary submit">Save</button>
                              @endif
                            </div>
                            </div>
                          </form>
                            <br />
                          <!-- level 200 -->
                          <form class="text-font form">
                              {{ csrf_field() }}
                              <div class="row">
                                  <div>
                                    <label class="col-sm-1">LEVEL</label>
                                    <div class="col-sm-1">
                                      <input type="text" value="200" id="level" name="level" class="form-control" readonly/>
                                    </div>
                                  </div>

                                  <div>
                                  <label class="col-sm-3">NAME OF MENTOR</label>
                                  <div class="col-sm-4 text-edit">
                                      @if(!empty($l2))
                                      <input class="form-control" type="text" value="{{$l2->title}} {{$l2->f_name}} {{$l2->las_name}}" readonly/>
                                        @else
                                        <select class="selectpicker" id="ment_name" data-live-search="true" name="ment_name" required>
                                                     <option data-tokens=""></option>
                                                        @foreach ($ment_list as $staff)
                                                     <option data-tokens="{{$staff->title}} {{$staff->f_name}} {{$staff->las_name}}" value="{{$staff->staff_id}}" name="ment_name">{{$staff->title}} {{$staff->f_name}} {{$staff->las_name}}</option>
                                                        @endforeach
                                          </select>
                                        @endif
                                  </div>
                              </div>
                              <div class="email" id="editBtn">
                              @if(!empty($l2))
                                @if($l2->status =='pending')
                              <a class="btn btn-primary trigger" id="request" href="{{$l2->verifyToken}}"><i class="fa fa-send"></i> Request End..</a>
                                <input type="hidden" value="{{$l2->staff_id}}" />
                              <a class="btn btn-info" href="{{$l2->id}}" id="edit"><i class="fa fa-edit"></i></a>
                              @elseif($l2->status=='Approved')
                                  <a class="btn btn-success">Approved</a>
                                  <a class="btn btn-info" href="{{$l2->id}}" id="edit"><i class="fa fa-edit"></i></a>
                                  @elseif($l2->status=='Rejected')
                                  <a class="btn btn-danger">Rejected</a>
                                  <a class="btn btn-info" href="{{$l2->id}}" id="edit"><i class="fa fa-edit"></i></a>
                              @endif
                              @else
                              <button type="submit" class="btn btn-primary submit">Save</button>
                              @endif
                            </div>
                            </div>
                          </form>

                          <br />
                          <!-- level 300 -->
                          <form class="text-font form">
                              {{ csrf_field() }}
                              <div class="row">
                                  <div>
                                    <label class="col-sm-1">LEVEL</label>
                                    <div class="col-sm-1">
                                      <input type="text" value="300" name="level" class="form-control" readonly/>
                                    </div>
                                  </div>

                                  <div>
                                  <label class="col-sm-3">NAME OF MENTOR</label>
                                  <div class="col-sm-4 text-edit">
                                      @if(!empty($l3))
                                      <input class="form-control" type="text" value="{{$l3->title}} {{$l3->f_name}} {{$l3->las_name}}" readonly/>
                                        @else
                                        <select class="selectpicker" id="select" data-live-search="true" name="ment_name" required>
                                                     <option data-tokens=""></option>
                                                        @foreach ($ment_list as $staff)
                                                     <option data-tokens="{{$staff->title}} {{$staff->f_name}} {{$staff->las_name}}" value="{{$staff->staff_id}}" name="ment_name">{{$staff->title}} {{$staff->f_name}} {{$staff->las_name}}</option>
                                                        @endforeach
                                          </select>
                                        @endif
                                  </div>
                              </div>
                              <div class="email" id="editBtn">
                              @if(!empty($l3))
                                @if($l3->status =='pending')
                              <a class="btn btn-primary trigger" id="request" href="{{$l3->verifyToken}}"><i class="fa fa-send"></i> Request End..</a>
                                <input type="hidden" value="{{$l3->staff_id}}" />
                              <a class="btn btn-info" href="{{$l3->id}}" id="edit"><i class="fa fa-edit"></i></a>
                              @elseif($l3->status=='Approved')
                                  <a class="btn btn-success">Approved</a>
                                  <a class="btn btn-info" href="{{$l3->id}}" id="edit"><i class="fa fa-edit"></i></a>
                                  @elseif($l3->status=='Rejected')
                                  <a class="btn btn-danger">Rejected</a>
                                  <a class="btn btn-info" href="{{$l3->id}}" id="edit"><i class="fa fa-edit"></i></a>
                              @endif
                              @else
                              <button type="submit" class="btn btn-primary submit">Save</button>
                              @endif
                            </div>
                            </div>
                          </form>

                          <br />
                          <!-- level 400 -->
                          <form class="text-font form">
                              {{ csrf_field() }}
                              <div class="row">
                                  <div>
                                    <label class="col-sm-1">LEVEL</label>
                                    <div class="col-sm-1">
                                      <input type="text" value="400" name="level" class="form-control" readonly/>
                                    </div>
                                  </div>

                                  <div>
                                  <label class="col-sm-3">NAME OF MENTOR</label>
                                  <div class="col-sm-4 text-edit">
                                      @if(!empty($l4))
                                      <input class="form-control" type="text" value="{{$l4->title}} {{$l4->f_name}} {{$l4->las_name}}" readonly/>
                                        @else
                                        <select class="selectpicker" id="select" data-live-search="true" name="ment_name" required>
                                                     <option data-tokens=""></option>
                                                        @foreach ($ment_list as $staff)
                                                     <option data-tokens="{{$staff->title}} {{$staff->f_name}} {{$staff->las_name}}" value="{{$staff->staff_id}}" name="ment_name">{{$staff->title}} {{$staff->f_name}} {{$staff->las_name}}</option>
                                                        @endforeach
                                          </select>
                                        @endif
                                  </div>
                              </div>
                              <div class="email" id="editBtn">
                              @if(!empty($l4))
                                @if($l4->status =='pending')
                              <a class="btn btn-primary trigger" id="request" href="{{$l4->verifyToken}}"><i class="fa fa-send"></i> Request End..</a>
                                <input type="hidden" value="{{$l4->staff_id}}" />
                              <a class="btn btn-info" href="{{$l4->id}}" id="edit"><i class="fa fa-edit"></i></a>
                              @elseif($l4->status=='Approved')
                                  <a class="btn btn-success">Approved</a>
                                  <a class="btn btn-info" href="{{$l4->id}}" id="edit"><i class="fa fa-edit"></i></a>
                                  @elseif($l4->status=='Rejected')
                                  <a class="btn btn-danger">Rejected</a>
                                  <a class="btn btn-info" href="{{$l4->id}}" id="edit"><i class="fa fa-edit"></i></a>
                              @endif
                              @else
                              <button type="submit" class="btn btn-primary submit">Save</button>
                              @endif
                            </div>
                            </div>
                          </form>
                        </div>
                    </div>

                </div>

        <div class="modal fade" id="mentor-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title text-font" id="myModalLabel">Edit Mentor Details <small class="pull-right">
                            <i class="fa fa-plus"></i></small></h4>
                          <hr />
                        </div>
                        <div class="modal-body">
                          <form class="form-horizontal text-style" id="editForm">
                            {{csrf_field()}}

                            <div class="form-group">
                              <div class="col-sm-3">
                                <label class="control-label">LEVEL</label>
                              </div>
                              <div class="col-sm-5" id="mentor-level">

                              </div>
                            </div>

                            <div class="form-group">
                              <div class="col-sm-3">
                                <label class="control-label">MENTOR</label>
                              </div>
                              <div class="col-sm-5">
                                <select class="selectpicker" data-live-search="true" name="ment_name" required>
                                  <option data-tokens=""></option>
                                  @foreach ($ment_list as $staff)
                                  <option data-tokens="{{$staff->title}} {{$staff->f_name}} {{$staff->las_name}}}" value="{{$staff->staff_id}}">{{$staff->title}} {{$staff->f_name}} {{$staff->las_name}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary modal-update">Save changes</button>
                            </div>
                          </form>
                        </div>

                      </div>
                    </div>
                  </div>

        @endsection


@push('scripts')
    <script>
        $(document).ready(function(){
            //Refreshing the page to clear json data fetched for modal
            $('#mentor-modal').on('hide.bs.modal',function(){
                setTimeout(function(){
                    $.ajax({
                        type:'get',
                        url:'{{route('mentor.create')}}',
                        success:function(data){
                            $("#replace").html(data);
                            $('select').selectpicker('refresh');
                            console.log('Page Loading worked');
                        },
                        error:function(){
                            console.log('Page Loading Failed!!')
                        }
                    });
                },300)
            })
            //Script for editng data by  modal
            $('.email #edit').on('click',function(e){
                e.preventDefault();
                const rid = this.getAttribute('href'),
                    route = "{{route('mentor.edit',['id'])}}",
                    $_route = route.replace('id',rid);


                $.ajax({
                    type:'get',
                    url:$_route,
                    success:function(data)
                    {
                        console.log(data);
                        $('#mentor-modal').modal();
                        $('#mentor-level').html('<input type="text" name="level" class="form-control" value="'+data.level+'" readonly/> <input type="hidden" id="id" value="'+data.id+'" readonly/>');
                    },
                    error:function(data)
                    {
                        alert(data);
                    }
                });
            });
            // script for submitting all forms
            $('.form').on('submit',function(e){
                e.preventDefault();
                var myform = $(this).closest("form");
                $.ajax({
                    type: "POST",
                    url:'{{route('mentor.store')}}',
                    data: myform.serialize(),
                    success:function(data){
                        $.ajax({
                            type:'get',
                            url:'{{route('mentor.create')}}',
                            success:function(data){
                                $('#replace').html(data);
                                $('select').selectpicker('refresh');
                            }
                        });
                        alertify.set('notifier','position', 'top-center');
                        alertify.set('notifier','delay',2);
                        alertify.success('Saved');
                    },
                    error:function(data){
                        alert('failed')
                    }
                })

            });

            //script for updating data from modal
            $('#editForm').on('submit',function(e){
                e.preventDefault();
                var myform = $(this).closest("form");
                var rid    = $('#id').val(),
                    route = "{{route('mentor.update',['id'])}}",
                    $_route = route.replace('id',rid);
                $.ajax({
                    type: "put",
                    url:$_route,
                    data: myform.serialize(),
                    success:function(data){
                        $('#mentor-modal').modal('toggle');
                        window.location.reload();
                    },

                })

            });

            //script for sending email
            $('.email #request').on('click',function(e){
                e.preventDefault();
                var btn_id = $(this);
                var token = this.getAttribute('href');
                var name  = $('#std_name').val();
                $.ajax({
                    type:'get',
                    url:'mentor/email/'+token,
                    beforeSend:function(){
                        $(btn_id).html('<i class="fa  fa-spinner fa-pulse fa-fw"></i>...Please Wait');
                    },
                    success:function(data){
                        var staff_email = data.email;
                        $.ajax({
                            type:'get',
                            url: '/send',
                            data:{
                                token:token,
                                name:name,
                                staff_email: staff_email
                            },
                            beforeSend:function(){
                                $(btn_id).html('<i class="fa  fa-spinner fa-pulse fa-fw"></i>...Sending');
                            },
                            success:function(data){
                                $(btn_id).html('Mail Sent !!!');
                            },
                            error:function(x,e){
                                if(x.status==0){
                                    alert('You are offline..Check Your Internet Connection');
                                }
                                else if(x.status==500){
                                    alert('Email Could Not Be Sent Check Your Connection');
                                    //if the particular form Sending fails
                                    $(btn_id).html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>...Retry');

                                }
                                else{
                                    alert('unknown Error has occured');
                                }

                            }
                        })
                    }
                })
            })
        })
    </script>
@endpush
