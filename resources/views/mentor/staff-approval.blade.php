<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title></title>
  <link rel="stylesheet" href="{{asset('css/paper.css')}}">
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <link rel="shortcut icon" href="{{{ asset('logoo.png') }}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap-select.min.css')}}">
  <link href="https://fonts.googleapis.com/css?family=Alegreya|Cinzel+Decorative|Cormorant|Courgette|Fontdiner+Swanky|Great+Vibes|Lobster|Lora|Mogra|Mr+Dafoe|Mr+De+Haviland|Noto+Serif|Oregano|Poiret+One|Pompiere|Raleway|Slabo+27px|Spectral|Vibur|Yellowtail" rel="stylesheet">
  <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
</head>
<body>
    <nav class="navbar navbar-default text-font">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
                <a class="" href="#"><img src="{{asset('logoo.png')}}" style="height:65px"/></a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/book">THE PRACTICAL EXPERIENCE RECORD</a></li>
                    <li><a href="">Mentor Endorsement</a></li>
                </ul>
                        </div>
        </div>
      </nav>


  <div class="container-fluid">
      @if(!empty($user))
        <div class="col-md-6 col-md-offset-3"style="margin-top:5%;">
              <form class="form-horizontal" action="{{route('mentor_action')}}" method="post" role="form">
                  <div class="panel-heading text-center">
                    <img src="{{asset('storage/greenFolder/'.$user->img)}}"class="img-rounded" style="height:200px; width:300px ">
                  </div>
                    {{csrf_field()}}
                  <input type="hidden" value="{{$user->verifyToken}}" name="hashed_token" />
                <div class="form-group">
                  <label class="col-md-4 control-label"><i class="fa fa-user"></i> Name </label>
                    <div class="col-md-4">
                      <input type="text" class="form-control col-xm-3" value="{{$user->title}} {{$user->surname}} {{$user->othernames}}" readonly/>
                    </div>
                </div>

                <div class="form-group">
                  <label class="col-md-4 control-label"><i class="fa fa-id-card"></i> ID number</label>
                  <div class="col-md-6">
                    <input type="number" name="id"  value="{{$user->reg_num}}" class="form-control col-xm-3"  readonly/>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-md-4 control-label"><i class="fa fa-id-card"></i> Program</label>
                  <div class="col-md-6">
                    <input type="text" name="id"  value="{{$user->program}}" class="form-control col-xm-5"  readonly/>
                  </div>
                </div>

                  <div class="form-group">
                    <label class="col-md-4 control-label"> <i class="fa fa-gavel"></i> Action</label>
                    <div class="col-md-6">
                    <select class="selectpicker" name="action" required>
                      <option></option>
                      <option>Approved</option>
                      <option>Rejected</option>
                    </select>
                  </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-4 control-label"> <i class="fa fa-comment"></i> Comment</label>
                    <div class="col-md-6">
                      <textarea class="form-control"name="comment" cols="50" rows="2" required></textarea>
                    </div>
                  </div>

                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Send Response <i class="fa fa-send"></i></button>
                  </div>
                  <p class=" text-center" style="margin-top:2%;">
                    <i><b>NB:</b> You are taking this action as the level <b>{{$user->mlevel}}</b> mentor for <b>{{$user->surname}} {{$user->othernames}}</b></i>
                  </p>
          </form>
        </div>
        @else
        <div class="col-md-6 col-md-offset-3" style="margin-top:5%">
            <img src="{{asset('404.jpg')}}" class="col-md-offset-2" style="height:300px; width:450px"/>
          <div class="">
            <h3 class="text-style text-center text-danger">THIS URL HAS EXPIRED!!</h3>
          </div>
        </div>
        @endif
        </div>
</body>


</html>
