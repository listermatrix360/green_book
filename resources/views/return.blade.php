<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
@include('layout.user-header')
@yield('dash-header')
<body>
  <div class="container">
    <h4 class="text-font">You have successfully approved the Following Student as his level {{$user->level}} mentor.</h4>
<hr />      <div class="row">
            <div class="col-md-4">
              <ul class="list-group">
                <li class="list-group-item">{{$user->name}}</li>
                <li class="list-group-item">{{$user->reg_num}}</li>
                <li class="list-group-item">{{$user->email}}</li>
                <li class="list-group-item">{{$user->school}}</li>
                <li class="list-group-item">{{$user->phone1}}</li>
              </ul>
            </div>
              <div class="col-md-4">
                <img class="img-thumbnail"  src="" alt="Student Image Here"/>

              </div>
          </div>

  </div>
</body>


</html>
