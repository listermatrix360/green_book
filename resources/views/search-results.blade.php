



          <ul class="collection with-header">
            @if($get_field->isEmpty())
            <li class="collection-headers">
              <h5 class="cinzel">No Results</h5>
            </li>

            @else
            <li class="collection-headers">
              <h5 class="cinzel">   {{count($get_field)}} Results</h5>
            </li>

              @foreach($get_field as $fetch)
              <li class="collection-item transparent cinzel">
                <div>
                  <strong>

                <a href="{{route('getAll',["reg_num"=>$fetch->reg_num])}}" style="color:white">{{$fetch->surname}} {{$fetch->othernames}}</a>
                  </strong>
                <a href="{{route('getAll',["reg_num"=>$fetch->reg_num])}}" class="secondary-content cinzel">
                  <i class="material-icons">send</i></a>
              </div>
              </li>
              @endforeach
              @endif
          </ul>
