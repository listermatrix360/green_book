<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/js/metisMenu.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/waves.min.js')}}"></script>

<!-- Required datatable js -->
<script src="https://themesbrand.com/veltrix/layouts/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="https://themesbrand.com/veltrix/layouts/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="https://themesbrand.com/veltrix/layouts/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="https://themesbrand.com/veltrix/layouts/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="https://themesbrand.com/veltrix/layouts/plugins/datatables/jszip.min.js"></script>
<script src="https://themesbrand.com/veltrix/layouts/plugins/datatables/pdfmake.min.js"></script>
<script src="https://themesbrand.com/veltrix/layouts/plugins/datatables/vfs_fonts.js"></script>
<script src="https://themesbrand.com/veltrix/layouts/plugins/datatables/buttons.html5.min.js"></script>
<script src="https://themesbrand.com/veltrix/layouts/plugins/datatables/buttons.print.min.js"></script>
<script src="https://themesbrand.com/veltrix/layouts/plugins/datatables/buttons.colVis.min.js"></script>
<!-- Responsive examples -->
<script src="https://themesbrand.com/veltrix/layouts/plugins/datatables/dataTables.responsive.min.js"></script>
<!-- Datatable init js -->
<script src="{{asset('assets/pages/datatables.init.js')}}"></script>
<script src="{{asset('assets/js/app.js')}}"></script>
@stack('script')
