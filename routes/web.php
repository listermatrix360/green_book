<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('custom','AdminController@props')->name('props');
Route::get('/','PagesController@welcome')->name('welcome-home');

Route::namespace('Auth')->group(function (){
    Route::get('login','LoginController@showLoginForm')->name('login');
    Route::post('login','LoginController@postLogin')->name('post.login');
    Route::post('user/logout','LoginController@logout')->name('user.logout');

});

// Route::group(['middleware'=>'guest'], function(){
Route::get('com_service/verify/{token}', 'mailController@community_val_request')->name('community_validation');

//For accessing chapel service record for the student
Route::get('chapel_service/verify/{token}', 'mailController@chapel_val_request')->name('chapel_validation');
//For accessing physical fitness record for the student
Route::get('physical/xdfbbsbfjsdbfjf/{token}','mailController@physical_val_request')->name('physical_validation');

Route::get('warden/verify/{token}', 'mailController@warden_val_request')->name('warden_validation');
Route::get('remark/chaplain/{token}', 'mailController@remark_chap_val_request')->name('chaplain_remark_validation');
Route::post('community-warden-verify','CommunityServiceController@warden_update_verify')->name('community_warden_verify');
Route::post('chaplain-alpha-remark','CommunityServiceController@remark_chaplain')->name('remark_of_chaplain');
// });


Route::post('chapel-verify','CommunityServiceController@chapel_update_verify')->name('ess_chapel_verify');

//Physical fitness verification by the ESS if he wants to endorse the peculiar request
Route::post('physical-verify','CommunityServiceController@physical_update_verify')->name('ess_physical_verify');


Route::middleware(['auth:web'])->group(function(){

    Route::prefix('student')->group(function (){

    Route::get('pass_word',function(){
        return view('auth.passwords.change-pass');
    })->name('key');

    Route::post('pass','PagesController@password');
    Route::get('help','PagesController@help')->name('help');

    Route::resource('book','PagesController');

    Route::prefix('personal-details')->group(function (){

        Route::get('index', 'PagesController@show')->name('personal.show');
        Route::any('create','PagesController@create')->name('personal.create');
        Route::any('edit',  'PagesController@edit')->name('personal.edit');

    });

    Route::resource('diary','DiaryController');
    Route::resource('attach','AttachmentController');
    Route::resource('mentor','MentorController');
    Route::get('mentor/email/{token}','MentorController@getemail');
    Route::any('send-request-intern','AttachmentController@intern_notify')->name('intern-notification');
    Route::post('chapel_save','CommunityServiceController@ChapelSave')->name('chapel-post');
    Route::post('physical_save','CommunityServiceController@post_phyiscal_data')->name('physical-post');
    Route::post('attach/{id}','AttachmentController@update');
    Route::post('/book/get/{reg_num}','PagesController@update');
    Route::post('/diary/{id}','DiaryController@update');
    Route::post('community-post/{rid}','CommunityServiceController@update')->name('comm-update');
    Route::post('chapel/remark/update','CommunityServiceController@chapel_update_remark')->name('chapel-remark-update');
    Route::post('spirit-post/{rid}','CommunityServiceController@spirit_update')->name('spirit-update');
    Route::post('physical-post/{rid}','CommunityServiceController@physical_update')->name('physical-update');
    Route::post('community-post','CommunityServiceController@store')->name('comm-store');
    Route::post('community-verify','CommunityServiceController@update_verify')->name('ess_verify');



    Route::post('remark-post','CommunityServiceController@remark_store')->name('comm-remark');
    Route::post('chapel/remark/post','CommunityServiceController@chapel_remark')->name('spirit-remark');
    Route::post('remark-update','CommunityServiceController@remark_update')->name('comm-update');
    Route::post('edu-post','EducationController@create')->name('educ-create');
    Route::post('edu-update/{id}','EducationController@update')->name('educ-update');
    Route::post('prof-skills/alpha_add','AttachmentController@alpha_add')->name('alpha-add');
    Route::post('prof-skills/{id}','AttachmentController@update_prof_skill');
    Route::post('updateCourses/{id}','EducationController@updateOtherCourses')->name('updateCourse');
    Route::post('PostCourses','EducationController@postOtherCourses')->name('PostCourses');
    Route::post('ment/update','MentorController@update')->name('ment_update');
    Route::post('img_upload','PagesController@getFile')->name('upload');
    //Autobiography Routes
    Route::get('autobio','AutoBioController@index')->name('biography');
    Route::post('auto/post','AutoBioController@store')->name('auto-post');

    Route::post('auto/update','AutoBioController@update')->name('auto-update');

    Route::get('community','CommunityServiceController@index')->name('community-index');
    Route::get('community-show/{formID}','CommunityServiceController@show')->name('comm-show');
    Route::get('prof-skills','AttachmentController@skill')->name('prof-skills');
    Route::get('prof-skills/{id}','AttachmentController@show_prof_skill');
    Route::get('prof-skills/{id}/edit','AttachmentController@edit_prof_skill');
    Route::get('community-token/{token}','CommunityServiceController@token')->name('comm-token');
    //spiritual fitness route for getting information for sending email
    Route::get('spirit-token/{token}','CommunityServiceController@spirit_token')->name('spiritual-token');

    //Physical fitness route for getting information for sending email
    Route::get('physical-token/{token}','CommunityServiceController@physical_token')->name('physical-token');

    Route::any('community-edit/{rid}','CommunityServiceController@edit')->name('comm-edit');
    Route::any('chapel-edit/{rid}','CommunityServiceController@chapel_edit')->name('chapel-edit');
    Route::get('physical-edit/{rid}','CommunityServiceController@physical_edit')->name('physical-edit');
    Route::get('physical_exercise','CommunityServiceController@physical')->name('physical-index');

    Route::get('sp_fitness','CommunityServiceController@spiritual')->name('spirit-index');

    Route::get('education','EducationController@index')->name('educ-index');

    Route::get('edu-show/{id}','EducationController@show')->name('educ-show');

    Route::get('edu-edit/{id}','EducationController@edit')->name('educ-edit');

    Route::get('otherCourses','EducationController@SeeOtherCourses')->name('education.otherCourses');

    Route::get('seeCourses/{id}','EducationController@showOtherCourses')->name('viewCourses');

    Route::get('editCourses/{id}','EducationController@EditOtherCourses')->name('editCourses');

    Route::get('showment/{book}', 'PagesController@displayMentor')->name('displayMentor');

    Route::get('file/doc','PagesController@show_file')->name('data-file');

    Route::get('department/{id}', 'SchoolController@getSchoolDepartments')->name('school.department');
    Route::get('program/{department}', 'SchoolController@get_program')->name('department.program');

    Route::get('community/return/warden','CommunityServiceController@warden')->name('return_warden_email');
    Route::get('community/return/chapel','CommunityServiceController@chaplain')->name('return_chap_email');
    });
});

Route::middleware(['guest'])->group(function(){
    Route::get('/anylogin',function(){return view('admin.single');})->name('singleapp');
});

Route::get('/reject','DataController@reject')->name('reject'); //rejected PER datatable
Route::get('/approved','DataController@approve')->name('approve'); // approved PER datatable
Route::get ('admin/users', 'DataController@json')->name('json'); //student dataTables
Route::get ('admin/pending', 'DataController@pending')->name('pending'); //PER pending dataTables
Route::get ('admin/staff', 'DataController@staff')->name('staff'); //staff datatable
Route::get ('admin/staff/disabled', 'DataController@staff_disabled')->name('disabled_staff'); //disabled_staff datatable
Route::get('admin/student/graduated','DataController@graduated')->name('graduated');

Route::get('other/reject','DataController@Jnr_reject')->name('Jnrreject'); //rejected PER datatable
Route::get('other/approved','DataController@Jnr_approve')->name('Jnrapprove'); // approved PER datatable
Route::get ('other/admin/users', 'DataController@Jnr_json')->name('Jnrjson'); //student dataTables
Route::get ('/other/admin/pending', 'DataController@Jnr_pending')->name('Jnrpending'); //PER Jnrpending dataTables


//Route for the Educational Support Services
Route::prefix('admin')->group(function(){
    Route::get('/',                       'AdminController@index')->name('admin.dashboard');
    Route::get('/get/user/{target}',      'AdminController@getStudentData')->name('tblUserData');//fetching users into the modal for edit
    Route::get('/login',                  'Auth\adminLoginController@showLoginForm')->name('admin.login');
    Route::get('/uzers',                  'AdminController@usershow')->name('usershow'); //Displaying users for the admin
    Route::get('/show/ajax/users',        'AdminController@get_datatable')->name('ajax_users');
    Route::get('/show/{reg_num}',         'AdminController@show')->name('showuser');
    Route::get('/d_list',                 'AdminController@dynamic')->name('d_list');
    Route::get('/{reg_num}/edit',         'AdminController@edit')->name('editUser');
    Route::get('/show/status/{reg}',      'AdminController@change_status');
    Route::get('/add-staff',              'AdminController@addStaff')->name('addStaff'); //Display staff-form
    Route::get('/staff/{staff_id}','AdminController@staff_edit_info')->name('staffEditInfo'); //fecthing info for staff edititng
    Route::get('/disabled','AdminController@disabled_update_info')->name('staff-disable');
    Route::get('/enabled','AdminController@enabled_update_info')->name('staff-enable');
    Route::get('/home-tab-blade-first', 'AdminController@home_tab')->name('home-tab');

    Route::post('/dexterity/post-for-me','AdminController@community_final_remark')->name('admin.dexterity');
    Route::post('/dexterity/post-for-update','AdminController@physical_final_remark')->name('admin.magnet');
    Route::post('/logout',                'Auth\adminLoginController@logout')->name('admin.logout');
    Route::post('/then/login',            'Auth\adminLoginController@login')->name('admin.login.submit');
    Route::post('/staffadd',              'AdminController@createStaff')->name('staffadd');
    Route::post('/action',                'AdminController@action')->name('profile_action');
    Route::post('/update',                'AdminController@prof_status')->name('status_update');
    Route::post('/get/user/post',         'AdminController@updateStudentData')->name('update_user');
    Route::post('/change-level' ,          'AdminController@update_level')->name('level-change');
    Route::post('/staff/update','AdminController@staff_update_info')->name('staffupdate');
    Route::post('/com/post/{state}/{value}','AdminController@com_post_update')->name('e_com_update');//ess after login view of community request
    Route::post('/cha/post/{state}/{value}','AdminController@cha_post_update')->name('e_chapel_update');//ess after login view of chapel request
    Route::post('/phy/post/{state}/{value}','AdminController@phy_post_update')->name('e_physical_update');//ess after login view of physical request
    Route::post('/set/year/academic','AdminController@change_year')->name('set_academic_year');
    Route::post('/user/admin/defer','AdminController@defer_state')->name('defer-status');
});


// Routes for sending various emails
Route::get('/send', 'mailController@send'); // for mentor
Route::get('/com_mail','mailController@sendComm'); //for community service thus to the ess
Route::get('/warden_mail', 'mailController@wardComm');  // for the warden
Route::get('/spirit_mail', 'mailController@spiritComm');
Route::get('/physical_mail', 'mailController@PhysicalComm'); // chapel service mail to the ess director
Route::get('/chaplain_mail','mailController@chap_request'); // to the chaplain for chapel service remark

Route::get('verify/{token}', 'mailController@mentor_email')->name('mentor_email');  //getting mentor data
Route::post('action/mentor','mailController@update')->name('mentor_action');   // for updating mentor table by the mentor

//Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
// Route::post('/addstudent','auth\RegisterController@addStdUser')->name('inquire');

//notification route
Route::get('notify-alert-notify','NotifyController@notify')->name('flash-notify');
Route::get('intern/clutter/{token}','AttachmentController@endorse_intern')->name('intern.clutter');
Route::post('intern/endorse/{token}/{rec}','AttachmentController@update_endorse_intern');
//event route
Route::resource('swift','EventController');
Route::delete('swift/delete/{id}','EventController@destroy');
Route::resource('misc','DSController');
Route::get('misc-fetch/{sid}','DSController@get');



//GLOBAL SEARCH FIELD FOR ALL
Route::get('search/{value}',function($value){
    $values = explode(" ",$value);
    $get_field = DB::table('users')->select('*')->where('surname','LIKE','%'.$values[0].'%')
        ->orWhere('othernames','LIKE','%'.$values[0].'%')
        ->orWhere('reg_num','LIKE','%'.$values[0].'%')->get();
    return view('search-results',compact('get_field'));
});
Route::get('frames/{reg_num}','AdminController@report')->name('frame');
//ROUTE FOR ACCESSING THE PER OF STUDENT AFTER SEARCHING
Route::get('find.student.01.regent.02.per/local-6500/dir-4477/{reg_num}',function($reg_num){

    $status = DB::table('profile_statuses')->select('prof_status')->where('reg_num',$reg_num)->first();
    $pd = DB::table('users')->join('personal_details','users.reg_num','=','personal_details.reg_num')
        ->where('users.reg_num',$reg_num)->first();

    $mentor = DB::table('mentors')->join('staff','mentors.staff_id','=','staff.staff_id')
        ->where('mentors.reg_num',$reg_num)->get();

    $com    = DB::table('commservices')->where('reg_num',$reg_num)->get();
    $chap   = DB::table('chapels')->where('reg_num',$reg_num)->get();
    $phy    = DB::table('physical_fits')->where('reg_num',$reg_num)->get();
    $diary  = DB::table('diaries')->where('reg_num',$reg_num)->get();
    $intern = DB::table('attachments')->where('reg_num',$reg_num)->get();
    $educ   = DB::table('educ_exams')->where('reg_num',$reg_num)->get();
    $prof   = DB::table('prof_skills')->where('reg_num',$reg_num)->get();
    $other  = DB::table('othcourses')->where('reg_num',$reg_num)->get();
    $auto   = DB::table('auto_bios')->where('reg_num',$reg_num)->first();
    return view('final',compact('status','pd','mentor','com','chap','phy','diary','intern','educ',
        'prof','other','auto'));

})->name('getAll');


Route::post('excel-import','DSController@import')->name('excel');
Route::post('excel-import/staff','DSController@import_staff')->name('excel_staff');
